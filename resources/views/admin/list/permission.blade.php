@include('admin.header')
<div class="be-content">
    <div class="main-content container-fluid">
        @if(Session::has('message'))
			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
		@endif
		<div class="row">
			<div class="col-md-12">
				<h3>Edit Permission for {{ $user->name }} ({{ $user->admin_type }})</h3>
			</div>
		</div>
		<form id="create-user-form" method="POST" action="{{ url('/admin/list/'.$user->id.'/permission') }}">
			@csrf
			<div class="row">
			    <div class="col-md-6">
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">

			    	<br />
						    <div class="form-group pt-2">
						    	@if(\App\Permission::where('user_id',$user->id)->where('name','transaction')->count() > 0)
						        	<input name="transaction" type="checkbox" checked value="1">
						        @else
									 <input name="transaction" type="checkbox">
						        @endif
						        <label>Transaction Management</label>
						    </div>
						    
						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','report')->count() > 0)
						        	<input name="report" type="checkbox" checked value="1">
						        @else
									 <input name="report" type="checkbox">
						        @endif
						        <label>Reports</label>
						    </div>
						    
						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','bank')->count() > 0)
						        	<input name="bank" type="checkbox" checked value="1">
						        @else
									 <input name="bank" type="checkbox">
						        @endif
						        <label>Banks Management</label>
						    </div>

						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','product')->count() > 0)
						        	<input name="product" type="checkbox" checked value="1">
						        @else
									 <input name="product" type="checkbox">
						        @endif
						        <label>Products Management</label>
						    </div>

						    
							<div class="form-group pt-2">
								@if(\App\Permission::where('user_id',$user->id)->where('name','member')->count() > 0)
						        	<input name="member" type="checkbox" checked value="1">
						        @else
									 <input name="member" type="checkbox">
						        @endif
						        <label>Members Management</label>
						    </div>
						    
			            </div>
			        </div>
			    </div>
			    <div class="col-md-6">
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			            	<br />
						    
						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','bonus')->count() > 0)
						        	<input name="bonus" type="checkbox" checked value="1">
						        @else
									 <input name="bonus" type="checkbox">
						        @endif
						        <label>Bonus Management</label>
						    </div>
						    
						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','promotion')->count() > 0)
						        	<input name="promotion" type="checkbox" checked value="1">
						        @else
									 <input name="promotion" type="checkbox">
						        @endif
						        <label>Promotion Management</label>
						    </div>

						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','annoucement')->count() > 0)
						        	<input name="annoucement" type="checkbox" checked value="1">
						        @else
									 <input name="annoucement" type="checkbox">
						        @endif
						        <label>Annoucement Management</label>
						    </div>

						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','spin')->count() > 0)
						        	<input name="spin" type="checkbox" checked value="1">
						        @else
									 <input name="spin" type="checkbox">
						        @endif
						        <label>Spin Me</label>
						    </div>

						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','sport')->count() > 0)
						        	<input name="sport" type="checkbox" checked value="1">
						        @else
									 <input name="sport" type="checkbox">
						        @endif
						        <label>Sport Match</label>
						    </div>

						    <div class="form-group pt-2">
						        @if(\App\Permission::where('user_id',$user->id)->where('name','setting')->count() > 0)
						        	<input name="setting" type="checkbox" checked value="1">
						        @else
									 <input name="setting" type="checkbox">
						        @endif
						        <label>Settings</label>
						    </div>
						</div>
			        </div>
			    </div>
			    
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary btn-block btn-xl btn-submit">Update Permission</button>
					<a href="{{ url('admin/list/'.$user->id) }}" class="btn btn-primary btn-block btn-xl btn-info">Back To Admin</a>
				</div>
			</div>
		</form>
        
    </div>
</div>          
@include('admin.footer')

</body></html>