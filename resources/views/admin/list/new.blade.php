@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Add Admin User</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/list/')}}" enctype="multipart/form-data">
			                	<div class="row">
			                		<div class="col-md-12">
			                			@csrf
									    <div class="form-group">
									        <label>Admin Username</label>
									        <input type="text" name="name" class="form-control"  required>
									    </div>
									    <div class="form-group">
									        <label>Admin Email</label>
									        <input type="text" name="email" class="form-control" required>
									    </div>

									    <div class="form-group">
									        <label>Admin Type</label>
									        <input type="text" name="type" class="form-control" required>
									    </div>

									    <div class="form-group">
									        <label>Phone No</label>
									        <input type="text" name="phone" id="phone" class="form-control" required>
									    </div>
									    
									    <div class="form-group">
									        <label>Password</label>
									        <input type="password" name="password" class="form-control" required>
									    </div>

									    <div class="form-group">
									        <label>Confirm Password</label>
									        <input type="password" name="password_2" class="form-control" required>
									    </div>
			                		</div>
			                	</div>
							    
							    
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Create New</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	
	$(".role").change(function(){

		if(this.value == 4)
		{
			$('#affiliate_rate').show();
			$('#affiliate_super').show();
		}
		else
		{
			$('#affiliate_rate').hide();
			$('#affiliate_super').hide();
		}

		if(this.value == 5)
		{
			$('#master_rate').show();
		}
		else
		{
			$('#master_rate').hide();
		}

	});

</script>
<script type="text/javascript">
	$('#phone').mask('601000000000');
</script>
</body></html>