@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Admin Details</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <div class="row">
			                	<div class="col-md-6">
			                		<table class="table table-bordered table-striped">
			                			<tbody>
			                				<tr>
			                					<td width="50%"><strong>Admin Name</strong></td>
			                					<td>{{ $user->name }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Phone Number</strong></td>
			                					<td>{{ $user->phone }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Email</strong></td>
			                					<td>{{ $user->email }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Type</strong></td>
			                					<td>{{ $user->admin_type }}</td>
			                				</tr>
			                				
			                			</tbody>
			                		</table>
			                		<br />
			                		
			                	</div>
			                	<div class="col-md-6">

			                		<a href="{{ url('admin/list/'.$user->id.'/permission') }}" class="btn btn-info btn-block">Update Permission</a><br />
			                		<a data-toggle="modal" data-target="#modal-password" class="btn btn-info btn-block">Update Password</a><br />

			                		@if($user->is_ban == 0)
			                			<a href="{{ url('admin/users/'.$user->id.'/ban') }}" onclick="return confirm('Are you sure?');" class="btn btn-warning btn-block">Ban Admin</a><br />
			                		@else
			                			<a href="{{ url('admin/users/'.$user->id.'/unban') }}" onclick="return confirm('Are you sure?');" class="btn btn-success btn-block">Un-Ban ADmin</a><br />
			                		@endif
									
									@if(Auth::user()->id == 1)
				                		<a href="{{ url('admin/users/'.$user->id.'/delete') }}" onclick="return confirm('Are you sure? all the transaction attached to this user also will be deleted! This action cant be restore!');" class="btn btn-danger btn-block">PERMANENTLY DELETE ADMIN</a><br />
			                		@endif
			                </div>
			                

			            </div>
			        </div>
			    </div>
			</div>
		</div>
		
		<div id="modal-password" tabindex="-1" role="dialog" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
		            </div>
		            <div class="modal-body">
						<form method="POST" action="{{ url('admin/users/password') }}">
						    @csrf
						    <input type="hidden" name="user_id" value="{{ $user->id }}">

						    <div class="row">
						    	<div class="form-group col-md-12">
							        <label>New Password</label>
							        <input type="password" name="password" min="6" class="form-control" required>
							    </div>
						    </div>
						    
						    <div class="row">
						    	<div class="form-group col-md-12">
							        <label>Confirm Password</label>
							        <input type="password" name="password_confirm" min="6" class="form-control" required>
							    </div>
						    </div>
						    
						    <div class="form-group">
						        <button type="submit" class="btn btn-info btn-block">Update New Password</button>
						    </div>
						</form>
		            </div>
		            <div class="modal-footer"></div>
		        </div>
		    </div>
		</div>
		
    </div>
@include('admin.footer')
<script>

	$('[data-toggle="datepicker"]').datepicker({
	  	dateFormat: 'dd-mm-yy',
	  	autoclose: true,
	});

	$("input[name=from]").change(function(){
	    oTable.draw();
	    calculateTotalDeposit();
	    calculateTotalWithdraw();
	});

	$("input[name=to]").change(function(){
	    oTable.draw();
	    calculateTotalDeposit();
	    calculateTotalWithdraw();
	});

	$("#status").change(function(){
	    oTable.draw();
	});

    var oTable = $('#user-transaction-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
	            url: '{{ url('admin/users/'.$user->id.'/transaction-data') }}',
	            data: function(d) {
	                d.from_date = $('input[name=from]').val();
	                d.to_date = $('input[name=to]').val();
	                d.status = $('#status').val();
	        	},
	        },
            columns: [
                { data: 'transaction_id', name: 'transaction_id' },
                { data: 'transaction_type', name: 'transaction_type' },
                { data: 'amount', name: 'amount' },
                { data: 'game', name: 'game' },
                { data: 'status', name: 'status' },
                { data: 'created_at', name: 'created_at' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });

    calculateTotalDeposit();
    calculateTotalWithdraw();

	function calculateTotalDeposit()
	{
		var from_date = $('input[name=from]').val();
		var to_date = $('input[name=to]').val();
		var user_id = "{{ $user->id }}";

		$.get("{{ url('api/admin/depositTotalUser?fromdate=') }}"+ from_date + "&todate=" + to_date + "&user_id=" + user_id, function(data, status){
	        
	        $("#totalDeposit").text(data);

	    });
	}

	function calculateTotalWithdraw()
	{
		var from_date = $('input[name=from]').val();
		var to_date = $('input[name=to]').val();
		var user_id = "{{ $user->id }}";

		$.get("{{ url('api/admin/withdrawTotalUser?fromdate=') }}"+ from_date + "&todate=" + to_date + "&user_id=" + user_id, function(data, status){
	        
	        $("#totalWithdraw").text(data);

	    });
	}
    

     	$(".datepicker").datepicker({
     		dateFormat: 'dd/mm/yy',
     		autoclose: true
     	});
</script>

</body></html>