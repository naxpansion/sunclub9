@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Admin Users</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <table id="games-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                        	
			                            <th>Admin Name</th>
			                            <th>Type</th>
			                            <th>Status</th>
			                            <th>Date Created</th>
			                            <th>Updated By</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                </table>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script>

	$

    
    var oTable = $('#games-table').DataTable({
    	processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url('admin/list/data') }}',
        },
        columns: [
            { data: 'name', name: 'name' },
            { data: 'admin_type', name: 'admin_type' },
            { data: 'is_ban', name: 'is_ban' },
            { data: 'created_at', name: 'created_at' },
            { data: 'add_by', name: 'add_by' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
    

</script>
</body></html>