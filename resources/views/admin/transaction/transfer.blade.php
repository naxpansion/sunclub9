@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Transfer Transaction List's</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <div class="row">
	                            <div class="col-xs-8 form-inline" style="position: absolute; z-index: 2;">
	                                <div class="input-daterange input-group" id="datepicker">
	                                	<span class="input-group-addon">From</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="from" value="{{ \Carbon\Carbon::now()->format('d-m-Y') }}" />
	                                    <span class="input-group-addon">To</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="to" value="{{ \Carbon\Carbon::now()->format('d-m-Y') }}"/>
	                                    <span class="input-group-addon">Status</span>
	                                    <select id="status" class="form-control input-sm" style="border-radius: 0;">
	                                    	<option value="">ALL</option>
	                                    	<option value="1">Pending</option>
	                                    	<option value="2">Complete</option>
	                                    	<option value="3">Decline</option>
	                                    </select>
	                                </div>
	                            </div>
	                        </div>
			                <table id="transfer-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                            <th>Transaction ID</th>
			                            <th>Member Username</th>
			                            <th>From Product Game/Game ID</th>
			                            <th>To Product Game/Game ID</th>
			                            <th>Amount (MYR)</th>
			                            <th>Status</th>
			                            <th>Date Created</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                </table>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script>
    var oTable = $('#transfer-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: 'transfer-data',
                data: function(d) {
                    d.from_date = $('input[name=from]').val();
                    d.to_date = $('input[name=to]').val();
                    d.status = $('#status').val();
                
            	},
            },
            columns: [
                { data: 'transaction_id', name: 'transaction_id' },
                { data: 'user_id', name: 'user_id' },
                { data: 'from_game', name: 'from_game' },
                { data: 'to_game', name: 'to_game' },
                { data: 'amount', name: 'amount' },
                { data: 'status', name: 'status' },
                { data: 'created_at', name: 'created_at' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
</script>
<script type="text/javascript">
	$('[data-toggle="datepicker"]').datepicker({
	  	dateFormat: 'dd-mm-yy',
	  	autoclose: true,
	});

	$("input[name=from]").change(function(){
	    oTable.draw();
	    console.log('from date change');
	});

	$("input[name=to]").change(function(){
	    oTable.draw();
	    console.log('to date change');
	});

	$("#status").on('change', function(){
		oTable.draw();
		// $('#transaction-table').DataTable().search(this.value).draw();
	});
</script>
</body></html>