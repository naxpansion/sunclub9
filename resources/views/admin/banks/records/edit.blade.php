@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Edit Record For Bank {{ $bankRecord->bank->name }}</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ route('bank_records.update',$bankRecord->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
			                	<div class="form-group">
							        <label>Type</label>
							        {{ Form::select('transaction_type', ['Debit' => 'Debit', 'Credit' => 'Credit'], $bankRecord->transaction_type, ['class' => 'form-control']) }}
							    </div>
							    <div class="form-group">
							        <label>Amount</label>
							        <input type="number" step="0.01" name="amount" class="form-control" value="{{ $bankRecord->amount }}" required>
							    </div>
							    <div class="form-group">
							        <label>Description</label>
							        <textarea class="form-control" name="description">{{ $bankRecord->description }}</textarea>
							    </div>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Update</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>