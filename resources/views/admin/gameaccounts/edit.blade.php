@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Edit Account For {{ $game_account->game->name }}</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/gameaccounts/'.$game_account->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
							    <div class="form-group">
							        <label>Username</label>
							        <input type="text" name="username" class="form-control" value="{{ $game_account->username }}" required>
							    </div>
							    <div class="form-group">
							        <label>Password</label>
							        <input type="text" name="password" class="form-control" value="{{ $game_account->password }}" required>
							    </div>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Update</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>