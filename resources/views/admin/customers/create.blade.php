@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Add Potential Customer</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ route('customers.store') }}" enctype="multipart/form-data">
			                	@csrf
							    <div class="form-group">
							        <label>Name</label>
							        <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
							    </div>
							    <div class="form-group">
							        <label>Phone Number</label>
							        <input type="text" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" required>
							    </div>
							    <div class="form-group">
							        <label>Source</label>
							        <input type="text" name="source" class="form-control" value="{{ old('source') }}" required>
							    </div>
							    <div class="form-group">
							        <label>Status</label>
							        <select name="status" class="form-control">
							        	<option value="1">No Deposit</option>
							        	<option value="2">Deposit</option>
							        </select>
							    </div>
							    <div class="form-group">
							        <label>Game Interested</label>
							        <textarea name="game" class="form-control">{{ old('game') }}</textarea>
							    </div>
							    <div class="form-group">
							        <label>Remarks</label>
							        <textarea name="remarks" class="form-control">{{ old('remarks') }}</textarea>
							    </div>
							    
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Add New</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	$('#phone').mask('600000000000');
</script>
</body></html>