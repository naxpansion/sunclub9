@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Customer Details</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <div class="row">
			                	<div class="col-md-6">
			                		<table class="table table-bordered table-striped">
			                			<tbody>
			                				<tr>
			                					<td width="30%"><strong>Name</strong></td>
			                					<td>{{ $customer->name }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Phone Number</strong></td>
			                					<td>{{ $customer->phone }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Source</strong></td>
			                					<td>{{ $customer->source }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Status</strong></td>
			                					<td>
			                						@if($customer->status == 1)
														<span class="label label-warning">No Deposit</span>
			                						@else
														<span class="label label-success">Deposit</span>
			                						@endif
			                					</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Game Interested</strong></td>
			                					<td>{{ $customer->game }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Remarks</strong></td>
			                					<td>{{ $customer->remarks }}</td>
			                				</tr>
			                				<tr>
			                					<td><strong>Added At</strong></td>
			                					<td>{{ $customer->created_at->format('d M Y,  h:iA') }}</td>
			                				</tr>
			                			</tbody>
			                		</table>
			                	</div>
			                	<div class="col-md-6">
			                		<a href="{{ route('customers.edit',$customer->id) }}" class="btn btn-info btn-block">Edit Customer</a><br />
			                		<form method="post" action="{{ route('customers.destroy',$customer->id) }}">
			                		@csrf
			                		@method('delete')
			                		<button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Are you sure?');">Delete</button>
			                		</form>
			                	</div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>