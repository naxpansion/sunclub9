@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Edit Bonus</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/bonuses/'.$bonus->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
							    <div class="form-group">
							        <label>Bonus Code</label>
							        <input type="text" name="bonus_code" class="form-control" value="{{ $bonus->bonus_code }}" required>
							    </div>
							    <div class="form-group">
							        <label>Bonus Name</label>
							        <input type="text" name="name" class="form-control" value="{{ $bonus->name }}" required>
							    </div>
							    <div class="form-group">
							        <label>Bonus Description</label>
							        <textarea name="description" class="form-control" >{{ $bonus->description }}</textarea>
							    </div>
							    <div class="form-group">
							        <label>Bonus Type</label>
							        {{ Form::select('bonus_type', ['fixed' => 'Fixed', 'percentage' => 'Percentage'], $bonus->type, ['class' => 'form-control']) }}
							    </div>
							    <div class="form-group">
							        <label>Value</label>
							        <input type="text" name="bonus_value" class="form-control" value="{{ $bonus->value }}" required>
							    </div>
							    <div class="form-group">
							        <label>Minimum Deposit</label>
							        <input type="number" step="0.01" name="min_deposit" class="form-control" value="{{ $bonus->min_deposit }}" required>
							    </div>
							    <div class="form-group">
							        <label>Maximum Bonus</label>
							        <input type="number" name="max_bonus" class="form-control" value="{{ $bonus->max_bonus }}" required>
							    </div>
							     <div class="form-group">
							        <label>Required Turnover</label>
							        <input type="number" name="turnover" value="{{ $bonus->turnover }}" class="form-control" required>
							    </div>
							    <div class="form-group">
							        <label>Allow Multiple Use?</label>
							        {{ Form::select('allow_multiple', ['0' => 'No', '1' => 'Yes'], $bonus->allow_multiple, ['class' => 'form-control' , 'id' => 'multi_used']) }}
							    </div>
							    <div class="form-group">
							        <label>Usage?</label>
							        {{ Form::select('daily', ['4' => 'One Time','0' => 'Unlimited', '1' => 'Daily Once','2'=>'Yearly Once',], $bonus->daily, ['class' => 'form-control', 'id' => 'usage']) }}
							    </div>
							    <div class="form-group">
							        <label>Exclude Games To Used This Code</label>
							        @php
							        	$exclude_games_raw = explode(',', $bonus->exclude_games);

							        	$exclude_games = \App\Game::find($exclude_games_raw);

							        	$games = \App\Game::all()->pluck('name','id');

							        @endphp
							        {{ Form::select('exclude_games[]', $games, $exclude_games, ['class' => 'form-control multiselect' , 'multiple' => 'multiple']) }}
							    </div>
							    <div class="form-group">
							        <label>Exclude Days To Used This Code</label>
							        @php

							        	$exclude_days_raw = explode(',', $bonus->exclude_days);

							        	$exclude_days = [
							        		'Monday' => 'Monday',
							        		'Tuesday' => 'Tuesday',
							        		'Wednesday' => 'Wednesday',
							        		'Thursday' => 'Thursday',
							        		'Friday' => 'Friday',
							        		'Saturday' => 'Saturday',
							        		'Sunday' => 'Sunday'
							        	]

							        @endphp
							        {{ Form::select('exclude_days[]', $exclude_days, $exclude_days_raw, ['class' => 'form-control multiselect' , 'multiple' => 'multiple']) }}
							    </div>
							    
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Update Bonus Details</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	
	$(document).ready(function() {
	    $('.multiselect').select2();

	    $("#multi_used").on('change',function(){
        if($("#multi_used").val() == '0')
        {
            $("#usage").prop("disabled", true);
        }
        else
        {
            $("#usage").prop("disabled", false);
        }
	});

</script>
</body></html>