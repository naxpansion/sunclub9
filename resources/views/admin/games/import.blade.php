@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Import Account For {{ $game->name }}</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/games/'.$game->id.'/import') }}" enctype="multipart/form-data">
			                	@csrf
							    <div class="form-group">
							        <label>CSV File</label>
							        <input type="file" name="file" class="form-control" required>
							    </div>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Import</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>