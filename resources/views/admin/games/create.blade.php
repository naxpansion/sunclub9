@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Add New Product</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/games') }}" enctype="multipart/form-data">
			                	@csrf
							    <div class="form-group">
							        <label>Product Name</label>
							        <input type="text" name="name" class="form-control" required>
							    </div>
							    <div class="form-group">
							        <label>Category</label>
							        <select class="form-control multiselect" name="category[]" multiple="multiple">
										<option value="Live Casino">Live Casino</option>
							        	<option value="SportBooks">SportBooks</option>
							        	<option value="Slots">Slots</option>
									</select>
							    </div>
							    <div class="form-group">
							        <label>Game Logo</label>
							        <input type="file" name="logo" class="form-control" required>
							    </div>
							    <div class="form-group">
							        <label>DESKTOP LINK</label>
							        <input type="text" name="desktop_link" class="form-control" value="" required>
							    </div>
							    <div class="form-group">
							        <label>ANDROID LINK</label>
							        <input type="text" name="android_link" class="form-control" value="" required>
							    </div>
							    <div class="form-group">
							        <label>IOS LINK</label>
							        <input type="text" name="ios_link" class="form-control" value="" required>
							    </div>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Add New</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	
	$(document).ready(function() {
	    $('.multiselect').select2();
	});

</script>
</body></html>