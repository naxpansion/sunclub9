@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Product Details</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <div class="row">
			                	<div class="col-md-6">
			                		<table class="table table-bordered table-striped">
			                			<tbody>
			                				<tr>
			                					<td width="30%"><strong>Product Name</strong></td>
			                					<td>{{ $game->name }}</td>
			                				</tr>
			                				<tr>
			                					<td><strong>Category</strong></td>
			                					<td style="text-transform: capitalize;">{{ $game->category }}</td>
			                				</tr>
			                				<tr>
			                					<td><strong>Logo</strong></td>
			                					<td><img width="100%" src="{{ url('storage/games/'.$game->logo) }}"></td>
			                				</tr>
			                				<tr>
			                					<td><strong>New Account Balance</strong></td>
			                					<td>{{ $game->GameAccountsNotAssigned->count() }}</td>
			                				</tr>
			                				<tr>
			                					<td><strong>Created At</strong></td>
			                					<td>{{ $game->created_at->format('d/m/Y,  h:iA') }}</td>
			                				</tr>
			                			</tbody>
			                		</table>
			                	</div>
			                	<div class="col-md-6">
			                		<a href="{{ url('admin/games/'.$game->id.'/edit') }}" class="btn btn-info btn-block">Edit Product Games</a><br />
			                		<a href="{{ url('admin/games/'.$game->id.'/new_account') }}" class="btn btn-info btn-block">Add New Game ID</a><br />
			                		@if(Auth::user()->id == 1)
				                		<form method="post" action="{{ url('admin/games/'.$game->id) }}">
				                		@csrf
				                		@method('delete')
				                		<button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Are you sure?');">Delete</button>
				                		</form>
			                		@endif
			                		<br />
			                		<a href="{{ url('admin/games/'.$game->id.'/import') }}" class="btn btn-info btn-block">Import Account</a><br />
			                	</div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="row">
			    <div class="col-md-12">
			        <h3>All ID Lists</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <table id="games-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                            <th>Username</th>
			                            <th>Password</th>
			                            <th>Status</th>
			                            <th>Date Created</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                </table>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script>
    $(function() {
        $('#games-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url('admin/games/'.$game->id.'/account-data') }}',
            columns: [
                { data: 'username', name: 'username' },
                { data: 'password', name: 'password' },
                { data: 'status', name: 'status' },
                { data: 'created_at', name: 'created_at' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
    });
</script>
</body></html>