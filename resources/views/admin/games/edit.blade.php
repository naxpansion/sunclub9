@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Edit Product</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/games/'.$game->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
							    <div class="form-group">
							        <label>Game Name</label>
							        <input type="text" name="name" class="form-control" value="{{ $game->name }}" required>
							    </div>
							    <div class="form-group">
							        <label>Category</label>
							        @php
							        	$category_raws = explode(',', $game->category);

							        @endphp
							        {{ Form::select('category[]', ['Live Casino' => 'Live Casino','SportBooks' => 'SportBooks','Slots' => 'Slots'], $category_raws, ['class' => 'form-control multiselect' , 'multiple' => 'multiple']) }}
							    </div>

							    <div class="form-group">
							        <label>
							        	Game Logo <a target="_blank" href="{{ url('storage/games/'.$game->logo) }}"><span class="label label-info">Current Logo</span></a>
							        </label>
							        <input type="file" name="logo" class="form-control">
							    </div>
							    <div class="form-group">
							        <label>DESKTOP LINK</label>
							        <input type="text" name="desktop_link" class="form-control" value="{{ $game->desktop_link }}" required>
							    </div>
							    <div class="form-group">
							        <label>ANDROID LINK</label>
							        <input type="text" name="android_link" class="form-control" value="{{ $game->android_link }}" required>
							    </div>
							    <div class="form-group">
							        <label>IOS LINK</label>
							        <input type="text" name="ios_link" class="form-control" value="{{ $game->ios_link }}" required>
							    </div>
							    
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Update</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	
	$(document).ready(function() {
	    $('.multiselect').select2();
	});

</script>
</body></html>