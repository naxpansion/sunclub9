@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Edit Sport</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ route('sports.update',$sport->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
							    <div class="form-group">
							        <label>Team 1 Name</label>
							        <input type="text" name="team_1_name" class="form-control" value="{{ $sport->team_1_name }}" required>
							    </div>
							    <div class="form-group">
							        <label>Team 1 Logo (98x98)</label>
							        <input type="file" name="team_1_logo" class="form-control" value="">
							    </div>
							    <div class="form-group">
							        <label>Team 1 Score</label>
							        <input type="number" step="0.01" name="team_1_rating" class="form-control" value="{{ $sport->team_1_rating }}" required>
							    </div>
							    <div class="form-group">
							        <label>Team 2 Name</label>
							        <input type="text" name="team_2_name" class="form-control" value="{{ $sport->team_2_name }}" required>
							    </div>
							    <div class="form-group">
							        <label>Team 2 Logo (98x98)</label>
							        <input type="file" name="team_2_logo" class="form-control" value="">
							    </div>
							    <div class="form-group">
							        <label>Team 2 Score</label>
							        <input type="number" step="0.01" name="team_2_rating" class="form-control" value="{{ $sport->team_2_rating }}" required>
							    </div>
							    <div class="form-group">
							    	<label>Match Date Time</label>
					                <div class='input-group date' id='datetimepicker1'>
					                    <input type='text' name="match_time" value="{{ $sport->match_time->format('d-m-Y h:i A') }}" class="form-control" required />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
							    <button type="submit" class="btn btn-info btn-block">Add New</button>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	$('#phone').mask('600000000000');
</script>
</body></html>