@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Sport List</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <table id="customers-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                            <th>Team 1 Name</th>
			                            <th>Team 1 Score</th>
			                            <th>Team 2 Name</th>
			                            <th>Team 2 Score</th>
			                            <th>Match Date/Time</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                </table>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script>
    $(function() {
        $('#customers-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('sports.data') }}',
            columns: [
                { data: 'team_1_name', name: 'team_1_name' },
                { data: 'team_1_rating', name: 'team_1_rating' },
                { data: 'team_2_name', name: 'team_2_name' },
                { data: 'team_2_rating', name: 'team_2_rating' },
                { data: 'match_time', name: 'match_time' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
    });
</script>
</body></html>