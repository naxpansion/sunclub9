@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>All Affiliate Request's</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <div class="row">
	                            <div class="col-xs-8 form-inline" style="position: absolute; z-index: 2;">
	                                <div class="input-daterange input-group" id="datepicker">
	                                	<span class="input-group-addon">From</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="from" value="01-01-2019" />
	                                    <span class="input-group-addon">To</span>
	                                    <input type="text" data-toggle="datepicker" class="input-sm form-control" name="to" value="{{ \Carbon\Carbon::now()->format('d-m-Y') }}"/>
	                                    <span style="display: none;" class="input-group-addon">User Role</span>
	                                    <select id="select_role" class="form-control input-sm" style="border-radius: 0; display: none;">
	                                    	<option value="">ALL</option>
	                                    	<option value="3">User</option>
	                                    	<option value="2">Staff</option>
	                                    	<option value="4">Affiliate</option>
	                                    </select>
	                                </div>
	                            </div>
	                        </div>
			                <table id="games-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                        	<th>Member Name</th>
			                            <th>Phone No.</th>
			                            <th>Date Apply</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                </table>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script>

	$('[data-toggle="datepicker"]').datepicker({
	  	dateFormat: 'dd-mm-yy',
	  	autoclose: true,
	});

	$("input[name=from]").change(function(){
	    oTable.draw();
	});

	$("input[name=to]").change(function(){
	    oTable.draw();
	});

	$("#select_role").change(function(){
	    oTable.draw();
	});

    
    var oTable = $('#games-table').DataTable({
    	processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url('admin/affiliate-request/data') }}',
            data: function(d) {
                d.from_date = $('input[name=from]').val();
                d.to_date = $('input[name=to]').val();
                d.role = $('#select_role').val();
        	},
        },
        columns: [
            { data: 'name', name: 'name' },
            { data: 'phone', name: 'phone' },
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
    

</script>
</body></html>