@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Request Password Details</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <div class="row">
			                	<div class="col-md-6">
			                		<table class="table table-bordered table-striped">
			                			<tbody>
			                				<tr>
			                					<td width="30%"><strong>Request From</strong></td>
			                					<td>{{ $password_request->name }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Mobile No</strong></td>
			                					<td>{{ $password_request->phone }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Request At</strong></td>
			                					<td>{{ $password_request->created_at->format('d/m/Y h:i A') }}</td>
			                				</tr>
			                				
			                			</tbody>
			                		</table>
			                	</div>
			                	<div class="col-md-6">
			                		<a href="{{ url('admin/users/'.$password_request->user_id) }}" class="btn btn-info btn-block">View User Profile</a>
			                	</div>
			                	
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>