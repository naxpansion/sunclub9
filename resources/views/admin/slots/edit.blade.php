@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Add Slots</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ route('slots.update',$slot->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
							    <div class="form-group">
							        <label>Name</label>
							        <input type="text" name="name" class="form-control" value="{{ $slot->name }}" required>
							    </div>
							    <div class="form-group">
							        <label>Image (86x96)</label>
							        <input type="file" name="image" class="form-control" value="">
							    </div>
							    <div class="form-group">
							        <label>Rating</label>
							        <input type="number" min="1" max="5" name="rating" class="form-control" value="{{ $slot->rating }}" required>
							    </div>
							    <div class="form-group">
							        <label>Link</label>
							        <input type="text" name="link" class="form-control" value="{{ $slot->link }}" required>
							    </div>
							    <div class="form-group">
							        <label>Is Hot</label>
							        {{ Form::select('is_hot', ['1' => 'Yes', '0' => 'No'], $slot->is_hot, ['class' => 'form-control']) }}
							    </div>
							    <div class="form-group">
							        <label>Is New</label>
							        {{ Form::select('is_new', ['1' => 'Yes', '0' => 'No'], $slot->is_new, ['class' => 'form-control']) }}
							    </div>
							    <div class="form-group">
							        <label>Is Featured</label>
							        {{ Form::select('is_featured', ['1' => 'Yes', '0' => 'No'], $slot->is_featured, ['class' => 'form-control']) }}
							    </div>
							    <div class="form-group">
							        <label>Is Progressive</label>
							        {{ Form::select('is_progressive', ['1' => 'Yes', '0' => 'No'], $slot->is_progressive, ['class' => 'form-control']) }}
							    </div>
							    <button type="submit" class="btn btn-info btn-block">Add New</button>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
<script type="text/javascript">
	$('#phone').mask('600000000000');
</script>
</body></html>