@include('admin.header')
    <div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        System Log
                    </div>
                    <div class="panel-body">
                        <table id="plan-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th width="80%">Log</th>
                                    <th>Date / Time</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            

@include('admin.footer')
<script type="text/javascript">
    $(function() {
        $('#plan-table').DataTable({
            order : [],
            processing: true,
            serverSide: true,
            ajax: '{{ route('systemlogs.data') }}',
            columns: [
                { data: 'log', name: 'log' },
                { data: 'created_at', name: 'created_at' },
            ]
        });
    });
</script>
</body></html>