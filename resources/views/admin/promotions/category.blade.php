@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-4">
			        <h3>Add New Promotion Category</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ route('promotionscategory.store') }}" enctype="multipart/form-data">
			                	@csrf
							    <div class="form-group">
							        <label>Category Name</label>
							        <input type="text" name="name" class="form-control" required>
							    </div>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Add New</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			    <div class="col-md-8">
			        <h3>All Promotion Category List's</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <table id="banks-table" class="table table-striped table-hover table-fw-widget">
			                    <thead>
			                        <tr>
			                            <th>Name</th>
			                            <th>Created At</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    	@foreach($promoCategories as $category)
			                    	<tr>
			                    		<td>{{ $category->name }}</td>
			                    		<td>{{ $category->created_at }}</td>
			                    		<td><a class="label label-info">edit</a> <a class="label label-danger">delete</a></td>
			                    	</tr>
			                    	@endforeach
			                    </tbody>
			                </table>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>