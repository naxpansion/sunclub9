@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Edit Promotion</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ route('promotions.update',$promotion->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
							    <div class="form-group">
							        <label>Title</label>
							        <input type="text" name="name" class="form-control" required value="{{ $promotion->title }}">
							    </div>
							    <div class="form-group">
							        <label>Banner Desktop (<a href="{{ secure_asset('promotions/'.$promotion->img_desktop) }}">Current Image</a>)</label>
							        <input type="file" name="img_desktop" class="form-control" >
							    </div>
							    <div class="form-group">
							        <label>Banner Mobile (<a href="{{ secure_asset('promotions/'.$promotion->img_mobile) }}">Current Image</a>)</label>
							        <input type="file" name="img_mobile" class="form-control" >
							    </div>
							    <div class="form-group">
							        <label>Content Desktop</label>
							        <textarea class="form-control editor" name="content_desktop">{{ $promotion->desktop }}</textarea>
							    </div>
							    <div class="form-group">
							        <label>Content Mobile</label>
							        <textarea class="form-control editor" name="content_mobile">{{ $promotion->mobile }}</textarea>
							    </div>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Update</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>