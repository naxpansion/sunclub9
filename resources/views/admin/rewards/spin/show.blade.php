@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Spin Wheel Reward</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <div class="row">
			                	<div class="col-md-6">
			                		<table class="table table-bordered table-striped">
			                			<tbody>
			                				<tr>
			                					<td width="30%"><strong>Member Username</strong></td>
			                					<td>{{ $reward->user->name }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Transaction Type</strong></td>
			                					<td>Reward</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>Reward Amount</strong></td>
			                					<td>{{ $reward->reward }}</td>
			                				</tr>
			                				<tr>
			                					<td width="30%"><strong>To Product / ID</strong></td>
			                					<td>
			                						@php
			                							$game = \App\Game::find($reward->game_id);
			                							$game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$reward->user->id)->first();
	                                                    if($game_id){
	                                                    	$username =  $game_id->username;
	                                                    }
	                                                    else
	                                                    {
	                                                    	$username = "-";
	                                                    }
			                						@endphp
													{{ $game->name }} / {{ $username }}
			                					</td>
			                				</tr>
			                				<tr>
			                					<td><strong>Request Submited</strong></td>
			                					<td>{{ $reward->created_at->format('d/m/Y,  h:iA') }}</td>
			                				</tr>
			                			</tbody>
			                		</table>
			                		@if($reward->status == 2)
			                		<div class="row">
			                			<div class="col-md-6">
			                				<a href="{{ url('admin/rewards/spin/'.$reward->id.'/approve') }}" onclick="return confirm('Are you sure?')" class="btn btn-block btn-success">APPROVE</a>
			                			</div>
			                			<div class="col-md-6">
			                				<a href="{{ url('admin/rewards/spin/'.$reward->id.'/reject') }}" onclick="return confirm('Are you sure?')" class="btn btn-block btn-danger">REJECT</a>
			                			</div>
			                		</div>
			                		@endif
			                		
			                	</div>
			                	<div class="col-md-6">
			                		<div class="row">
			                			<div class="col-md-12">
			                				@if(Auth::user()->id == 1)
						                		<form method="post" action="{{ url('admin/rewards/spin/'.$reward->id) }}">
						                		@csrf
						                		@method('delete')
						                		<button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Are you sure?');">Delete</button>
						                		</form>
						                	@endif
			                			</div>
			                		</div>
			                	</div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>