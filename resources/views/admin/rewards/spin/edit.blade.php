@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Update Reward Status</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/rewards/spin/'.$reward->id) }}" enctype="multipart/form-data">
			                	@csrf
			                	@method('patch')
							    <div class="form-group">
							        <label>Category</label>
							        {{ Form::select('status', ['2' => 'In Progress', '3' => 'Approve', '4' => 'Reject'], $reward->status, ['class' => 'form-control']) }}
							    </div>
							    <div class="form-group">
							        <label>
							        	Remarks
							        </label>
							        <textarea class="form-control" name="remarks">{{ $reward->remarks }}</textarea>
							    </div>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Update</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>