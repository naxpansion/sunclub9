@include('admin.header')
    <div class="be-content">
        <div class="main-content container-fluid">
        	@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
			<div class="row">
			    <div class="col-md-12">
			        <h3>Manage Wheel</h3>
			        <div class="panel panel-default panel-border-color panel-border-color-primary">
			            <div class="panel-body">
			                <br />
			                <form method="POST" action="{{ url('admin/wheel') }}" enctype="multipart/form-data">
			                	@csrf
			                	<div class="form-group">
							        <label>Wheel PIE Image 500x500 (<a target="_blank" href="{{ url('wheel/wheel_all.png') }}">Current Image</a>)</label>
							        <input type="file" name="wheel_pie" class="form-control">
							    </div>
							    <table class="table">
							    	<tbody>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_1" value="{{ \App\Wheel::find(1)->name }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_1" value="{{ \App\Wheel::find(1)->percentage }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_1" value="{{ \App\Wheel::find(1)->code }}" class="form-control" required>
											    </div>
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_2" value="{{ \App\Wheel::find(2)->name }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_2" value="{{ \App\Wheel::find(2)->percentage }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_2" value="{{ \App\Wheel::find(2)->code }}" class="form-control" required>
											    </div>
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_3" value="{{ \App\Wheel::find(3)->name }}" class="form-control" required readonly="readonly">
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_3" value="{{ \App\Wheel::find(3)->percentage }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_3" value="{{ \App\Wheel::find(3)->code }}" class="form-control" readonly="readonly" required>
											    </div>
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_4" value="{{ \App\Wheel::find(4)->name }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_4" value="{{ \App\Wheel::find(4)->percentage }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_4" value="{{ \App\Wheel::find(4)->code }}" class="form-control" required>
											    </div>
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_5" value="{{ \App\Wheel::find(5)->name }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_5" value="{{ \App\Wheel::find(5)->percentage }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_5" value="{{ \App\Wheel::find(5)->code }}" class="form-control" required>
											    </div>
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_6" value="{{ \App\Wheel::find(6)->name }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_6" value="{{ \App\Wheel::find(6)->percentage }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_6" value="{{ \App\Wheel::find(6)->code }}" class="form-control" required>
											    </div>
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_7" class="form-control" value="{{ \App\Wheel::find(7)->name }}" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_7" value="{{ \App\Wheel::find(7)->percentage }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_7" value="{{ \App\Wheel::find(7)->code }}" class="form-control" required>
											    </div>
							    			</td>
							    		</tr>
							    		<tr>
							    			<td>
							    				<div class="form-group">
											        <label>Section Name</label>
											        <input type="text" name="name_8" value="{{ \App\Wheel::find(8)->name }}" class="form-control" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section percentage</label>
											        <input type="text" name="percentage_8" class="form-control" value="{{ \App\Wheel::find(8)->percentage }}" required>
											    </div>
							    			</td>
							    			<td>
							    				<div class="form-group">
											        <label>Section Code</label>
											        <input type="text" name="code_8" class="form-control" value="{{ \App\Wheel::find(8)->code }}" required>
											    </div>
							    			</td>
							    		</tr>
							    	</tbody>
							    </table>
							    <div class="form-group">
							        <button type="submit" class="btn btn-info">Update Wheel</button>
							    </div>
							</form>
			                <br />
			            </div>
			        </div>
			    </div>
			</div>
		</div>
    </div>
@include('admin.footer')
</body></html>