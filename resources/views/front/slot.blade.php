@extends ('front.master')
@section('content')


<div style="background: url({{ url('images/slot/bg.png') }}) no-repeat; background-position: center; background-size: cover; padding-left: 8%; padding-right: 8%;" >
	<div class="row">
		<div class="col-md-12 center">
			<img src="{{ url('images/slot/title.png') }}">
			<br />
			<img src="{{ url('images/slot/item.png') }}" usemap="#image-map">
		</div>
	</div>
	<br />
	<br />
</div>

<map name="image-map">
    <area target="" alt="" title="" href="{{ \App\Game::find(12)->desktop_link }}" coords="335,403,19" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(12)->android_link }}" coords="381,403,16" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(12)->ios_link }}" coords="423,406,16" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(8)->desktop_link }}" coords="782,393,19" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(8)->android_link }}" coords="826,396,14" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(8)->ios_link }}" coords="869,396,16" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(9)->desktop_link }}" coords="1207,398,18" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(9)->android_link }}" coords="1249,400,16" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(9)->ios_link }}" coords="1293,400,16" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(10)->desktop_link }}1" coords="534,845,15" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(10)->android_link }}" coords="579,845,16" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(10)->ios_link }}" coords="625,846,16" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(11)->desktop_link }}" coords="1000,835,22" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(11)->android_link }}" coords="1046,840,18" shape="circle">
    <area target="" alt="" title="" href="{{ \App\Game::find(11)->desktop_link }}" coords="1090,840,15" shape="circle">
</map>

@endsection
