@extends('front.master')
@section('content')
<div id="slider">
    <div style="width:100%;">
        <div class="slider-wrapper theme-default">
            <div class="nivoSlider homeslider">
                <img src="images/slides/1.png" alt="Free bonus online casino" />
                <img src="images/slides/2.png" alt="Free Daily bonus online casino" />
                <img src="images/slides/3.png" alt="Member free bonus online casino singapore" />
                <img src="images/slides/4.png" alt="Online casino Singapore free bonus" />
                <img src="images/slides/5.png" alt="Best 918kiss slot game free bonus" />
                <img src="images/slides/7.jpeg" alt="Mega888 slot game free bonus" />
                <img src="images/slides/6.png" alt="Best online casino referral bonus 25%" />
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 50px; padding: 0 5% 0 5%;" >
    <div class="col-md-6" style="background: url('sport/field.png'); height: 300px;">
        {{-- <img src="{{ secure_asset('images/football.png') }}"> --}}
        <div class="slider-sport" style="margin-top:27px; " id="matchcontent">
            @php
                $sports = \App\Sport::orderBy('match_time','ASC')->whereDate('match_time', '>=', \Carbon\Carbon::now())->get();
            @endphp
            @foreach($sports as $sport)
                <div class="slide">
                    <div class="row" style="margin:0; margin-top: 5px;">
                        <div class="col-md-4 text-center team">
                            <img src="{{ url('storage/matches/'.$sport->team_1_logo) }}" class="flag">
                            <h6 style="color: white;">{{ $sport->team_1_name }}</h6>
                            <p>{{ $sport->team_1_rating }}</p>
                        </div>
                        <div class="col-md-4 text-center team">
                            <img style="width:100%;" src="{{ url('images/games/5.png') }}"><a href="#">
                            <a target="_blank" href="http://www.igk99.com/Default1.aspx?lang=EN-US"><img class="btn-bet" style="padding-bottom: 8px;" src="https://88659552w8.cdnasiaclub.com/wcv2/images/MY/english/bet-btn.png"></a>
                            <p style="color:#9a9a9a;font-family: Conv_Montserrat-Light;line-height: 21px!important;">{{ $sport->match_time->format('d M Y') }}<br>{{ $sport->match_time->format('h:i A') }} (GMT+8)</p>
                        </div>
                        <div class="col-md-4 text-center team">
                            <img src="{{ url('storage/matches/'.$sport->team_2_logo) }}" class="flag">
                            <h6 style="color: white;">{{ $sport->team_2_name }}</h6>
                            <p>{{ $sport->team_2_rating }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-md-6">
        <div>
            <h1 style="color: gold; text-align: center;">LIVE CHAT 24 HOURS</h1>
        </div>
        <div>
            <br /><br />
            <ul style="list-style: none;">
                <li style="padding-bottom: 8px;">
                    <img src="images/whatsapp2.png" alt="" style="max-width: 70%; width: 8%;" />&nbsp;&nbsp;&nbsp;<span style="font-size:1.2rem;text-decoration:none;color:white"><a style="color: white; text-decoration: none;" href="{{ \App\Setting::find(8)->value }}">WhatsApp : {{ \App\Setting::find(8)->value }}</a></span>
                </li>
                <li style="padding-bottom: 8px;">
                    <img src="images/wechat2.png" alt="" style="max-width: 80%;width: 8%;" />&nbsp;&nbsp;&nbsp;<span style="font-size:1.2rem;text-decoration:none;color:white">WeChat ID : {{ \App\Setting::find(9)->value }}</span>
                </li>
                <li style="padding-bottom: 8px;">
                    <img src="{{ url('images/email.png') }}" alt="" style="max-width: 80%;width: 8%;" />&nbsp;&nbsp;&nbsp;<span style="font-size:1.2rem;text-decoration:none;color:white"><a style="color: white;" href="mailto:{{ \App\Setting::find(10)->value }}">Email : {{ \App\Setting::find(10)->value }}</a></span>
                </li>
            </ul>

        </div>
    </div>
</div>
@endsection
@section('popup')
<div id="popup-annouce" class="spin-wheel mfp-hide" >
    <div class="row">
        <div class="col-md-12" style="text-align: center;">
            @foreach(\App\Annoucement::all() as $annoucement)
                <img src="{{ url('storage/image/'.$annoucement->image) }}">
            @endforeach
        </div>
    </div>
</div>
@php
    $announcement = \App\Annoucement::all()->count();
@endphp
@if($announcement == 0)

@else
    <script type="text/javascript">
        (function($) {
            $(window).load(function () {
                // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
                $.magnificPopup.open({
                    items: {
                        src: '#popup-annouce'
                    },
                    type: 'inline'

                  // You may add options here, they're exactly the same as for $.fn.magnificPopup call
                  // Note that some settings that rely on click event (like disableOn or midClick) will not work here
                }, 0);
            });
        })(jQuery);
    </script>
@endif
@endsection

@section('footer_title')
    <div class="footer_div">
     <h1>Best Betting & Online Casino Games in Malaysia</h1>
     <h2>Join us now to get free credit & High bonus</h2>
   </div>
@endsection
