@extends ('front.master')
@section('content')


<div id="ctl00_cphBody_bg" class="vip">
    <div class="uni-title">
        <img src="/images/splash.png" alt=""/>
        <p></p>
    </div>
    <div id="vip_content">
        <div class="vip-title">
            <img src="/images/vip-club-title.png" alt="" />
        </div>
        <div class="backboard">
            <img src="/images/vip-board.png" alt="" width="100%" />
            <div class="vip-rank">
                <table id="tbl-rank-logo">
                    <tr>
                        <td style="width:30%;"><img src="/images/vip-badge-1.png" alt="" style="width:60%" /></td>
                        <td><img src="/images/vip-badge-silver.png" alt="" /></td>
                        <td><img src="/images/vip-badge-gold.png" alt="" /></td>
                        <td><img src="/images/vip-badge-plat.png" alt="" /></td>
                        <td><img src="/images/vip-badge-diam.png" alt="" /></td>
                        <td><img src="/images/vip-badge-sig.png" alt="" /></td>
                    </tr>
                </table>
                <table class="tbl-rank-det">
                    <tr>
                        <th style="width:30%;text-align:left;">Total Deposit More Than</th>
                        <th>MYR 30</th>
                        <th>MYR 10,000</th>
                        <th>MYR 50,000</th>
                        <th>MYR 100,000</th>
                        <th>MYR 150,000</th>
                    </tr>
                    <tr>
                        <td style="text-align:left;font-weight:bold !important;">Withdraw limit per day</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>7</td>
                        <td>Unlimited</td>
                    </tr>
                    <tr>
                        <td style="text-align:left;font-weight:bold !important;">Birthday Surprise</td>
                        <td>NO</td>
                        <td>MYR 100</td>
                        <td>MYR 500</td>
                        <td>MYR 1,000</td>
                        <td>MYR 2,000</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="m-backboard">
            <div class="m-bb-rank">
                <img src="/images/vip-board-m2.png" alt="" width="100%" />
                <div class="rank">
                    <div class="rank-logo">
                        <img src="/images/vip-badge-silver.png" alt="" width="35%" />
                    </div>
                    <div class="rank-tbl">
                        <table class="tbl-rank-det">
                            <tr>
                                <th style="width:30%;text-align:left;">Total Deposit More Than</th>
                                <th>MYR 30</th>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Withdraw limit per day</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Birthday Surprise</td>
                                <td>NO</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-bb-rank">
                <img src="/images/vip-board-m2.png" alt="" width="100%" />
                <div class="rank">
                    <div class="rank-logo">
                        <img src="/images/vip-badge-gold.png" alt="" width="35%" />
                    </div>
                    <div class="rank-tbl">
                        <table class="tbl-rank-det">
                            <tr>
                                <th style="width:30%;text-align:left;">Total Deposit More Than</th>
                                <th>MYR 10,000</th>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Withdraw limit per day</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Birthday Surprise</td>
                                <td>MYR 100</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-bb-rank">
                <img src="/images/vip-board-m2.png" alt="" width="100%" />
                <div class="rank">
                    <div class="rank-logo">
                        <img src="/images/vip-badge-plat.png" alt="" width="40%" />
                    </div>
                    <div class="rank-tbl">
                        <table class="tbl-rank-det">
                            <tr>
                                <th style="width:30%;text-align:left;">Total Deposit More Than</th>
                                <th>MYR 50,000</th>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Withdraw limit per day</td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Birthday Surprise</td>
                                <td>MYR 500</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-bb-rank">
                <img src="/images/vip-board-m2.png" alt="" width="100%" />
                <div class="rank">
                    <div class="rank-logo">
                        <img src="/images/vip-badge-diam.png" alt="" width="40%" />
                    </div>
                    <div class="rank-tbl">
                        <table class="tbl-rank-det">
                            <tr>
                                <th style="width:30%;text-align:left;">Total Deposit More Than</th>
                                <th>MYR 100,000</th>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Withdraw limit per day</td>
                                <td>7</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Birthday Surprise</td>
                                <td>MYR 1,000</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-bb-rank">
                <img src="/images/vip-board-m2.png" alt="" width="100%" />
                <div class="rank">
                    <div class="rank-logo">
                        <img src="/images/vip-badge-sig.png" alt="" width="45%" />
                    </div>
                    <div class="rank-tbl">
                        <table class="tbl-rank-det">
                            <tr>
                                <th style="width:30%;text-align:left;">Total Deposit More Than</th>
                                <th>MYR 150,000</th>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Withdraw limit per day</td>
                                <td>Unlimited</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;font-weight:bold !important;">Birthday Surprise</td>
                                <td>MYR 2,000</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="vip-desc">
            <div class="vip-list">
                <div class="vip-number">
                    <div class="number-round">1</div>
                </div>
                <div class="vip-steps">
                    <p>To redeem BIRTHDAY SURPRISE, member must send an enquiry to our Customer Service's Livechat, Whatsapp or WeChat.</p>
                </div>
            </div>
            <div class="vip-list">
                <div class="vip-number">
                    <div class="number-round">2</div>
                </div>
                <div class="vip-steps">
                    <p>Docs Requirement: Username and attach a copy of NRIC/Passport for verification.</p>
                </div>
            </div>
            <div class="vip-list">
                <div class="vip-number">
                    <div class="number-round">3</div>
                </div>
                <div class="vip-steps">
                    <p>The birthday bonus amount has to be rolled over 1 time before withdrawal can be made.</p>
                </div>
            </div>
            <div class="vip-list">
                <div class="vip-number">
                    <div class="number-round">4</div>
                </div>
                <div class="vip-steps">
                    <p>Deposit and Bonus claimed cannot be wagered in 918Kiss and Mega888 CASINO.</p>
                </div>
            </div>
            <div class="vip-list">
                <div class="vip-number">
                    <div class="number-round">5</div>
                </div>
                <div class="vip-steps">
                    <p>The BIRTHDAY SURPRISE is offered limited to individual member, family, account, email address, contact number, bank account or IP address. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</p>
                </div>
            </div>
            <div class="vip-list">
                <div class="vip-number">
                    <div class="number-round">6</div>
                </div>
                <div class="vip-steps">
                    <p>Deposit and Bonus claimed cannot be transferred to another game account until and unless the rollover requirement has been met.</p>
                </div>
            </div>
            <div class="vip-list">
                <div class="vip-number">
                    <div class="number-round">7</div>
                </div>
                <div class="vip-steps">
                    <p>After redeem birthday bonus, member VIP tag will restart from Silver again.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="PReg">
    <p>Not our member yet?</p>
    <a href="{{ route('register') }}" class="btn">Register Now</a>
</div>
    
@endsection
