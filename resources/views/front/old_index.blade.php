@extends ('front.master')

@section('content')
<style type="text/css">
    @media (min-width: 992px){
        .col-md-6 {
            width: 49%;
        }
    }
</style>
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider bullet skin 032 css*/
        .jssorb032 {position:absolute;}
        .jssorb032 .i {position:absolute;cursor:pointer;}
        .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
        .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}

        .left-arrow: {visibility: hidden}
    </style>
<div class="content">
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
            <div>
                <a href="{{ url('promotions') }}"><img data-u="image" src="main_banner.png" /></a>
            </div>
        </div>
        <!-- Bullet Navigator -->
        <!-- <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div> -->
        <!-- Arrow Navigator -->
        <!-- <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div> -->
    </div>
    <div class="container">
        <div class="home-content">
            <div class="row-p clearfix">
                <div class="col-md-12">
                    <img src="4.png" width="100%">
                </div>  
            </div>
            <div class="row-p clearfix">
                <div class="col-md-6 slider-sport-bg" style="
            margin-right: 0.5%;">
                    <h4>Sports</h4>
                    <div class="slider-sport" style="margin-top:27px;" id="matchcontent">
                        @php
                            $sports = \App\Sport::orderBy('match_time','ASC')->whereDate('match_time', '>=', \Carbon\Carbon::now())->get();
                        @endphp
                        @foreach($sports as $sport)
                            <div class="slide">
                                <div class="row" style="margin:0;">
                                    <div class="col-md-4 text-center team">
                                        <img src="{{ url('storage/matches/'.$sport->team_1_logo) }}" class="flag">
                                        <h6>{{ $sport->team_1_name }}</h6>
                                        <p>{{ $sport->team_1_rating }}</p>
                                    </div>
                                    <div class="col-md-4 text-center team">
                                        <img style="width:100%;" src="https://wc88xml.wc365.net/images/maxlogo.png"><a href="sport-maxbet.html">
                                        <a href="https://mbet11.com/downloads#maxbet"><img class="btn-bet" style="padding-bottom: 8px;" src="https://88659552w8.cdnasiaclub.com/wcv2/images/MY/english/bet-btn.png"></a>
                                        <p style="color:#9a9a9a;font-family: Conv_Montserrat-Light;line-height: 21px!important;">{{ $sport->match_time->format('d M Y') }}<br>{{ $sport->match_time->format('h:i A') }} (GMT+8)</p>
                                    </div>
                                    <div class="col-md-4 text-center team">
                                        <img src="{{ url('storage/matches/'.$sport->team_2_logo) }}" class="flag">
                                        <h6>{{ $sport->team_2_name }}</h6>
                                        <p>{{ $sport->team_2_rating }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6 slider-slots-bg" style="
            margin-left: 0.5%;">
                    <h4>Slots</h4>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active link-hot" href="#hotgame" role="tab" data-toggle="tab">Hot Game</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link-new" href="#newgame" role="tab" data-toggle="tab">New Game</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link-prog" href="#progs" role="tab" data-toggle="tab">Progressive</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link-fea" href="#fea" role="tab" data-toggle="tab">Featured</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="hotgame">
                            <div class="slider-hot">
                                @php
                                    $slots = \App\Slot::where('is_hot',1)->get();
                                    $i = 0;
                                @endphp
                                @foreach($slots as $slot)
                                    <div class="slide">
                                        <div class="row" style="padding:5px;">
                                            <div class="col-md-4 text-center team">
                                                <img src="{{ url('storage/slots/'.$slot->image) }}" class="flag">
                                            </div>
                                            <div class="col-md-8 team">
                                                <span class="title">{{ $slot->name }}</span><br />
                                                <span class="rate">{{ $slot->rating }} of 5 star</span><br />
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i><br /><br />
                                                <span class="btn-slot">
                                                    <a target="_blank" href="{{ $slot->link }}"><img src="https://88659552w8.cdnasiaclub.com/wcv2/images/MY/english/5.png"></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="newgame">
                            <div id="slider-newgame">
                                @php
                                    $slots = \App\Slot::where('is_new',1)->get();
                                    $i = 0;
                                @endphp
                                @foreach($slots as $slot)
                                    <div class="slide">
                                        <div class="row" style="padding:5px;">
                                            <div class="col-md-4 text-center team">
                                                <img src="{{ url('storage/slots/'.$slot->image) }}" class="flag">
                                            </div>
                                            <div class="col-md-8 team">
                                                <span class="title">{{ $slot->name }}</span><br />
                                                <span class="rate">{{ $slot->rating }} of 5 star</span><br />
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i><br /><br />
                                                <span class="btn-slot">
                                                    <a target="_blank" href="{{ $slot->link }}"><img src="https://88659552w8.cdnasiaclub.com/wcv2/images/MY/english/5.png"></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="progs">
                            <div id="slider-progs">
                                @php
                                    $slots = \App\Slot::where('is_progressive',1)->get();
                                    $i = 0;
                                @endphp
                                @foreach($slots as $slot)
                                    <div class="slide">
                                        <div class="row" style="padding:5px;">
                                            <div class="col-md-4 text-center team">
                                                <img src="{{ url('storage/slots/'.$slot->image) }}" class="flag">
                                            </div>
                                            <div class="col-md-8 team">
                                                <span class="title">{{ $slot->name }}</span><br />
                                                <span class="rate">{{ $slot->rating }} of 5 star</span><br />
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i><br /><br />
                                                <span class="btn-slot">
                                                    <a target="_blank" href="{{ $slot->link }}"><img src="https://88659552w8.cdnasiaclub.com/wcv2/images/MY/english/5.png"></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="fea">
                            <div id="slider-fea">
                                @php
                                    $slots = \App\Slot::where('is_featured',1)->get();
                                    $i = 0;
                                @endphp
                                @foreach($slots as $slot)
                                    <div class="slide">
                                        <div class="row" style="padding:5px;">
                                            <div class="col-md-4 text-center team">
                                                <img src="{{ url('storage/slots/'.$slot->image) }}" class="flag">
                                            </div>
                                            <div class="col-md-8 team">
                                                <span class="title">{{ $slot->name }}</span><br />
                                                <span class="rate">{{ $slot->rating }} of 5 star</span><br />
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i><br /><br />
                                                <span class="btn-slot">
                                                    <a target="_blank" href="{{ $slot->link }}"><img src="https://88659552w8.cdnasiaclub.com/wcv2/images/MY/english/5.png"></a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>           
            </div>
            <!-- <div class="row-p clearfix" >
                <div class="col-md-6" style="background-color: rgba(0,0,0,0.5); margin-right: 0.5%;">
                    <h4>Latest Deposit Transaction</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="30%">Name</th>
                                <th>Mobile No</th>
                                <th>Amount</th>
                                <th>Date / Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $deposits = \App\Transaction::orderBy('id', 'desc')->where('transaction_type','deposit')->where('deposit_type','normal')->take(3)->get();
                            @endphp
                            @foreach($deposits as $deposit)
                                <tr>
                                    <td>{{ substr($deposit->user->name, 0, 3) }}***{{ substr($deposit->user->name, -3) }}</td>
                                    <td>{{ substr($deposit->user->phone, 0, 4) . '****' . substr($deposit->user->phone,  -4) }}</td>
                                    <td>RM {{ $deposit->amount }}</td>
                                    <td>{{ $deposit->updated_at->format('d M Y, h:i A') }}</td>
                                </tr>
                                @php
                                    $random_deposit = \App\Transaction::inRandomOrder()->first();
                                @endphp
                                <tr>
                                    <td>{{ substr($random_deposit->user->name, 0, 3) }}***{{ substr($random_deposit->user->name, -3) }}</td>
                                    <td>{{ substr('01'.mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9), 0, 4) . '****' . substr('01'.mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9),  -4) }}</td>
                                    <td>RM {{ number_format(mt_rand(5,10) * 20, 2) }}</td>
                                    <td>{{ $deposit->updated_at->format('d M Y, h:i A') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6" style="background-color: rgba(0,0,0,0.5); margin-left: 0.5%;">
                    <h4>Latest Withdrawal Transaction</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="30%">Name</th>
                                <th>Mobile No</th>
                                <th>Amount</th>
                                <th>Date / Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $withdraws = \App\Transaction::orderBy('id', 'desc')->where('transaction_type','withdraw')->take(3)->get();
                            @endphp
                            @foreach($withdraws as $withdraw)
                                <tr>
                                    <td>{{ substr($withdraw->user->name, 0, 3) }}***{{ substr($withdraw->user->name, -3) }}</td>
                                    <td>{{ substr($withdraw->user->phone, 0, 4) . '****' . substr($withdraw->user->phone,  -4) }}</td>
                                    <td>RM {{ number_format($withdraw->amount,2) }}</td>
                                    <td>{{ $withdraw->updated_at->format('d M Y, h:i A') }}</td>
                                </tr>
                                @php
                                    $random_deposit = \App\Transaction::inRandomOrder()->first();
                                @endphp
                                <tr>
                                    <td>{{ substr($random_deposit->user->name, 0, 3) }}***{{ substr($random_deposit->user->name, -3) }}</td>
                                    <td>{{ substr('01'.mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9), 0, 4) . '****' . substr('01'.mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9).mt_rand(1,9),  -4) }}</td>
                                    <td>RM {{ number_format(mt_rand(5,10) * 20, 2) }}</td>
                                    <td>{{ $deposit->updated_at->format('d M Y, h:i A') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div> -->
            <!-- x -->
        </div>
    </div>
</div>
<div id="divdetection" style="display: none;" class="modal_mobile">
    <div style="padding: 20px; background-color: #fff; color: #000;">
        <div style="margin-bottom: 20px; text-decoration: underline;" class="modal_font_size">Detection</div>
        <span id="ctl00_MainContent_Label1" class="modal_font_size">Switch to Mobile / Tablet versions?</span>
        <div style="margin-top: 30px; text-align: center;">
            <input type="submit" name="ctl00$MainContent$btnyesswtich" value="Yes" id="ctl00_MainContent_btnyesswtich" class="modal_font_size modal_btn_width" />
            &nbsp;<input type="submit" name="ctl00$MainContent$btnnoswitch" value="No" id="ctl00_MainContent_btnnoswitch" class="modal_font_size modal_btn_width" />
        </div>
    </div>
</div>
<a id="ctl00_MainContent_lnkdetection" href="javascript:__doPostBack(&#39;ctl00$MainContent$lnkdetection&#39;,&#39;&#39;)" style="display: none;"></a>
<div id="boxes">
    <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window">
        <img src="images/common/betasia-popup-xmas-2017.jpg" />
    </div>
    <!-- Mask to cover the whole screen -->
    <div style="width: 1478px; height: 602px; display: none; opacity: 0.8;" id="mask">
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 0px;">
            <div class="modal-header" style="background: linear-gradient(to bottom, #ffd65e 3%,#fcc735 21%,#ff9f0f 47%,#ff9f0f 47%,#ff9f0f 99%); border: 1px solid orange;">
                <span type="button" class="close" data-dismiss="modal">&times;</span>
                <h4 class="modal-title">Annoucement</h4>
            </div>
            <div class="modal-body text-center" style="background-color: #373737; color: white;">
                <ol>
                    @foreach(\App\Annoucement::all() as $annoucement)
                        <img src="{{ url('storage/image/'.$annoucement->image) }}">
                    @endforeach
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,o:-0.7}],
              [{b:900,d:2000,x:-379,e:{x:7}}],
              [{b:900,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,sX:2,sY:2},{b:0,d:900,x:-171,y:-341,o:1,sX:-2,sY:-2,e:{x:3,y:3,sX:3,sY:3}},{b:900,d:1600,x:-283,o:-1,e:{x:16}}]
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        });
    </script>
<script type="text/javascript">
    // (function($) {
    //     $(window).load(function () {

    //         $.magnificPopup.open({
    //             items: {
    //                 src: '#spin-wheel'
    //             },
    //             type: 'inline'
    //         }, 0);
    //     });
    // })(jQuery);

    // (function($) {
    //     $(window).load(function () {

    //         $.magnificPopup.open({
    //             items: {
    //                 src: '#info-popup'
    //             },
    //             type: 'inline'
    //         }, 0);
    //     });
    // })(jQuery);


    

$('.slider-sport').bxSlider({
      auto: false,
      speed: 1000,
      pause: 7000,
      responsive: false,
});

var slider_hot = $('.slider-hot').bxSlider({
      auto: false,
      speed: 1000,
      pause: 7000,
      responsive: false,
      // wrapperClass: 'hotgame-wrapper'
});

var slider_new = $('#slider-newgame').bxSlider({
      auto: false,
      speed: 1000,
      pause: 7000,
      responsive: false,
      // wrapperClass: 'hotgame-wrapper'
});

var slider_prog = $('#slider-progs').bxSlider({
      auto: false,
      speed: 1000,
      pause: 7000,
      responsive: false,
      // wrapperClass: 'hotgame-wrapper'
});

var slider_fea = $('#slider-fea').bxSlider({
      auto: false,
      speed: 1000,
      pause: 7000,
      responsive: false,
      // wrapperClass: 'hotgame-wrapper'
});

$(".link-hot").on("click",function(){
    setTimeout(function() {
        slider_hot.redrawSlider();
    }, 0);
});

$(".link-new").on("click",function(){  
    setTimeout(function() {
        slider_new.redrawSlider();
    }, 0); 
});

$(".link-prog").on("click",function(){
    setTimeout(function() {
        slider_prog.redrawSlider();
    }, 0);
});

$(".link-fea").on("click",function(){  
    setTimeout(function() {
        slider_fea.redrawSlider();
    }, 0); 
});
</script>
@endsection

