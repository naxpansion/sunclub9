@extends ('front.master')
@section('content')


<div style="display:none;">
    Promotions
</div>
<div id="ctl00_cphBody_bg" class="promotion">
    <div class="uni-title">
        <img src="/images/splash.png" alt=""/>
        <p>PROMOTIONS</p>
    </div>
    <link rel="stylesheet" type="text/css" href="/lib/js/animatedscroller/accordion.css" />
    <!-- ************************************* 150% WELCOME BONUS *************************************-->
    @foreach(\App\Promotion::all() as $promotion)
    <div class="accordionBanner">
        <p>{!! $promotion->title !!}</p>
        <img src="{{ url('storage/promotions/'.$promotion->img_desktop) }}" class="promo-large-img" />
        <img src="{{ url('storage/promotions/'.$promotion->img_desktop) }}" class="promo-small-img" />
    </div>
        <div class="accordionContent">
            <div style="padding:10px;" class="dinpro txt14">
                {!! $promotion->desktop !!}
            </div>
        </div>
        <div class="space20"></div>
    @endforeach
    {{-- <!-- ************************************* FIRST DAILY DEPOSIT BONUS 30% *************************************-->
    <div class="accordionBanner">
        <p><span>30%</span> FIRST DAILY DEPOSIT UP TO <span>MYR 388</span></p>
        <img src="images/slides/2.png" class="promo-large-img" />
        <img src="/images/m-pban5.jpg" class="promo-small-img" />
    </div>
    <div class="accordionContent">
        <div style="padding:10px;" class="dinpro txt14">
            <span class="white txt16 b600 dinpro">FIRST DAILY DEPOSIT BONUS 30%</span>
            <div class="space10"></div>
            <ol>
                <li>All members can claim the Bonus ONCE (1) per day.</li>
                <li>Select the Bonus code at the deposit page. Member will be loaded with 30% Bonus on top of the deposit made up to a maximum of MYR388.</li>
                <li>
                    The Deposit and Bonus amount have to be rolled over 25 times before withdrawal can be made.
                    <div class="space10"></div>
                    <b>Example</b>
                    <table width="100%" class="tbh1">
                        <tr>
                            <th>Deposit</th>
                            <td>MYR100</td>
                        </tr>
                        <tr>
                            <th>Bonus</th>
                            <td>MYR100 x 30% = MYR30</td>
                        </tr>
                        <tr>
                            <th>Min Withdrawal</th>
                            <td>(MYR100 + MYR30) x 25 = MYR3,250</td>
                        </tr>
                    </table>
                    <div class="space30"></div>
                </li>
                <li>All draw bets, void bets, bets made on both outcomes, bets placed on odds below Euro 1.75 or Asia 0.75 will not be taken into the calculation or count towards any rollover requirement.</li>
                <li>Deposit and Bonus claimed cannot be wagered in 918kiss and Mega888 CASINO.</li>
                <li>Deposit and Bonus claimed cannot be transferred to another game account until and unless the rollover requirement has been met.</li>
                <li>The promotion is offered limited to individual member, family, account, email address, contact number, bank account or IP address ONCE (1) per day. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
                <li>This Promotion cannot be claimed in conjunction with any other promotions.</li>
                <li>sunclub9 reserves the right to alter, amend or terminate this Promotion, or any aspect of it, at any time without prior notice.</li>
            </ol>
        </div>
    </div>
    <div class="space20"></div>
    <!-- ************************************* MEMBER SPECIAL 20% *************************************-->
    <div class="accordionBanner">
        <p><span>20%</span> MEMBER SPECIAL DAILY BONUS UP TO <span>MYR 388</span></p>
        <img src="images/slides/3.png" class="promo-large-img" />
        <img src="/images/m-pban4.jpg" class="promo-small-img" />
    </div>
    <div class="accordionContent">
        <div style="padding:10px;" class="dinpro txt14">
            <span class="white txt16 b600 dinpro">MEMBER DAILY DEPOSIT BONUS 20%</span>
            <div class="space10"></div>
            <ol>
                <li>All members can claim the bonus ONCE (1) per day.</li>
                <li>Select the Bonus code at the deposit page. Member will be loaded with 20% Bonus on top of the deposit made up to a maximum of MYR388.</li>
                <li>
                    The Deposit and Bonus amount have to be rolled over 20 times before withdrawal can be made.
                    <div class="space10"></div>
                    <b>Example</b>
                    <table width="100%" class="tbh1">
                        <tr>
                            <th>Deposit</th>
                            <td>MYR100</td>
                        </tr>
                        <tr>
                            <th>Bonus</th>
                            <td>MYR100 x 20% = MYR20</td>
                        </tr>
                        <tr>
                            <th>Rollover requirement</th>
                            <td>(MYR100 + MYR20) x 20 = MYR2,400</td>
                        </tr>
                    </table>
                    <div class="space30"></div>
                </li>
                <li>All draw bets, void bets, bets made on both outcomes, bets placed on odds below Euro 1.75 or Asia 0.75 will not be taken into the calculation or count towards any rollover requirement.</li>
                <li>Deposit and Bonus claimed cannot be wagered in 918kiss and Mega888 CASINO.</li>
                <li>Deposit and Bonus claimed cannot be transferred to another game account until and unless the rollover requirement has been met.</li>
                <li>The promotion is offered limited to individual member, family, account, email address, contact number, bank account or IP address ONCE (1) per day. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
                <li>This Promotion cannot be claimed in conjunction with any other promotions.</li>
                <li>sunclub9 reserves the right to alter, amend or terminate this Promotion, or any aspect of it, at any time without prior notice.</li>
            </ol>
        </div>
    </div>
    <div class="space20"></div>
    <!-- ************************************* UNLIMITED 10% *************************************-->
    <div class="accordionBanner">
        <p><span>10%</span> UNLIMITED BONUS UP TO <span>MYR 388</span></p>
        <img src="images/slides/4.png" class="promo-large-img" />
        <img src="/images/m-pban2.jpg" class="promo-small-img" />
    </div>
    <div class="accordionContent">
        <div style="padding:10px;" class="dinpro txt14">
            <span class="white txt16 b600 dinpro">UNLIMITED BONUS 10%</span>
            <div class="space10"></div>
            <ol>
                <li>All members can claim the bonus UNLIMITED.</li>
                <li>Select the Bonus code at the deposit page. Member will be loaded with 10% Bonus on top of the deposit made up to a maximum of MYR388.</li>
                <li>
                    The Deposit and Bonus amount have to be rolled over 10 times before withdrawal can be made.
                    <div class="space10"></div>
                    <b>Example</b>
                    <table width="100%" class="tbh1">
                        <tr>
                            <th>Deposit</th>
                            <td>MYR100</td>
                        </tr>
                        <tr>
                            <th>Bonus</th>
                            <td>MYR100 x 10% = MYR10</td>
                        </tr>
                        <tr>
                            <th>Rollover requirement</th>
                            <td>(MYR100 + MYR10) x 10 = MYR1,100</td>
                        </tr>
                    </table>
                    <div class="space30"></div>
                </li>
                <li>All draw bets, void bets, bets made on both outcomes, bets placed on odds below Euro 1.75 or Asia 0.75 will not be taken into the calculation or count towards any rollover requirement.</li>
                <li>Deposit and Bonus claimed cannot be wagered in 918kiss and Mega888 CASINO.</li>
                <li>Deposit and Bonus claimed cannot be transferred to another game account until and unless the rollover requirement has been met.</li>
                <li>The promotion is offered limited to individual member, family, account, email address, contact number, bank account or IP address ONCE (1) per day. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
                <li>This Promotion cannot be claimed in conjunction with any other promotions.</li>
                <li>sunclub9 reserves the right to alter, amend or terminate this Promotion, or any aspect of it, at any time without prior notice.</li>
            </ol>
        </div>
    </div>
    <div class="space20"></div>
    <!-- ************************************* EXCLUSIVE 918KISS BONUS *************************************-->
    <div class="accordionBanner">
        <p><span>918KISS</span> EXCLUSIVE BONUS <span>20%</span> UP TO <span>MYR 388</span></p>
        <img src="images/slides/5.png" class="promo-large-img" />
        <img src="/images/m-pban6.jpg" class="promo-small-img" />
    </div>
    <div class="accordionContent">
        <div style="padding:10px;" class="dinpro txt14">
            <p><span class="white txt16 b600 dinpro">EXCLUSIVE 918Kiss BONUS</span> UP TO <span>MYR 388</span></p>
            <div class="space10"></div>
            <ol>
                <li>All members can claim the bonus ONCE (1) per day.</li>
                <li>Select the Bonus code at the deposit page. Member will be loaded with 20% Bonus on top of the deposit made up to a maximum of MYR388.</li>
                <li>
                    The Deposit and Bonus amount have to hit FIVE (5) times of deposit and bonus before withdrawal can be made.
                    <div class="space10"></div>
                    <b>Example</b>
                    <table width="100%" class="tbh1">
                        <tr>
                            <th>Deposit</th>
                            <td>MYR100</td>
                        </tr>
                        <tr>
                            <th>Bonus</th>
                            <td>MYR100 x 20% = MYR20</td>
                        </tr>
                        <tr>
                            <th>Min Withdrawal</th>
                            <td>(MYR100 + MYR20) x 5 = MYR600</td>
                        </tr>
                    </table>
                    <div class="space30"></div>
                </li>
                <li>Deposit and Bonus claimed cannot be transferred to another game account until and unless the withdrawal requirement has been met.</li>
                <li>The promotion is offered limited to individual member, family, account, email address, contact number, bank account or IP address ONCE (1) per day. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
                <li>This Promotion cannot be claimed in conjunction with any other promotions.</li>
                <li>sunclub9 reserves the right to alter, amend or terminate this Promotion, or any aspect of it, at any time without prior notice.</li>
            </ol>
        </div>
    </div>
    <div class="space20"></div>
    <!-- ************************************* Mega888 Exclusive Bonus 20% Up To MYR388 *************************************-->
    <div class="accordionBanner">
        <p><span>Mega888</span> EXCLUSIVE BONUS <span>20%</span> UP TO <span>MYR 388</span></p>
        <img src="images/slides/slide_7.jpeg" class="promo-large-img" />
        <img src="/images/m-pban6.jpg" class="promo-small-img" />
    </div>
    <div class="accordionContent">
        <div style="padding:10px;" class="dinpro txt14">
            <span class="white txt16 b600 dinpro">MEGA888 EXCLUSIVE BONUS 20% UP TO MYR388</span>
            <div class="space10"></div>

            <ol>
                <li>All members can claim the bonus ONCE (1) per day.</li>
                <li>Select the Bonus code at the deposit page. Member will be loaded with 20% Bonus on top of the deposit made up to a maximum of MYR388.</li>
                <li>
                    The Deposit and Bonus amount have to hit FIVE (5) times of deposit and bonus before withdrawal can be made.

                    <div class="space10"></div>
                    <b>Example</b>

                    <table width="100%" class="tbh1">
                        <tr>
                            <th>Deposit</th>
                            <td>MYR100</td>
                        </tr>
                        <tr>
                            <th>Bonus</th>
                            <td>MYR100 x 20% = MYR20</td>
                        </tr>
                        <tr>
                            <th>Min Withdrawal</th>
                            <td>(MYR100 + MYR20) x 5 = MYR600</td>
                        </tr>
                    </table>
                    <div class="space30"></div>

                </li>
                <li>Deposit and Bonus claimed cannot be transferred to another game account until and unless the withdrawal requirement has been met.</li>
                <li>The promotion is offered limited to individual member, family, account, email address, contact number, bank account or IP address ONCE (1) per day. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
                <li>This Promotion cannot be claimed in conjunction with any other promotions.</li>
                <li>sunclub9 reserves the right to alter, amend or terminate this Promotion, or any aspect of it, at any time without prior notice.</li>
                
            </ol>

        </div>
    </div>
    <div class="space20"></div>

    <!-- ************************************* REFERRAL BONUS 25% *************************************-->
    <div class="accordionBanner">
        <p><span>REFERRAL</span> BONUS UP TO <span>25%</span></p>
        <img src="images/slides/6.png" class="promo-large-img" />
        <img src="/images/m-pban3.jpg" class="promo-small-img" />
    </div>
    <div class="accordionContent">
        <div style="padding:10px;" class="dinpro txt14">
            <span class="white txt16 b600 dinpro">REFERRAL BONUS UP TO 25%!<br />(Please Contact Customer Service for the BONUS!)</span>
            <div class="space10"></div>
            TERMS & CONDITIONS
            <div class="space10"></div>
            <ol>
                <li>
                    This promotion is open to all sunclub9 active members. Example: If you introduce a friend and he deposits MYR5,000 you will get 10% (MYR500) Referral Bonus. When your friend makes the second deposit, you get 15% bonus of the deposit amount.
                    <div class="space10"></div>
                    <b>Example</b>
                    <table width="100%" class="tbh1">
                        <tr>
                            <th>REFERRED FRIEND BONUS</th>
                            <th>MAX.BONUS</th>
                            <th>ROLLOVER</th>
                        </tr>
                        <tr>
                            <td>FIRST DEPOSIT 10%</td>
                            <td>UNLIMITED</td>
                            <td>5X</td>
                        </tr>
                        <tr>
                            <td>SECOND DEPOSIT 15%</td>
                            <td>UNLIMITED</td>
                            <td>8X</td>
                        </tr>
                    </table>
                    <div class="space30"></div>
                </li>
                <li>
                    We only will give out the REFERRAL BONUS once your friend rollover has reach x2.
                    <div class="space10"></div>
                    <b>Example</b>
                    <table width="100%" class="tbh1">
                        <tr>
                            <th>Your Friend Deposit</th>
                            <td>MYR1000</td>
                        </tr>
                        <tr>
                            <th>Rollover requirement</th>
                            <td>MYR1000 x 2 = MYR2,000</td>
                        </tr>
                    </table>
                    <div class="space30"></div>
                </li>
                <li>To qualify as a Referrer, you must be an existing sunclub9 active member and have made a minimum of 5 times deposit.</li>
                <li>To redeem your referral bonus, your new "Friends" must register an account and make deposits at least MYR50 in their accounts during promotional period. Kindly contact our customer service and provide you and your friend ID. The referral bonus will be credited to you within 24 hours after verifications.</li>
                <li>If the deposit and bonus are not rolled over for at least 1 time within 7 days after bonus is claimed, the full bonus amount will be forfeited. In the event that a withdrawal is made prior to the fulfilment of the rollover requirement, the bonus and winnings will also be forfeited.</li>
                <li>Referral bonus must make a turnover of x5/x8 times before any withdrawal can be made.</li>
                <li>All draw bets, void bets, bets made on both outcomes, bets placed on odds below Euro 1.75 or Asia 0.75 will not be taken into the calculation or count towards any rollover requirement.</li>
                <li>Deposit and Bonus claimed cannot be wagered in 918kiss and Mega888 CASINO.</li>
                <li>Deposit and Bonus claimed cannot be transferred to another game account until and unless the rollover requirement has been met.</li>
                <li>The promotion is offered limited to individual member, family, account, email address, contact number, bank account or IP address. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
                <li>This Promotion cannot be claimed in conjunction with any other promotions.</li>
                <li>sunclub9 reserves the right to alter, amend or terminate this Promotion, or any aspect of it, at any time without prior notice.</li>
            </ol>
        </div>
    </div> --}}
    <div class="space20"></div>
    <div class="space20"></div>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript" src="/lib/js/animatedscroller/ddaccordion.js"></script>
    <script type="text/javascript" src="/lib/js/animatedscroller/accordian.js"></script>
</div>
@auth
<div class="PReg">
    <p>Not our member yet?</p>
    <a href="{{ route('register') }}" class="btn">Register Now</a>
</div>
@endauth
@endsection
