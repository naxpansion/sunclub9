@extends ('front.master')
@section('content')


<div>
	<div class="row">
		<div class="col-md-12" style="text-align: center; background: url({{ url('images/Sportsbook_banner.jpg') }}); background-position: center; background-repeat: no-repeat; padding-top: 10%;">
			<div id="slider" style="text-align: center;">
			    <div style="width:50%; margin: auto;" >
			        <div class="slider-wrapper theme-default">
			            <div class="nivoSlider homeslider">
			            	<img src="images/sport/sport_3.jpg" alt="" />
			                <img src="images/sport/sport_1.png" alt="" />
			                {{-- <img src="images/sport/sport_2.png" alt="" /> --}}
			            </div>
			        </div>
			    </div>
			</div>
			<br />
			<br />
			<a target="_blank" href="{{ \App\Game::find(3)->desktop_link }}" class="btn btn-warning btn-block">PLAY NOW</a>
			<br /><br />
		</div>
	</div>
</div>
    
@endsection
