@extends ('front.master')
@section('content')


<div style="display:none;">
    Banking
</div>
<div id="ctl00_cphBody_bg" class="banking">
    <div class="uni-title">
        <img src="/images/splash.png" alt=""/>
        <p>RESET PAS</p>
    </div>
    <center>
        <form method="POST" action="{{ route('password.request') }}"">
                                    @csrf

                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="form-group">
                                        <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>
                                        
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <input id="password" placeholder="New Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

                                        @if ($errors->has('password_confirmation'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    
                                    <div class="form-group login-submit">
                                        <button type="submit" class="btn btn-primary btn-xl">Reset Password</button>
                                    </div>
                                </form>
        
    </center>
</div>
<div class="PReg">
    <p>Not our member yet?</p>
    <a href="{{ route('register') }}" class="btn">Register Now</a>
</div>

    
@endsection
