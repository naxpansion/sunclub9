@extends ('front.master')
@section('content')


<div style="display:none;">
    Banking
</div>
<div id="ctl00_cphBody_bg" class="banking">
    <div class="uni-title">
        <img src="/images/splash.png" alt=""/>
        <p>RESET PASSWORD</p>
    </div>
    <center>
        <div class="r-content">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            <form method="POST" action="{{ url('password_reset') }}">
                @csrf
                <p style="color: white;">Please enter your registered phone number to reset your password:</p>
                <input id="phone" type="text" class="phonemask" name="phone" value="{{ old('phone') }}" required>
                <br />
                <span class="errors" style="color: red; margin-left: 10px;">
                    @if ($errors->has('phone'))
                        {{ $errors->first('phone') }}
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </span>
                    
                </div>
                <br />
                <div class="clearfix">
                    <input type="submit" name="btnsend" value="Request New Password" id="btnsend" class="btn btn-warning btn-more pull-right" />
                    </form>
                </div>
                <br />
                <br />
                <br />
        
    </center>
</div>

    
@endsection
@section('script')
<script type="text/javascript" src="{{ secure_asset('plugins/jquery.mask.js') }}"></script>
<script type="text/javascript">
    $(".phonemask").mask('600000000000');
</script>
@endsection