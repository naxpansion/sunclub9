@extends('mobile.master')

@section('content')        

        <div class="all-elements">

            @include ('mobile.sidebar')
        
            <div id="content" class="page-content">
        
            @include ('mobile.nav')	
                        
            <div>
                <div id="ctl00_UpdatePanel1">
            
               
                <div class="container bg-dark">
                    <h2 style="text-align: center;">Banking</h2>

                    <div class="form-register banking-table-holder">
                        <h4>Deposit</h4>
                        <div class="table-responsive no-bottom">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table promo-table" data-snap-ignore="true">
                                <tbody>
                                    <tr>
                                        <th style="width: 40%;">Banking Methods</th>
                                        <th>Minimum</th>
                                        <th>Maximum</th>
                                        <th style="width: 23%;">Processing Time</th>
                                    </tr>
                                    <tr>
                                        <td>Online transfer &amp; ATM &amp; CDM</td>
                                        <td>MYR 30</td>
                                        <td>MYR 100,000</td>
                                        <td>3-10 minutes</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <h4>Withdrawal</h4>
                        <div class="table-responsive no-bottom">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table promo-table" data-snap-ignore="true">
                                <tbody>
                                    <tr>
                                        <th>Banking Methods</th>
                                        <th>Minimum</th>
                                        <th>Maximum</th>
                                        <th>Processing Time</th>
                                    </tr>
                                    <tr>
                                        <td>Online transfer & ATM & CDM</td>
                                        <td>MYR 50</td>
                                        <td>MYR 10,000</td>
                                        <td>15 minutes</td>
                                    </tr>
                                    <tr>
                                        <td>Online transfer & ATM & CDM</td>
                                        <td colspan="2">MYR 10,001&emsp;&emsp;to&emsp;&emsp;MYR 100,000</td>
                                        <td>12 hours</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br><br />
                        <ol class="padding-style9">
                            <li>Withdrawals will only be approve and release into the member’s bank account bearing the same name used to register at sunclub9.</li>
                            <li>Large withdrawal amount will take longer processing time.</li>
                            <li>Member is required to meet at least one (1) time turnover of the deposited amount before withdrawal can be made.</li>
                            <li>Withdrawal is not allowed if the rollover requirements for any bonus claimed have not been fulfilled.</li>
                            <li>Please retain the Bank Receipt (if fund deposited via ATM) or Transaction Reference Number (if fund deposited via internet banking) as we will request for the proof of deposit.</li>
                            <li>All deposits and withdrawals processing time are subject to internet banking availability.</li>
                            <li>For other Local Banks, please liaise with our Customer Service via live chat for more info.</li>
                            <li>Please refer to the terms and condition for more details.</li>
                            <li>Receipt will be forfeit after 24 hours.</li>                                       
                        </ol>				
                    </div>
                </div>
                
        
                
        </div>
        <div id="ctl00_UpdateProgress1" style="display:none;">
            
        <div class="overlay" />
                    <div class="overlayContent">
                        <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                    </div>
            
        </div>
            </div>
        
            @include ('mobile.footer')
                            
            </div>  
            
        </div>
    
@endsection