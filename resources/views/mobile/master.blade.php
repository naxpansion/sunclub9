

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

@include ('mobile.header')

<body >
    
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KG3SGWL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    @yield ('content')

    @include ('mobile.modal')

    @yield ('scripts')

<!-- Start of LiveChat (www.livechatinc.com) code -->
<div id="info-popup" class="spin-wheel mfp-hide" style="background-image: url({{ url('images/common/Casino_rouletter_wheel.png')  }});">
    <div class="row">
        <div class="col-md-12" style="text-align: center;">
            <h2>Welcome To MBET11</h4>
            <br />
            <p>
                Dear VIP members, previous whatsapp (+6016-4318615) sudah berhenti, please contact us via the new whatsapp no. (017-6979611) or <a style="color:white;" href="https://api.whatsapp.com/send?phone={{ \App\Setting::find(8)->value }}">Klik disini</a>
            </p>
        </div>
    </div>
</div>
<div id="spin-wheel" class="spin-wheel mfp-hide" style="background-image: url({{ url('wheel/bg.jpeg')  }});">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <p>Every visitor are awarded with 1 free spin</p>
                <p>Simply click on the 'start spin' to try your luck</p>
                <p>Good Luck!</p>
            </div>
        </div>
        <div id="mobile-canvasContainer">
            <canvas id='spinner' width='330' height='330' class="mobile-spinner">
                Canvas not supported, use another browser.
            </canvas>
            <img id="mobile-prizePointer" src="{{ url('wheel/mobile_arrow.png') }}" alt="V" />
        </div>
        <br />
        <div class="row">
            <div class="col-md-8">
                <button id="spin_button" class="btn btn-block btn-lg btn-warning" onClick="startSpin();">Start Spin</button>
                <button id="reset_button" style="display: none;" class="btn btn-block btn-lg btn-warning" onClick="resetWheel(); return false;">Play Again</button>
            </div>
            <div class="col-md-4">
                <table class="table table-bordered" style="background-color: rgba(0,0,0,0.5);">
                    <tr>
                        <td style="vertical-align: middle;">TOKEN REMAINING</td>
                        @auth
                            <td style="text-align: center;"><h4 id="token">{{ \Auth::user()->token }}</h4></td>
                        @else
                            @php
                                $token_check = \App\SpinIp::where('ip',\Request::ip())->count();
                                if($token_check == 0)
                                {
                                    $token = 1;
                                }
                                else
                                {
                                    $token = 0;
                                }
                            @endphp
                            <td style="text-align: center;"><h4 id="token">{{ $token }}</h4></td>
                        @endauth
                    </tr>
                </table>
            </div>
        </div>
    </div>
<script src="https://mbet11.com/plugins/jquery.magnific-popup.js"></script>
<script src="https://mbet11.com/plugins/winwheel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script type="text/javascript" src="{{ secure_asset('lib/js/nivo/jquery.nivo.slider.js') }}"></script>
<script type="text/javascript">
    //document.getElementById("bank_container").style.display = "none";
    $(window).load(function () {
        $('.homeslider').nivoSlider({ controlNav: false, effect: 'fade' });
        //Hide Nivo Left Right Nav if only one image
        var count_nivoimg = $(".nivoSlider>img").length //count children with img tag
        if (count_nivoimg <= 2) {
            $(".nivoSlider .nivo-directionNav").hide();
        } 
    })
</script>
<script type="text/javascript">
                
                $('form').submit(function() {
                    $(this).find("button[type='submit']").prop('disabled',true);
                });
                
                
                var spinPopup = $('#mini_spin').magnificPopup({
                    items: {
                        src: '#spin-wheel'
                    },
                    type: 'inline'
                }, 0);

                // let theWheel = new Winwheel({
                //     'canvasId'        : 'spinner',
                //     'drawMode'     : 'image',    // drawMode must be set to image.
                //     'numSegments'  : 8,          // The number of segments must be specified.
                //     'imageOverlay' : true,       // Set imageOverlay to true to display the overlay.
                //     'lineWidth'    : 1,          // Overlay uses wheel line width and stroke style so can set these
                //     'strokeStyle'  : 'red'       // as desired to change appearance of the overlay.
                // });

                let theWheel = new Winwheel({
                    'canvasId'        : 'spinner', 
                    'drawMode'     : 'image', 
                    'responsive'      : true,
                    'outerRadius'     : 212,        // Set outer radius so wheel fits inside the background.
                    'innerRadius'     : 75,         // Make wheel hollow so segments dont go all way to center.
                    'drawText'          : false,         // Need to set this true if want code-drawn text on image wheels.
                    // 'textFontSize'      : 18,
                    // 'textOrientation'   : 'curved',     // Note use of curved text.
                    // 'textDirection'     : 'reversed',   // Set other text options as desired.
                    // 'textAlignment'     : 'outer',
                    // 'textMargin'        : 20,   // Align text to outside of wheel.
                    'numSegments'     : 8,         // Specify number of segments.
                    'segments'        :             // Define segments including colour and text.
                    [                               // font size and text colour overridden on backrupt segments.
                       @foreach(\App\Wheel::all() as $wheel)
                            {'fillStyle' : '#ee1c24', 'text' : '{{ $wheel->name }}'},
                       @endforeach
                    ],
                    'animation' :           // Specify the animation to use.
                    {
                        'type'     : 'spinToStop',
                        'duration' : 10,
                        'spins'    : 3,
                        'callbackFinished' : alertPrize,  // Function to call whent the spinning has stopped.
                        'callbackSound'    : playSound,   // Called when the tick sound is to be played.
                        'soundTrigger'     : 'pin'        // Specify pins are to trigger the sound.
                    },
                    'pins' :                // Turn pins on.
                    {
                        'visible'    : false,
                        'responsive' : true,
                        'number'     : 24,
                        'fillStyle'  : 'silver',
                        'outerRadius': 4,
                    }
                });

                // Create new image object in memory.
                let loadedImg = new Image();
                 
                // Create callback to execute once the image has finished loading.
                loadedImg.onload = function()
                {
                    theWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
                    theWheel.draw();                    // Also call draw function to render the wheel.
                }
                 
                // Set the image source, once complete this will trigger the onLoad callback (above).
                loadedImg.src = "{{ secure_asset('wheel/mobile_spin.png') }}";


                // Loads the tick audio sound in to an audio object.
                    let audio = new Audio('{{ secure_asset('wheel/tick.mp3') }}');

                    // This function is called when the sound is to be played.
                    function playSound()
                    {
                        // Stop and rewind the sound if it already happens to be playing.
                        audio.pause();
                        audio.currentTime = 0;

                        // Play the sound.
                        audio.play();
                    }

                    // Vars used by the code in this page to do power controls.
                    let wheelPower    = 0;
                    let wheelSpinning = false;

                    // -------------------------------------------------------
                    // Click handler for spin button.
                    // -------------------------------------------------------
                    function startSpin()
                    {
                        var token = parseInt($("#token").text());

                        if(token < 1)
                        {
                            @auth

                            swal("You don't have any token available, simply make a deposit to entitle a free spin token.", {
                                buttons: {
                                    cancel: "Deposit",
                                },
                                icon: "warning",
                                title: "Whoops!!!!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ route('register') }}');
                                        break;

                                    default:
                                        window.location.replace('{{ url('player/deposit/step1') }}');
                                        
                                }
                            });

                            @else

                            swal("You don't have any token available, simply make a deposit to entitle a free spin token.", {
                                buttons: {
                                    cancel: "Login",
                                    catch: {
                                        text: "Register",
                                        value: "register",
                                    },
                                },
                                icon: "warning",
                                title: "Whoops!!!!",
                            })
                            .then((value) => {
                                switch (value) {

                                    case "register":
                                        window.location.replace('{{ route('register') }}');
                                        break;

                                    default:
                                        var magnificPopup = $.magnificPopup.instance; 
                                        magnificPopup.close();
                                        $('input[name="email"]').focus();
                                        $(".login-here").show();
                                        
                                }
                            });

                            @endauth
                            
                        }
                        else
                        {
                            if (wheelSpinning == false)
                            {

                                var d = Math.random();
                                console.log(d);

                                var section_1 = {{ \App\Wheel::find(1)->percentage }} * 0.01;
                                var section_2 = {{ \App\Wheel::find(2)->percentage }} * 0.01;
                                var section_3 = {{ \App\Wheel::find(3)->percentage }} * 0.01;
                                var section_4 = {{ \App\Wheel::find(4)->percentage }} * 0.01;
                                var section_5 = {{ \App\Wheel::find(5)->percentage }} * 0.01;
                                var section_6 = {{ \App\Wheel::find(6)->percentage }} * 0.01;
                                var section_7 = {{ \App\Wheel::find(7)->percentage }} * 0.01;
                                var section_8 = {{ \App\Wheel::find(8)->percentage }} * 0.01;

                                if (d < section_1)
                                {
                                    var prize = 23; //Try again
                                }
                                else if (d < section_1 + section_2)
                                {
                                    var prize = 70; //RM8
                                }
                                else if (d < section_1 + section_2 + section_3)
                                {
                                    var prize = 121; //30% Daily
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4)
                                {
                                    var prize = 160; //Jackpot
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5)
                                {
                                    var prize = 200; //30% Daily
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6)
                                {
                                    var prize = 260; //RM4
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6 + section_7)
                                {
                                    var prize = 290; //RM4
                                }
                                else if (d < section_1 + section_2 + section_3 + section_4 + section_5 + section_6 + section_7 + section_8)
                                {
                                    var prize = 340; //RM4
                                }
                                // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                                // to rotate with the duration of the animation the quicker the wheel spins.
                                theWheel.animation.spins = 6;

                                // Disable the spin button so can't click again while wheel is spinning.
                                $("#spin_button").hide();

                                // Begin the spin animation by calling startAnimation on the wheel object.
                                theWheel.animation.stopAngle = prize;
                                theWheel.startAnimation();

                                // Set to true so that power can't be changed and spin button re-enabled during
                                // the current animation. The user will have to reset before spinning again.
                                wheelSpinning = true;

                                $.post("{{ url('api/spin_ip') }}", { 'ip':'{{ \Request::ip() }}'}, function(result,status){
                                    console.log(result);
                                });

                                var new_token = token - 1;
                                $("#token").text(new_token);


                            }
                        }

                        
                    }

                    // -------------------------------------------------------
                    // Function for reset button.
                    // -------------------------------------------------------
                    function resetWheel()
                    {
                        theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                        theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                        theWheel.draw();                // Call draw to render changes to the wheel.

                        wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
                        $("#spin_button").show();
                        $("#reset_button").hide();
                    }

                    // -------------------------------------------------------
                    // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
                    // -------------------------------------------------------
                    function alertPrize(indicatedSegment)
                    {
                        // Just alert to the user what happened.
                        // In a real project probably want to do something more interesting than this with the result.
                        // alert("You have won " + indicatedSegment.endAngle);
                        @auth
                            $.post("{{ url('api/reward/auth') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                                    console.log(result);
                                });

                            if(indicatedSegment.endAngle == 45)
                            {
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(1)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 90)
                            {
                                swal("To claim this reward please make a deposit.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(2)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/deposit/step1') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 135)
                            {
                                swal("You can try again by making any deposit to us!", {
                                    buttons: {
                                        cancel: "Try Again",
                                        catch: {
                                            text: "Get Token By Deposit",
                                            value: "register",
                                        },
                                    },
                                    icon: "warning",
                                    title: "Awww! We Are So Sorry :(",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ url('player/deposit/step1') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 180)
                            {
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(4)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 225)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(5)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 270)
                            {
                                
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(6)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }

                            else if(indicatedSegment.endAngle == 315)
                            {
                                
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(7)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }
                            
                            else if(indicatedSegment.endAngle == 360)
                            {
                                swal("To claim this reward please click button below.", {
                                    buttons: {
                                        catch: {
                                            text: "Claim",
                                            value: "claim",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(8)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "claim":
                                            window.location.replace('{{ url('player/rewards') }}');
                                            break;

                                        default:
                                            
                                    }
                                });
                            }
                        @else

                            $.post("{{ url('api/reward/guest') }}", { 'reward':indicatedSegment.endAngle}, function(result,status){
                                    console.log(result);
                                });

                            if(indicatedSegment.endAngle == 45)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(1)->name }}!!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 90)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(2)->name }}",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 135)
                            {
                                swal("You can try again by making any deposit to us!", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "warning",
                                    title: "Awww! We Are So Sorry :(",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 180)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(4)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 225)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(5)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 270)
                            {
                                
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(6)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 315)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(7)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                            
                                    }
                                });
                            }
                            else if(indicatedSegment.endAngle == 360)
                            {
                                swal("To claim this reward please Login / Register! You can track your reward in your dashboard under [reward] menu.", {
                                    buttons: {
                                        cancel: "Login",
                                        catch: {
                                            text: "Register",
                                            value: "register",
                                        },
                                    },
                                    icon: "success",
                                    title: "You Won {{ App\Wheel::find(8)->name }}!",
                                })
                                .then((value) => {
                                    switch (value) {

                                        case "register":
                                            window.location.replace('{{ route('register') }}');
                                            break;

                                        default:
                                            var magnificPopup = $.magnificPopup.instance; 
                                            magnificPopup.close();
                                            $('input[name="email"]').focus();
                                            $(".login-here").show();
                                            
                                    }
                                });
                            }
                        @endauth
                        $("#reset_button").show();
                    }

    $('.slider-sport').bxSlider({
        auto: false,
        speed: 1000,
        pause: 7000,
        responsive: false,
    });
            </script>
    <!-- Start of LiveChat (www.livechatinc.com) code -->
           <script type="text/javascript">
             window.__lc = window.__lc || {};
             window.__lc.license = 11156872;
             (function() {
               var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
               lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
               var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
             })();
           </script>
           <noscript>
           <a href="https://www.livechatinc.com/chat-with/11156872/" rel="nofollow">Chat with us</a>,
           powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
           </noscript>
           <!-- End of LiveChat code -->

    <script type="text/javascript">
    (function(p,u,s,h){
        p._pcq=p._pcq||[];
        p._pcq.push(['_currentTime',Date.now()]);
        s=u.createElement('script');
        s.type='text/javascript';
        s.async=true;
        s.src='https://cdn.pushcrew.com/js/82e2eba1f1d8b74281f74203195cf20c.js';
        h=u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s,h);
    })(window,document);
  </script>

</body>
</html>

<!-- Localized -->