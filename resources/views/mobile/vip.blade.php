@extends('mobile.master')

@section('content')        
            
        <div class="all-elements">

                @include ('mobile.sidebar')
        
            <div id="content" class="page-content">
        
                @include ('mobile.nav')
                        
            <div>

                    <div id="ctl00_UpdatePanel1">

                        <div class="container bg-dark">
                            <div class="row">
                                <div class="col-md-12 center">
                                    <h2 style="text-align: center;">VIP Club</h2>
                                    <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td style="width:30%;"><img src="/images/vip-badge-1.png" alt="" style="width:60%" /></td>
                                            <td><img src="/images/vip-badge-silver.png" alt="" /></td>
                                            <td><img src="/images/vip-badge-gold.png" alt="" /></td>
                                            <td><img src="/images/vip-badge-plat.png" alt="" /></td>
                                            <td><img src="/images/vip-badge-diam.png" alt="" /></td>
                                            <td><img src="/images/vip-badge-sig.png" alt="" /></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%;text-align:left;">Total Deposit More Than</td>
                                            <td>MYR 30</td>
                                            <td>MYR 10,000</td>
                                            <td>MYR 50,000</td>
                                            <td>MYR 100,000</td>
                                            <td>MYR 150,000</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;font-weight:bold !important;">Withdraw limit per day</td>
                                            <td>3</td>
                                            <td>4</td>
                                            <td>5</td>
                                            <td>7</td>
                                            <td>Unlimited</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;font-weight:bold !important;">Birthday Surprise</td>
                                            <td>NO</td>
                                            <td>MYR 100</td>
                                            <td>MYR 500</td>
                                            <td>MYR 1,000</td>
                                            <td>MYR 2,000</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <ol>
                                        <li>To redeem BIRTHDAY SURPRISE, member must send an enquiry to our Customer Service's Livechat, Whatsapp or WeChat.</li>
                                        <li>Docs Requirement: Username and attach a copy of NRIC/Passport for verification.</li>
                                        <li>The birthday bonus amount has to be rolled over 1 time before withdrawal can be made.</li>
                                        <li>Deposit and Bonus claimed cannot be wagered in 918Kiss and Mega888 CASINO.</li>
                                        <li>The BIRTHDAY SURPRISE is offered limited to individual member, family, account, email address, contact number, bank account or IP address. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
                                        <li>Deposit and Bonus claimed cannot be transferred to another game account until and unless the rollover requirement has been met.</li>
                                        <li>After redeem birthday bonus, member VIP tag will restart from Silver again.</li>
                                    </ol>
                                </div>
                            </div>
                            <br />
                            <br />
                        </div>

                    </div>

        <div id="ctl00_UpdateProgress1" style="display:none;">
            
        <div class="overlay" />
                    <div class="overlayContent">
                        <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                    </div>
            
        </div>
            </div>
        
            @include ('mobile.footer')
                            
            </div>  
            
        </div>

@endsection