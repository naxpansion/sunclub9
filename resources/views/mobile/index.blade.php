@extends('mobile.master')
@section('content')
<div class="all-elements">
@include ('mobile.sidebar')
<div id="content" class="page-content">
    @include ('mobile.nav')
        <div id="" style="z-index: 99999;">   
            <a href="{{ url('promotions') }}"><img id="#" src="{{ asset('images/slides/1.png') }}" class="img-responsive width-full"/></a>
        </div>
        <div id="ctl00_UpdatePanel1">
            
            <span style="padding-top: 2vh;">&nbsp;</span>
            <div class="row">
                <div class="col-xs-12 slider-sport-bg">
                    <h5 style="padding-left: 15px;">Sports</h5>
                    <div class="slider-sport" style="margin-top:27px;" id="matchcontent">
                        @php
                            $sports = \App\Sport::orderBy('match_time','ASC')->whereDate('match_time', '>=', \Carbon\Carbon::now())->get();
                        @endphp
                        @foreach($sports as $sport)
                            <div class="slide">
                                <div class="row" style="margin:0;">
                                    <div class="col-xs-4 text-center team">
                                        <img src="{{ url('storage/matches/'.$sport->team_1_logo) }}" class="flag">
                                        <h6>{{ $sport->team_1_name }}</h6>
                                        <p>{{ $sport->team_1_rating }}</p>
                                    </div>
                                    <div class="col-xs-4 text-center team">
                                        <img style="width:100%;" src="https://wc88xml.wc365.net/images/maxlogo.png"><a href="sport-maxbet.html">
                                        <a href="https://mbet11.com/downloads#maxbet"><img class="btn-bet" style="padding-bottom: 8px;" src="https://88659552w8.cdnasiaclub.com/wcv2/images/MY/english/bet-btn.png"></a>
                                        <p style="color:#9a9a9a;font-family: Conv_Montserrat-Light;line-height: 21px!important;">{{ $sport->match_time->format('d M Y') }}<br>{{ $sport->match_time->format('h:i A') }} (GMT+8)</p>
                                    </div>
                                    <div class="col-xs-4 text-center team">
                                        <img src="{{ url('storage/matches/'.$sport->team_2_logo) }}" class="flag">
                                        <h6>{{ $sport->team_2_name }}</h6>
                                        <p>{{ $sport->team_2_rating }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <span style="padding-top: 2vh;">&nbsp;</span>
            
            
        </div>
        <div id="ctl00_UpdateProgress1" style="display:none;">
            <div class="overlay" />
                <div class="overlayContent">
                    <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                </div>
            </div>
        </div>
        @include ('mobile.footer')
    </div>
</div>
<div id="popup-annouce" class="spin-wheel mfp-hide" >
    <div class="row">
        <div class="col-md-12" style="text-align: center;">
            @foreach(\App\Annoucement::all() as $annoucement)
                <img src="{{ url('storage/image/'.$annoucement->image) }}" width="100%">
            @endforeach
        </div>
    </div>
</div>
@php
    $announcement = \App\Annoucement::all()->count();
@endphp
@if($announcement == 0)

@else
    <script type="text/javascript">
        (function($) {
            $(window).load(function () {
                // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
                $.magnificPopup.open({
                    items: {
                        src: '#popup-annouce'
                    },
                    type: 'inline'

                  // You may add options here, they're exactly the same as for $.fn.magnificPopup call
                  // Note that some settings that rely on click event (like disableOn or midClick) will not work here
                }, 0);
            });
        })(jQuery);
    </script>
@endif
@endsection

@section('scripts')
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="{{ asset('mobile/js/slick.min.js') }}"></script>
    <script src="https://shop.tomac.com.my/js/sticky-kit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
        $("#ms_slider").stick_in_parent();
    </script>
    <script>
    
    $('#mb-slider').slick();

    // (function($) {
    //     $(window).load(function () {

    //         $.magnificPopup.open({
    //             items: {
    //                 src: '#spin-wheel'
    //             },
    //             type: 'inline'
    //         }, 0);
    //     });
    // })(jQuery);

    // (function($) {
    //     $(window).load(function () {

    //         $.magnificPopup.open({
    //             items: {
    //                 src: '#info-popup'
    //             },
    //             type: 'inline'
    //         }, 0);
    //     });
    // })(jQuery);

    $('.slider-sport').bxSlider({
          auto: true,
          speed: 1000,
          pause: 5000,
          responsive: false,
    });

    var slider_hot = $('.slider-hot').bxSlider({
          auto: true,
          speed: 1000,
          pause: 5000,
          responsive: false,
          // wrapperClass: 'hotgame-wrapper'
    });

    var slider_new = $('#slider-newgame').bxSlider({
          auto: true,
          speed: 1000,
          pause: 5000,
          responsive: false,
          // wrapperClass: 'hotgame-wrapper'
    });

    var slider_prog = $('#slider-progs').bxSlider({
          auto: true,
          speed: 1000,
          pause: 5000,
          responsive: false,
          // wrapperClass: 'hotgame-wrapper'
    });

    var slider_fea = $('#slider-fea').bxSlider({
          auto: true,
          speed: 1000,
          pause: 5000,
          responsive: false,
          // wrapperClass: 'hotgame-wrapper'
    });

    $(".link-hot").on("click",function(){
        setTimeout(function() {
            slider_hot.redrawSlider();
        }, 0);
    });

    $(".link-new").on("click",function(){  
        setTimeout(function() {
            slider_new.redrawSlider();
        }, 0); 
    });

    $(".link-prog").on("click",function(){
        setTimeout(function() {
            slider_prog.redrawSlider();
        }, 0);
    });

    $(".link-fea").on("click",function(){  
        setTimeout(function() {
            slider_fea.redrawSlider();
        }, 0); 
    });
    
    </script>
    <script type="text/javascript" src="{{ secure_asset('plugins/jquery.mask.js') }}"></script>
    <script type="text/javascript">
        $(".phonemask").mask('601000000000');
    </script>

@endsection