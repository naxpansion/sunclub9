@extends('mobile.master')
@section('content')        
<div class="all-elements">
    @include ('mobile.sidebar')
    <div id="content" class="page-content">
        @include ('mobile.nav') 
        <div>
            <div id="ctl00_UpdatePanel1">
                <div class="container bg-dark">
                    <ul id="tabmenu">
                        <li class=""><a href="{{ url('player') }}">Main</a></li>
                        <li><a href="{{ url('player/deposit/step1') }}" >Deposit</a></li>
                        <li><a href="{{ url('player/withdrawal/step1') }}" class="deposit">Withdrawal</a></li>
                        <li><a href="{{ url('player/transfer/step1') }}" class="rebate">Transfer</a></li>
                        <li><a href="{{ url('player/transaction') }}" class="special">Transaction</a></li>
                        <li class="active"><a href="{{ url('player/rewards') }}" class="special">Rewards</a></li>
                        <li><a href="{{ url('player/profile') }}" class="scr888">Profile</a></li>
                    </ul>
                    <div id="ourHolder" class="promo-box">
                        <div id="ctl00_MainContent_UpdatePanel1">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="deposit">
                                    @if(Session::has('message'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                    @endif
                                    <h5>Reward List's</h5>
                                    <div>
                                        <div class="table-responsive no-bottom">
                                            <table cellspacing="0" border="0" style="width:100%;border-collapse:collapse; color: white;" class="table table-bordered">
                                                <tr>
                                                    <th scope="col">Reward</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col" width="10%">Game</th>
                                                    <th scope="col" width="10%">Remarks</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                                @if($rewards->count() == 0)
                                                <tr >
                                                    <th scope="col" colspan="7" style="padding-top: 10px; text-align: center">No Reward</th>
                                                </tr>
                                                @else
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach($rewards as $reward)
                                                        @if($reward->reward == "Try Again")
                                                            
                                                        @else
                                                            <tr>
                                                                <td scope="col">{{ $reward->reward }}</td>
                                                                <td scope="col">
                                                                    @if($reward->status == 1)
                                                                        <span class="label label-info">Pending</span>
                                                                    @elseif($reward->status == 3)
                                                                        <span class="label label-success">Approved</span>
                                                                    @elseif($reward->status == 2)
                                                                        <span class="label label-warning">In Progress</span>
                                                                    @elseif($reward->status == 4)
                                                                        <span class="label label-danger">Reject</span>
                                                                    @endif
                                                                </td>
                                                                <td>{{ $reward->game->name ?? '-' }}</td>
                                                                <td>{{ $reward->remarks ?? '-' }}</td>
                                                                <td scope="col">{{ $reward->created_at->format('d M Y, h:iA') }}</td>
                                                                <td scope="col">
                                                                    @if($reward->reward_is_percentage == 0)
                                                                        @if($reward->status == 1)
                                                                            <a href="{{ url('player/rewards/claim/'.$reward->id) }}"><span class="label label-default">Click Here To Claimed</span></a>
                                                                        @else

                                                                        @endif
                                                                    @else
                                                                        @if($reward->status == 1)
                                                                            <a href="{{ url('player/deposit/step1') }}"><span class="label label-default">Click Here To Claimed</span></a>
                                                                        @else

                                                                        @endif
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @php
                                                        $i++;
                                                    @endphp
                                                    @endforeach

                                                @endif
                                            </table>
                                            {{ $rewards->links() }}
                                            <h5>Terms & Condition</h5>
                                            <ol>
                                                <li>Members can deposit UNLIMITED time per day to earn UNLIMITED game token. No maximum limit on the total deposits per day, per month or per year.</li>
                                                <li>Members can enjoy this promotion with any other promotion available unless otherwise stated.</li>
                                                <li>The prize amount has to be rollover at least 1x time before withdrawal can be made.</li>
                                                <li>sunclub9 reserved the rights to cancel this promotion at any time, either for all players or individual player without prior notice.</li>
                                                <li>To ensure fairness, all randomly awarded prizes are determined by our certified RNG (Random Number Generator), which is a computational device designed to generate a sequence of numbers or symbols that lac k any pattern and cannot be predicted based on a known sequence, so all final results will be based on the backend system’s report.</li>
                                                <li>General Terms of Use specified on this site applies to all promotions.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                    </div>
                </div>
            </div>
        </div>
        <div id="ctl00_UpdateProgress1" style="display:none;">
            <div class="overlay" />
                <div class="overlayContent">
                    <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                </div>
            </div>
        </div>
        @include ('mobile.footer')
    </div>
</div>
@endsection
@section('scripts')
@endsection