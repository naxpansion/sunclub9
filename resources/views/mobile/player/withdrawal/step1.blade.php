@extends('mobile.master')
@section('content')        
<div class="all-elements">
    @include ('mobile.sidebar')
    <div id="content" class="page-content">
        @include ('mobile.nav') 
        <div>
            <div id="ctl00_UpdatePanel1">
                <div class="container bg-dark">
                    <ul id="tabmenu">
                        <li class=""><a href="{{ url('player') }}">Account</a></li>
                        <li class=""><a href="{{ url('player/deposit/step1') }}" >Deposit</a></li>
                        <li class="active"><a href="{{ url('player/withdrawal/step1') }}" class="deposit">Withdrawal</a></li>
                        <li><a href="{{ url('player/transfer/step1') }}" class="rebate">Transfer</a></li>
                        <li><a href="{{ url('player/transaction') }}" class="special">Transaction</a></li>
                        <li><a href="{{ url('player/rewards') }}" class="special">Rewards</a></li>
                        <li><a href="{{ url('player/profile') }}" class="scr888">Profile</a></li>
                    </ul>
                    <div id="ourHolder" class="promo-box">
                        <div class="tab-content short">
                            <div class="tab-pane fade in active">
                                <div class="title-wrap">
                                    <div class="outer-mask">
                                        <div class="inner-mask">
                                            <div class="title-p">
                                                <h2 class="smaller">Withdraw</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="member-main start-game">
                                    <div class="member-row clearfix">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                                            <tr>
                                                <td style="width: 200px;">Bank Name</td>
                                                <td>{{ \Auth::user()->bank_name }}</td>
                                            </tr>
                                            <tr>
                                                <td>Account Name</td>
                                                <td>{{ \Auth::user()->name }}</td>
                                            </tr>
                                            <tr>
                                                <td>Account Number</td>
                                                <td>{{ \Auth::user()->bank_account_no }}</td>
                                            </tr>
                                        </table>
                                        <form method="post" action="{{ url('player/withdrawal/step3') }}">
                                        @csrf
                                        <div class="table-responsive">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table borderless" style="color: white;">
                                                <tr>
                                                    <td width="25%" class="align-middle">Product Game</td>
                                                    <td>
                                                        <select name="game_id" class="form-control form-control select">
                                                            <option value="">Select Product</option>
                                                            <optgroup label="SportBooks">
                                                                @php
                                                                    $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                                                @endphp
                                                                @foreach($games as $game)
                                                                    @php
                                                                        $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                                                    @endphp
                                                                    @if($account)
                                                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </optgroup>
                                                            <optgroup label="Live Casino">
                                                                @php
                                                                    $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                                                @endphp
                                                                @foreach($games as $game)
                                                                    @php
                                                                        $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                                                    @endphp
                                                                    @if($account)
                                                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </optgroup>
                                                            <optgroup label="Slots">
                                                                @php
                                                                    $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                                                @endphp
                                                                @foreach($games as $game)
                                                                    @php
                                                                        $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                                                    @endphp
                                                                    @if($account)
                                                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </optgroup>

                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Withdraw Amount</td>
                                                    <td>
                                                        <input name="amount" type="number" min="{{ \App\Setting::find(12)->value }}" max="{{ \App\Setting::find(13)->value }}" step="0.01" class="form-control" value="{{ old('amount') }}" required />
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                             <button type="submit" class="btn btn-warning btn-more pull-right">NEXT</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="ctl00_UpdateProgress1" style="display:none;">
            <div class="overlay" />
                <div class="overlayContent">
                    <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                </div>
            </div>
        </div>
        @include ('mobile.footer')
    </div>
</div>
<script type="text/javascript">
    
    $('#now').click(function(){
        
        $('#deposit_date').val('{{ \Carbon\Carbon::now()->format('d/m/Y') }}');
        $('#deposit_hour').val('{{ \Carbon\Carbon::now()->format('h') }}');
        $('#deposit_minutes').val('{{ \Carbon\Carbon::now()->format('i') }}');
        $('#deposit_stamp').val('{{ \Carbon\Carbon::now()->format('A') }}');

    });

    $("#bonus_select").on('change',function(){
        if($("#bonus_select").val() == '')
        {
            $("#reward_select").prop("disabled", false);
        }
        else
        {
            $("#reward_select").prop("disabled", true);
        }
    })

    $("#reward_select").on('change',function(){
        if($("#reward_select").val() == '')
        {
            $("#bonus_select").prop("disabled", false);
        }
        else
        {
            $("#bonus_select").prop("disabled", true);
        }
    })

</script>
<script type="text/javascript">
    $(".datepicker").datepicker({
            dateFormat: "dd/mm/yy",
            showOn: 'both',
            buttonImage: "{{ secure_asset('images/date.png') }}",
            buttonText: "Open datepicker",
            buttonImageOnly: true,
            showAnim: 'slideDown',
            duration: 'fast',
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });
</script>
@endsection
@section('scripts')
@endsection