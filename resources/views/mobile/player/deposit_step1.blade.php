@extends('mobile.master')
@section('content')        
<div class="all-elements">
    @include ('mobile.sidebar')
    <div id="content" class="page-content">
        @include ('mobile.nav') 
        <div>
            <div id="ctl00_UpdatePanel1">
                <div class="container bg-dark">
                    <ul id="tabmenu">
                        <li class=""><a href="{{ url('player') }}">Account</a></li>
                        <li class="active"><a href="{{ url('player/deposit/step1') }}" >Deposit</a></li>
                        <li><a href="{{ url('player/withdrawal/step1') }}" class="deposit">Withdrawal</a></li>
                        <li><a href="{{ url('player/transfer/step1') }}" class="rebate">Transfer</a></li>
                        <li><a href="{{ url('player/transaction') }}" class="special">Transaction</a></li>
                        <li><a href="{{ url('player/rewards') }}" class="special">Rewards</a></li>
                        <li><a href="{{ url('player/profile') }}" class="scr888">Profile</a></li>
                    </ul>
                    <div id="ourHolder" class="promo-box">
                        <div class="tab-content short">
                            <div class="tab-pane fade in active">
                                <div class="title-wrap">
                                    <div class="outer-mask">
                                        <div class="inner-mask">
                                            <div class="title-p">
                                                <h2 class="smaller">Deposit</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="member-main start-game">
                                    <div class="member-row clearfix">
                                        <form method="post" action="{{ url('player/deposit') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="table-responsive">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table borderless" style="color: white;">
                                                <tr>
                                                    <td width="25%" class="align-middle">Product Game</td>
                                                    <td>
                                                        <select name="game_id" class="form-control form-control select">
                                                            <option value="">Select Product</option>
                                                            <optgroup label="SportBooks">
                                                                @php
                                                                    $games = \App\Game::where('category','LIKE','%SportBooks%')->get();
                                                                @endphp
                                                                @foreach($games as $game)
                                                                    @php
                                                                        $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                                                    @endphp
                                                                    @if($account)
                                                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </optgroup>
                                                            <optgroup label="Live Casino">
                                                                @php
                                                                    $games = \App\Game::where('category','LIKE','%Live Casino%')->get();
                                                                @endphp
                                                                @foreach($games as $game)
                                                                    @php
                                                                        $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                                                    @endphp
                                                                    @if($account)
                                                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </optgroup>
                                                            <optgroup label="Slots">
                                                                @php
                                                                    $games = \App\Game::where('category','LIKE','%Slots%')->get();
                                                                @endphp
                                                                @foreach($games as $game)
                                                                    @php
                                                                        $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first();
                                                                    @endphp
                                                                    @if($account)
                                                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </optgroup>

                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Deposit Amount</td>
                                                    <td>
                                                        <input name="amount" type="number" min="{{ \App\Setting::find(5)->value }}" max="{{ \App\Setting::find(6)->value }}" step="0.01" class="form-control" value="{{ old('amount') }}" required />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Deposit Method</td>
                                                    <td class="">
                                                        <div class="sms"><input value="method_online" name="payment_method" type="radio" style="outline: none;" / checked="true" required><span>Online Transfer</span></div>
                                                        <div class="sms"><input value="method_atm" name="payment_method" type="radio" style="outline: none;" /><span>Atm Transfer</span></div>
                                                        <div class="sms"><input value="method_cash" name="payment_method" type="radio" style="outline: none;" /><span>Cash Deposit</span></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Deposit To Bank</td>
                                                    <td>
                                                        <select name="bank"  class="form-control form-control select" required>
                                                            <option value="">Select Bank</option>
                                                            @foreach($banks as $bank)
                                                                <option value="{{ $bank->id }}">{{ $bank->name }} - {{ $bank->account_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Date / Time</td>
                                                    <td>
                                                        <div style="float:left;">
                                                            <input name="deposit_date" id="deposit_date" type="text" maxlength="10" class="form-control datepicker" style="width: 100px;" value="{{ \Carbon\Carbon::now()->format('d/m/Y') }}" placeholder="31/12/2019" required/>
                                                        </div>
                                                        <div style="float:left;">
                                                            <div id="ctl00_MainContent_uptxtdatetime">
                                                                <div style="float:left; margin-right: 5px; ">
                                                                    <select name="deposit_hour" id="deposit_hour" class="form-control form-control select" style="width:55px;" required>
                                                                        <option value="00">00</option>
                                                                        <option value="01">01</option>
                                                                        <option value="02">02</option>
                                                                        <option value="03">03</option>
                                                                        <option value="04">04</option>
                                                                        <option value="05">05</option>
                                                                        <option value="06">06</option>
                                                                        <option value="07">07</option>
                                                                        <option value="08">08</option>
                                                                        <option value="09">09</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                </div>
                                                                <div style="float:left; margin-right: 5px; ">
                                                                    <select name="deposit_minutes" id="deposit_minutes" class="form-control form-control select" style="width:55px;" required>
                                                                        <option value="00">00</option>
                                                                        <option value="01">01</option>
                                                                        <option value="02">02</option>
                                                                        <option value="03">03</option>
                                                                        <option value="04">04</option>
                                                                        <option value="05">05</option>
                                                                        <option value="06">06</option>
                                                                        <option value="07">07</option>
                                                                        <option value="08">08</option>
                                                                        <option value="09">09</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                        <option value="13">13</option>
                                                                        <option value="14">14</option>
                                                                        <option value="15">15</option>
                                                                        <option value="16">16</option>
                                                                        <option value="17">17</option>
                                                                        <option value="18">18</option>
                                                                        <option value="19">19</option>
                                                                        <option value="20">20</option>
                                                                        <option value="21">21</option>
                                                                        <option value="22">22</option>
                                                                        <option value="23">23</option>
                                                                        <option value="24">24</option>
                                                                        <option value="25">25</option>
                                                                        <option value="26">26</option>
                                                                        <option value="27">27</option>
                                                                        <option value="28">28</option>
                                                                        <option value="29">29</option>
                                                                        <option value="30">30</option>
                                                                        <option value="31">31</option>
                                                                        <option value="32">32</option>
                                                                        <option value="33">33</option>
                                                                        <option value="34">34</option>
                                                                        <option value="35">35</option>
                                                                        <option value="36">36</option>
                                                                        <option value="37">37</option>
                                                                        <option value="38">38</option>
                                                                        <option value="39">39</option>
                                                                        <option value="40">40</option>
                                                                        <option value="41">41</option>
                                                                        <option value="42">42</option>
                                                                        <option value="43">43</option>
                                                                        <option value="44">44</option>
                                                                        <option value="45">45</option>
                                                                        <option value="46">46</option>
                                                                        <option value="47">47</option>
                                                                        <option value="48">48</option>
                                                                        <option value="49">49</option>
                                                                        <option value="50">50</option>
                                                                        <option value="51">51</option>
                                                                        <option value="52">52</option>
                                                                        <option value="53">53</option>
                                                                        <option value="54">54</option>
                                                                        <option value="55">55</option>
                                                                        <option value="56">56</option>
                                                                        <option value="57">57</option>
                                                                        <option value="58">58</option>
                                                                        <option value="59">59</option>
                                                                    </select>
                                                                </div>
                                                                <div style="float:left; ">
                                                                    <select name="deposit_stamp" id="deposit_stamp" class="form-control select form-control" style="width:55px;" required>
                                                                        <option value="AM">AM</option>
                                                                        <option value="PM">PM</option>
                                                                    </select>
                                                                    &nbsp;
                                                                </div>
                                                                <div style="float:left; margin-right: 5px; ">
                                                                    <button id="now" style="height: 30px; padding-top: 3px;" type="button" class="btn btn-info">Now</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Reference No</td>
                                                    <td>
                                                        <input name="refference_no" type="text" maxlength="30" class="form-control" value="{{ old('refference_no') }}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Scanned Receipt</td>
                                                    <td>
                                                        <div id="ctl00_MainContent_UpdatePanel5">
                                                            <input type="file" name="receipt" class="form-control" id="receipt" value="{{ old('receipt') }}" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">Bonus Code</td>
                                                    <td>
                                                        <select name="bonus_code" id="bonus_select"  class="form-control form-control select">
                                                            <option value="">No Bonus</option>
                                                            @foreach($bonuses as $bonus)
                                                                @php
                                                                    $exclude_arr = explode(',', $bonus->exclude_games);

                                                                    $prefixed_array = preg_filter('/^/', 'exclude_', $exclude_arr);

                                                                    $exclude_class = implode(" ", $prefixed_array);

                                                                @endphp
                                                                <option class="bonus_option {{ $exclude_class }}" value="{{ $bonus->bonus_code }}">{{ $bonus->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                             <button type="submit" class="btn btn-warning btn-more pull-right">NEXT</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="ctl00_UpdateProgress1" style="display:none;">
            <div class="overlay" />
                <div class="overlayContent">
                    <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                </div>
            </div>
        </div>
        @include ('mobile.footer')
    </div>
</div>
<script type="text/javascript">
    
    $('#now').click(function(){
        
        $('#deposit_date').val('{{ \Carbon\Carbon::now()->format('d/m/Y') }}');
        $('#deposit_hour').val('{{ \Carbon\Carbon::now()->format('h') }}');
        $('#deposit_minutes').val('{{ \Carbon\Carbon::now()->format('i') }}');
        $('#deposit_stamp').val('{{ \Carbon\Carbon::now()->format('A') }}');

    });

    $("#bonus_select").on('change',function(){
        if($("#bonus_select").val() == '')
        {
            $("#reward_select").prop("disabled", false);
        }
        else
        {
            $("#reward_select").prop("disabled", true);
        }
    })

    $("#reward_select").on('change',function(){
        if($("#reward_select").val() == '')
        {
            $("#bonus_select").prop("disabled", false);
        }
        else
        {
            $("#bonus_select").prop("disabled", true);
        }
    })

</script>
<script type="text/javascript">
    $(".datepicker").datepicker({
            dateFormat: "dd/mm/yy",
            showOn: 'both',
            buttonImage: "{{ secure_asset('images/date.png') }}",
            buttonText: "Open datepicker",
            buttonImageOnly: true,
            showAnim: 'slideDown',
            duration: 'fast',
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });
</script>
@endsection
@section('scripts')
@endsection