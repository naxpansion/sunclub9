@extends('mobile.master')
@section('content')        
<div class="all-elements">
    @include ('mobile.sidebar')
    <div id="content" class="page-content">
        @include ('mobile.nav') 
        <div>
            <div id="ctl00_UpdatePanel1">
                <div class="container bg-dark">
                    <ul id="tabmenu">
                        <li class="active"><a href="{{ url('player') }}">Account</a></li>
                        <li><a href="{{ url('player/deposit/step1') }}" >Deposit</a></li>
                        <li><a href="{{ url('player/withdrawal/step1') }}" class="deposit">Withdrawal</a></li>
                        <li><a href="{{ url('player/transfer/step1') }}" class="rebate">Transfer</a></li>
                        <li><a href="{{ url('player/transaction') }}" class="special">Transaction</a></li>
                        <li><a href="{{ url('player/rewards') }}" class="special">Rewards</a></li>
                        <li><a href="{{ url('player/profile') }}" class="scr888">Profile</a></li>
                    </ul>
                    <div id="ourHolder" class="promo-box">
                        <div class="tab-content short">
                            <div class="tab-pane fade in active">
                                <div class="title-wrap">
                                    <div class="outer-mask">
                                        <div class="inner-mask">
                                            <div class="title-p">
                                                <h2 class="smaller">My Game Account</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="member-main start-game">
                                    <div class="member-row clearfix">
                                        @foreach(\App\Game::where('status',1)->get() as $game)
                                            @php
                                                $account = \App\GameAccount::where('user_id',\Auth::user()->id)->where('game_id',$game->id)->first()
                                            @endphp
                                            @if($account === null)
                                                <div class="col-12" id="section_{{ $game->id }}">
                                                    <div class="card mb-1">
                                                        <img style="border: 1px solid #fff;" src="{{ secure_asset('storage/games/'.$game->logo) }}" width="100%" alt="" />
                                                    </div>
                                                    <a href="{{ url('player/request/'.$game->id) }}" class="btn btn-block btn-warning mb-1">REQUEST ACCOUNT</a>
                                                    <a href="{{ $game->ios_link }}"><button type="button" class="btn btn-primary btn-block">Download IOS</button></a>
                                                    <a href="{{ $game->android_link }}"><button type="button" class="btn btn-primary btn-block">Download ANDROID</button></a>
                                                </div>
                                            @else
                                                <div class="col-12" id="section_{{ $game->id }}">
                                                    <div class="card mb-1">
                                                        <img style="border: 1px solid #fff;" src="{{ secure_asset('storage/games/'.$game->logo) }}" width="100%" alt="" />
                                                    </div>
                                                    <table class="table table-game">
                                                        <tr>
                                                            <td>Username</td>
                                                            <td>{{ $account->username }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Password</td>
                                                            <td>{{ $account->password }}</td>
                                                        </tr>
                                                    </table>
                                                    <a href="{{ $game->ios_link }}"><button type="button" class="btn btn-primary btn-block">Download IOS</button></a>
                                                    <a href="{{ $game->ios_link }}"><button type="button" class="btn btn-primary btn-block">Download ANDROID</button></a>
                                                </div>
                                            @endif
                                            <br /><br /><br />&nbsp;
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="ctl00_UpdateProgress1" style="display:none;">
            <div class="overlay" />
                <div class="overlayContent">
                    <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                </div>
            </div>
        </div>
        @include ('mobile.footer')
    </div>
</div>
@endsection
@section('scripts')
@endsection