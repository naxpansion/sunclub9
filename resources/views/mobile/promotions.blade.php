@extends('mobile.master')

@section('content')        
        
        <div class="all-elements">

            @include ('mobile.sidebar')
        
            <div id="content" class="page-content">
        
            @include ('mobile.nav')	
                        
            <div>
                <div id="ctl00_UpdatePanel1">
            
               
                <div class="container bg-dark">
                    <h2 style="text-align: center;">Promotions</h2>
                    
                    <ul id="filterOptions">
                        <li class="active"><a href="#" class="all">All</a></li>
                        @foreach(\App\PromotionCategory::get() as $category)
                        <li><a href="#" class="{{ str_replace(' ','',$category->name) }}">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
        
                    <div id="ourHolder" class="promo-box">
                    
                    @foreach(\App\Promotion::get() as $promotion)
                        @php
                            $cats_raws = explode(',',$promotion->category);
                            $cats = '';

                            foreach($cats_raws as $cats_raw)
                            {
                                $cats .= str_replace(' ','',$cats_raw).' ';
                            }
                        @endphp
                        <div class="promo new latest {{ $cats }}">
                            <h3>
                                <img src="{{ secure_asset('storage/promotions/'.$promotion->img_mobile) }}" class="img-responsive width-full"/>
                            </h3>
                            <div class="promo-font">
                                <h4>{{ $promotion->title }}</h4>
                                {!! $promotion->mobile !!}
                            </div>
                        </div>
                    @endforeach
                        


                    </div>
                    
                </div>
                
        
                
        </div>
        <div id="ctl00_UpdateProgress1" style="display:none;">
            
        <div class="overlay" />
                    <div class="overlayContent">
                        <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                    </div>
            
        </div>
            </div>
        
            @include ('mobile.footer')
                            
            </div>  
            
        </div>
    
@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $(".promo div").hide();

        $(".promo h3").click(function () {
            $(this).next("div").slideToggle("fast")
		.siblings("p:visible").slideUp("fast");
            $(this).toggleClass("active");
            $(this).siblings("h3").removeClass("active");
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#filterOptions li a').click(function () {
            // fetch the class of the clicked item
            var ourClass = $(this).attr('class');

            // reset the active class on all the buttons
            $('#filterOptions li').removeClass('active');
            // update the active state on our clicked button
            $(this).parent().addClass('active');

            if (ourClass == 'all') {
                // show all our items
                $('#ourHolder').children('div.promo').show();
            }
            else {
                // hide all elements that don't share ourClass
                $('#ourHolder').children('div:not(.' + ourClass + ')').hide();
                // show all elements that do share ourClass
                $('#ourHolder').children('div.' + ourClass).show();
            }
            return false;
        });
    });
</script>
    
@endsection