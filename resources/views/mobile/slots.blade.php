@extends('mobile.master')

@section('content')        
            
        <div class="all-elements">

                @include ('mobile.sidebar')
        
            <div id="content" class="page-content">
        
                @include ('mobile.nav')
                        
            <div>

                    <div id="ctl00_UpdatePanel1">

                        <div class="container" style="background: url({{ url('images/slot/bg.png') }}) no-repeat; background-position: center; background-size: cover; padding-left: 8%; padding-right: 8%;" >
                            <div class="row">
                                <div class="col-md-12 center">
                                    <img src="{{ url('images/slot/title.png') }}" width="100%">
                                    <br />
                                    <img src="{{ url('images/slot/item.png') }}" usemap="#image-map" width="100%">
                                </div>
                            </div>
                            <br />
                            <br />
                        </div>

                    </div>

        <div id="ctl00_UpdateProgress1" style="display:none;">
            
        <div class="overlay" />
                    <div class="overlayContent">
                        <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                    </div>
            
        </div>
            </div>
        
            @include ('mobile.footer')
                            
            </div>  
            
        </div>

@endsection