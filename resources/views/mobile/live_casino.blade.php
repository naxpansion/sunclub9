@extends('mobile.master')

@section('content')        
            
        <div class="all-elements">

                @include ('mobile.sidebar')
        
            <div id="content" class="page-content">
        
                @include ('mobile.nav')
                        
            <div>

                    <div id="ctl00_UpdatePanel1">

                            <div class="container bg-dark" style="background: url({{ url('images/live/bg_live.png') }}) no-repeat; background-position: center; background-size: cover; padding-left: 8%; padding-right: 8%;">
                                
                                <div class="row">
                                    <div class="col-md-12 center">
                                        <img src="{{ url('images/live/title.png') }}" width="100%">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <img src="{{ url('images/live/ct85.png') }}" usemap="#image-map" width="100%">
                                    </div>
                                    <div class="col-xs-12">
                                        <img src="{{ url('images/live/sun_city.png') }}" usemap="#image-map-2" width="100%">
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-xs-12">
                                        <img src="{{ url('images/live/joker.png') }}" usemap="#image-map-3" width="100%">
                                    </div>
                                    <div class="col-xs-12">
                                        <img src="{{ url('images/live/sa36.png') }}" usemap="#image-map-4" width="100%">
                                    </div>
                                </div>
                                <br />
                            <map name="image-map">
                                <area target="_blank" alt="#" title="#" href="{{ \App\Game::find(5)->desktop_link }}" coords="144,334,17" shape="circle">
                                <area target="" alt="#" title="#" href="{{ \App\Game::find(5)->android_link }}" coords="192,336,16" shape="circle">
                                <area target="" alt="#" title="#" href="{{ \App\Game::find(5)->ios_link }}" coords="236,334,14" shape="circle">
                            </map>

                            <map name="image-map-2">
                                <area target="_blank" alt="" title="" href="#" coords="314,331,14" shape="circle">
                                <area target="_blank" alt="" title="" href="#" coords="358,329,15" shape="circle">
                                <area target="_blank" alt="" title="" href="#" coords="404,330,17" shape="circle">
                            </map>

                            <map name="image-map-3">
                                <area target="" alt="" title="" href="{{ \App\Game::find(7)->desktop_link }}" coords="148,331,16" shape="circle">
                                <area target="" alt="" title="" href="{{ \App\Game::find(7)->android_link }}" coords="194,331,16" shape="circle">
                                <area target="" alt="" title="" href="{{ \App\Game::find(7)->ios_link }}" coords="238,331,16" shape="circle">
                            </map>

                            <map name="image-map-4">
                                <area target="" alt="" title="" href="{{ \App\Game::find(6)->desktop_link }}" coords="321,335,13" shape="circle">
                                <area target="" alt="" title="" href="{{ \App\Game::find(6)->android_link }}" coords="369,335,14" shape="circle">
                                <area target="" alt="" title="" href="{{ \App\Game::find(6)->ios_link }}" coords="414,336,18" shape="circle">
                            </map>
                                                            
                            </div>

                    </div>

        <div id="ctl00_UpdateProgress1" style="display:none;">
            
        <div class="overlay" />
                    <div class="overlayContent">
                        <img src="{{ asset('mobile/images/ajax-loader.gif') }}" alt="Loading" border="1" />
                    </div>
            
        </div>
            </div>
        
            @include ('mobile.footer')
                            
            </div>  
            
        </div>

@endsection