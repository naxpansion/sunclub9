@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li class=""><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li class=""><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li class="active"><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
                        
        <div class="member-main">
            <div class="member-row">
                <h4>Profile Details</h4>
                <div class="member-main">
                    <div id="ctl00_MainContent_UpdatePanel1">
                        <form method="post" action="{{ url('player/profile/update') }}">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">
                        <div class="member-row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                                <tr>
                                    <td width="40%">Email Address</td>
                                    <td>{{ \Auth::user()->email }}</td>
                                </tr>
                                <tr>
                                    <td>Full Name</td>
                                    <td>{{ \Auth::user()->name }}</td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td>{{ \Auth::user()->username }}</td>
                                </tr>
                                <tr>
                                    <td>Contact Number</td>
                                    <td>{{ \Auth::user()->phone }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>
                                        {{ Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], \Auth::user()->gender , ['class' => 'form-control']) }}
                                        <span class="text-error">
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date Of Birth</td>
                                    <td>
                                        <input name="dob" type="text" maxlength="10" class="datepicker form-control" value="{{ \Auth::user()->dob }}" required/>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td>SMS Service</td>
                                    <td>
                                        {{ Form::select('sms_service', ['1' => 'Yes', '0' => 'No'], \Auth::user()->sms_service , ['class' => 'form-control']) }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-warning btn-more pull-right">SAVE</button>
                            </form>
                        </div>
                    </div>
                </div>
                <br /><br />
                <h4>Bank Info</h4>
                <div class="member-main">
                    <div class="member-row">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                            
                            <tr>
                                <td width="40%">Bank Account Name</td>
                                <td>{{ \Auth::user()->name }}</td>
                            </tr>
                            <tr>
                                <td>Bank Name</td>
                                <td>{{ \Auth::user()->bank_name }}</td>
                            </tr>
                            <tr>
                                <td>Bank Account Number</td>
                                <td>{{ \Auth::user()->bank_account_no }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br /><br />
                <h4>Change Password</h4>
                <div class="member-main">
                    <div id="ctl00_MainContent_upchangepass">
                        <form method="post" action="{{ url('player/profile/password') }}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">
                        <div class="member-row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                                <tr>
                                    <td width="40%">Current Password</td>
                                    <td>
                                        <input name="current_pass" type="password" minlength="6" class="form-control" />
                                        <span class="text-error"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>New Password</td>
                                    <td>
                                        <input name="new_pass" type="password" minlength="6" class="form-control" />
                                        <span class="text-error"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Confirm Password</td>
                                    <td><input name="confirm_pass" type="password" minlength="6" class="form-control" />
                                        <span class="text-error">
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-warning btn-more pull-right">UPDATE</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        $(".datepicker").datepicker({
                dateFormat: "dd/mm/yy",
                showOn: 'both',
                buttonImage: "{{ secure_asset('images/date.png') }}",
                buttonText: "Open datepicker",
                buttonImageOnly: true,
                showAnim: 'slideDown',
                duration: 'fast',
                showOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "1900:-1"
            });
    </script>

@endsection