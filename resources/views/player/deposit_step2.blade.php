@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li ><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li class="active"><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <h4 style="color: gold;">Step 2 : Please Transfer Deposit TO Any Bank Below.</h4>
        
        <div class="member-main">
            <div class="member-row">
                @foreach($banks as $bank)
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                        <tr>
                            <th rowspan="2"><div class="bank">{{ $bank->name }}</div></th>
                            <td>Account Number: {{ $bank->account_no }}</td>
                        </tr>
                        <tr>
                            <td>Account Name: {{ $bank->account_name }}</td>
                        </tr>
                    </table>
                @endforeach
            </div>
            <div class="member-row" style="color: white;">
                Notes:
                <ol>
                    <li>Minimum Deposit MYR {{ \App\Setting::meta('MIN_DEPOSIT') }}.</li>
                    <li>We Accept Online Transfer / ATM Transfer / Cash Deposit.</li>
                    <li>Please keep the bank receipt or transaction reference number.</li>
                    <li>Click next after transfer has been maded.</li>
                </ol>
            </div>
            <div class="clearfix">
                <form method="get" action="{{ url('player/deposit/step3') }}">
                    <a href="{{ URL::previous() }}"><button type="button" class="btn btn-warning btn-more pull-left">BACK</button></a>
                    @csrf
                    <input type="hidden" name="games" value="{{ $game_id }}">
                    <button type="submit" class="btn btn-warning btn-more pull-right">NEXT</button>
                </form>
            </div>
        </div>

@endsection