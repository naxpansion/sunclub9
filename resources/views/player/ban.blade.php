@include('player.header')
<div class="content">
    <div class="container">
        <div class="page-content">
            <div class="m-page clearfix">
                <h3 style="text-align: center;">your account has been disabled. Please contact support through live chat.</h3>
            </div>
        </div>
    </div>
</div>
@include('player.footer')
</body></html>