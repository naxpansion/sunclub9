@extends ('front.master')
@section('content')


<div class="profile-bg" style="min-width: 1024px;">
    <div id="profile_container">
        <div id="menu6">
            <ul>
                <li><a href="{{ url('/player') }}"><span>MY ACCOUNT</span></a></li>
                <li><a href="{{ url('player/deposit/step1') }}"><span>DEPOSIT</span></a></li>
                <li class="active"><a href="{{ url('player/withdrawal/step1') }}"><span>WITHDRAW</span></a></li>
                <li><a href="{{ url('player/transfer/step1') }}"><span>TRANSFER</span></a></li>
                <li><a href="{{ url('player/transaction') }}"><span>TRANSACTION</span></a></li>
                <li><a href="{{ url('player/rewards') }}"><span>REWARD</span></a></li>
                <li><a href="{{ url('player/profile') }}"><span>PROFILE</span></a></li>
                <li><a href="{{ url('player/affiliate') }}"><span>AFFILIATE</span></a></li>
            </ul>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <br /><br /><br /><br />
        <h4>Step 2 : Please confirm your bank details.</h4>
        <div class="member-main">
            <div class="member-row">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="color: white;">
                    <tr>
                        <td style="width: 200px;">Bank Name</td>
                        <td>{{ \Auth::user()->bank_name }}</td>
                    </tr>
                    <tr>
                        <td>Account Name</td>
                        <td>{{ \Auth::user()->name }}</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>{{ \Auth::user()->bank_account_no }}</td>
                    </tr>
                </table>
                <p style="color: white;">* Please contact our customer service if any inaccurate of your bank info.</p>
            </div>
            <div class="clearfix">
                <form method="post" action="{{ url('player/withdrawal/step3') }}">
                    <a href="{{ URL::previous() }}"><button type="button" class="btn btn-warning btn-more pull-left">BACK</button></a>
                    @csrf
                    <input type="hidden" name="game_id" value="{{ $input['game_id'] }}">
                    <input type="hidden" name="amount" value="{{ $input['amount'] }}">
                    <button type="submit" class="btn btn-warning btn-more pull-right">NEXT</button>
                </form>
            </div>

        </div>
    </div>
@endsection