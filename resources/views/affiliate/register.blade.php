@extends ('front.master')
@section('content')

<div id="ctl00_cphBody_bg" class="banking">
    <div class="uni-title">
        <img src="/images/splash.png" alt=""/>
        <p>Affiliate Register</p>
    </div>
    <center>
        <!--<div id="bank2">
            <img src="/images/cimbbank.jpg" />
            <img src="/images/maybank.jpg" />
            <img src="/images/public.jpg" />
            <img src="/images/hongleong.jpg" />
            </div>-->
        <div id="b-type">
            <form method="POST" action="{{ route('register') }}">

                @csrf
                <div class="row">
                    <div class="col-md-12">
                        a
                    </div>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                    <tr>
                        <td>
                            <input name="name" type="text" id="name" placeholder="Full Name" autocomplete="name" class="field-register" onkeyup="nospaces(this)" value="{{old('name') }}" / required>
                        </td>
                        <td class="col-2">&nbsp;</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="username" type="text" id="username" placeholder="Username" autocomplete="name" class="field-register" onkeyup="nospaces(this)" value="{{old('username') }}" / required>
                        </td>
                        <td class="col-2">&nbsp;</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="email" type="email" autocomplete="email" placeholder="Email Address" class="field-register" required/>
                        </td>
                        <td class="col-2">&nbsp;</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="phone" type="text" placeholder="60123456789" class="field-register" autocomplete="tel-national" value="{{old('phone') }}" required/>
                        </td>
                        <td class="col-2">Please provide a valid Contact Number</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="password" type="password" autocomplete="new-password" placeholder="Password" class="field-register" required/>
                        </td>
                        <td class="col-2">Minimum 6 alphamumeric (A-Z, a-z, 0-9) character only.</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="password_confirmation" type="password" autocomplete="new-password" placeholder="Confirm Password" class="field-register" required/>
                        </td>
                        <td class="col-2">&nbsp;</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <input name="bank_name" type="text" id="bank_name" placeholder="Bank Name" autocomplete="bank_name" class="field-register"  / required>
                        </td>
                        <td class="col-2">&nbsp;</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="bank_account_no" type="text" id="bank_account_no" placeholder="Bank Account No" autocomplete="bank_account_no" class="field-register" / required>
                        </td>
                        <td class="col-2">&nbsp;</td>
                        <td><span class="text-error">
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 375px;">
                            <div class="g-recaptcha" data-sitekey="6LdrOUoUAAAAAO3ICDvvbpAnGFz55RoADJ1YP4Vt"></div>
                        </td>
                        <td class="col-2">
                            <table>
                            </table>
                        </td>
                        <td><span class="text-error">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>This email has been used, please use another email.</strong>
                                </span>
                            @endif
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback">
                                    <strong>{{ old('phone') }} has already used for registration. Please Contact Customer Services for more assistance.</strong>
                                </span>
                            @endif
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>Please make sure your password is more than 6(six) character</strong>
                                </span>
                            @endif
                            </span>
                        </td>
                    </tr>
                    
                </table>
                <span class="text-error">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>You are already a registered member of us! Please Contact Customer Services for more assistance.</strong>
                        </span>
                    @endif
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ old('phone') }} has already used for registration. Please Contact Customer Services for more assistance.</strong>
                        </span>
                    @endif
                    </span>
                <input type="submit" name="btnsend" value="Submit" id="btnsend" class="btn btn-warning btn-more pull-right" />
            </form>
        </div>
    </center>
</div>
    
@endsection
@section('script')
<script type="text/javascript" src="{{ secure_asset('plugins/jquery.mask.js') }}"></script>
<script type="text/javascript">
    $(".phonemask").mask('601000000000');
</script>
@endsection

