<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bonus_code');
            $table->string('name');
            $table->string('description');
            $table->string('type');
            $table->string('value');
            $table->integer('allow_multiple');
            $table->integer('daily');
            $table->decimal('min_deposit',10,2);
            $table->decimal('max_bonus',10,2)->nullable();
            $table->integer('turnover')->nullable();
            $table->string('exclude_games')->nullable();
            $table->string('exclude_days')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonuses');
    }
}
