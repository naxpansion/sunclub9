function isEmpty (str)
/* Function to check empty text field, it will return true or false. */
{
    allspace = true;
    i = 0;
    j = i + 1;
    while ((i < str.value.length) && (allspace == true)) {
      if (str.value.substring(i,j) != " ") {
		allspace = false;
	  } 
	  i++;
	  j = i + 1;
    }

    if ((str.value == "") || allspace)
		return true;
	return false;
}

function isSpace(str)
{
    gotspace = false;
    i = 0;
    j = i + 1;
    while (i < str.value.length) {
      if (str.value.substring(i,j) == " ") {
		gotspace = true;
	  } 
	  i++;
	  j = i + 1;
    }

    if (gotspace)
		return true;
	return false;
}

function isEmptySelectBox (sltboxName)
/* Function to check empty select box, it will return true or false. */
{
    if (sltboxName.selectedIndex < 0) 
        return true;
    return false;
}

function isNumeric (txtFldName)
/* Function to check whether it is a numeric. */
{
    str = txtFldName.value;
    if (str.length == 0)
        return false;
    for (var n = 0; n < str.length; n++)
        if (str.substring(n, n+1) < "0" || str.substring(n, n+1) > "9")
	    return false;
    return true;
}

function isNumDot (txtFldName)
/* function to check whether it is a numeric */
{
    str = txtFldName.value;
	
	if (str.length == 0)
		return false;

    valid = true;
    n = 0;
	while ((n < str.length) && (valid == true)) {
	    if ((str.substring(n, n + 1) >= "0" && str.substring(n, n + 1) <= "9") || str.substring(n, n + 1) == "." || str.substring(n, n + 1) == "," || str.substring(n, n + 1) == "-")
			valid = true
		else
		    valid = false
        n++;
    }
	return valid;
}

function isNumDash (txtFldName)
/* function to check whether it is a numeric */
{
    str = txtFldName.value;
	
	if (str.length == 0)
		return false;

    valid = true;
    n = 0;
	while ((n < str.length) && (valid == true)) {
		if ((str.substring(n, n+1) >= "0" && str.substring(n, n+1) <= "9") || str.substring(n, n+1) == "-" || str.substring(n, n+1) == " " || str.substring(n, n+1) == "+")
			valid = true
		else
		    valid = false
        n++;
    }
	return valid;
}

function isDecimal (txtFldName)
/* function to check whether it is a decimal */
{
   str = txtFldName.value;
   dotfound = 0;
   predotpos = 0;
   postdotpos = 0;

   if (isNumDot(txtFldName)) {
      for (var n = 0; n < str.length; n++) {
         if (str.substring(n, n+1) == ".") {
		    predotpos = n - 1;
			postdotpos = n + 1;
	 	    dotfound++;
		 }  
	  }
	  if (dotfound == 0) {
	     return true
	  } else if (dotfound > 1) {
	     return false
	  } else if (dotfound == 1) {
         if ((predotpos < 0) || (postdotpos > (str.length - 1))) {
		    return false
		 } else {
		    return true
		 }
      }
   } else {
      return false;
   }
}

function isMaxLength (txtAreaName, maxLength)
/* function to check maximum length of the field */
{
    if (txtAreaName.value.length > maxLength)
        return false;
    return true;
}

function isMinLength (txtFldName, minLength)
/* function to check the minimum length of the field */
{
    if (txtFldName.value.length < minLength)
        return false;
    return true;
}

function isPasswordMatch (pwdFldName1, pwdFldName2)
/* Function to check whether the both passwords entered are identical. */
{
    if (pwdFldName1.value.length != pwdFldName2.value.length)
        return false;
    else {
		if (pwdFldName1.value != pwdFldName2.value)
 			return false;
	else
 	    return true;
    }	
}

function isValidDate (dateDay, dateMonth, dateYear)
/* Function to check whether the date is valid */
{
    day = parseInt(dateDay.options[dateDay.selectedIndex].value);
	month = parseInt(dateMonth.options[dateMonth.selectedIndex].value) - 1;
    year = parseInt(dateYear.options[dateYear.selectedIndex].value);

	if ((day == "")|| (month == "") || (year == "")) {
		return false;
	}

	var err = 0;

	if (day < 1 || day > 31) err = 1
	if (month < 0 || month > 11) err = 1
	if (year < 1950 || year > 2050 || isNaN(year)) err = 1
		
	if (month == 1 || month == 3 || month == 5 || month == 8 || month == 10) {
		if (day == 31) err=1
	}

	//Febuary, Leap year
	if (month == 1)	{
		var g = parseInt(year/4)
		if (isNaN(g)) 	{
			err = 1
		}
		if (day > 29) err=1
		if (day == 29 && ((year/4)!=parseInt(year/4))) err=1
	}

	if (err == 1)
		return false;
	else
		return true;
}


function isValidDateValues (dateDay, dateMonth, dateYear)
/* Function to check whether the date is valid */
{
	if ((dateDay == "")|| (dateMonth == "") || (dateYear == "")) {
		return false;
	}

    day = parseInt(dateDay);
	month = parseInt(dateMonth) - 1;
    year = parseInt(dateYear);
	var err = 0;

	if (day < 1 || day > 31) err = 1
	if (month < 0 || month > 11) err = 1
	if (year < 1950 || year > 2050) err = 1
		
	if (month == 1 || month == 3 || month == 5 || month == 8 || month == 10) {
		if (day == 31) err=1
	}

	//Febuary, Leap year
	if (month == 1)	{
		var g = parseInt(year/4)
		if (isNaN(g)) 	{
			err = 1
		}
		if (day > 29) err=1
		if (day == 29 && ((year/4)!=parseInt(year/4))) err=1
	}

	if (err == 1)
		return false;
	else
		return true;
}


function isValidDateRange (startDay, startMonth, startYear, endDay, endMonth, endYear)
/* Function to check the valid duration between two dates */
{ 
 DayStartSlt = startDay.selectedIndex;
 MonthStartSlt = startMonth.selectedIndex;
 YearStartSlt = startYear.selectedIndex;
 
 DayStart = parseInt(startDay.options[DayStartSlt].value);
 MonthStart = parseInt(startMonth.options[MonthStartSlt].value) - 1;
 YearStart = parseInt(startYear.options[YearStartSlt].value);

 DayEndSlt = endDay.selectedIndex;
 MonthEndSlt = endMonth.selectedIndex;
 YearEndSlt = endYear.selectedIndex;
   
 DayEnd = parseInt(endDay.options[DayEndSlt].value);
 MonthEnd = parseInt(endMonth.options[MonthEndSlt].value) - 1;
 YearEnd = parseInt(endYear.options[YearEndSlt].value);
  
 startDate = new Date(YearStart, MonthStart, DayStart);
 endDate = new Date(YearEnd, MonthEnd, DayEnd);
 
 startDate = startDate.getTime();
 endDate = endDate.getTime();
  
 if (endDate - startDate >= 0)
	return true;
 else
	return false;
}

function isValidImageFile(file_value) {
	last_pos = file_value.lastIndexOf(".");
	len = file_value.length;		
	file_ext = file_value.substring(last_pos+1, len);
	file_ext = file_ext.toLowerCase();
		
	if (!((file_ext == "gif") || (file_ext == "jpg") || (file_ext == "jpeg"))) {
		return false;
	} else {
		return true;
	}
}


function isValidMovieFile(file_value) {
	last_pos = file_value.lastIndexOf(".");
	len = file_value.length;		
	file_ext = file_value.substring(last_pos+1, len);
	file_ext = file_ext.toLowerCase();
		
	if (!((file_ext == "mov") || (file_ext == "avi") || (file_ext == "mpg") || (file_ext == "mpeg"))) {
		return false;
	} else {
		return true;
	}
}

function isValidCreditCard (txtFldValue)
/* Function to check whether the credit card no# is valid or not. */
{
 var C=0; 
 var TOT=0; 
 var FLAG=0; 
 var ccNo;
 var cnt;
 var first_value;
 var next_value;
    
 ccNo = txtFldValue; 
 first_value = ccNo.substring(0, 1); 
 
 cnt = 1;
 for (j=1; j<16; j++)
 {
  next_value = ccNo.substring(j, j+1);
  
  if (first_value == next_value)
	cnt++; 
 } 
 
 if (cnt != 16)
 {
  for (i=0; i<16; i++)
  {
    C = ccNo.substring(i, i+1);
        
	if (FLAG == 0)
	{
		C = C * 2; 

		if (C > 9) 
		{
			C = C - 9; 
		}
		
		FLAG = 1;
	}	 	
	else
	{
		FLAG = 0; 
	}
	TOT = TOT + parseInt(C); 	
  }  

 if (TOT % 10 == 0)
	FLAG = 0;	 
 else 
	FLAG = 1;
 }
 else
	FLAG = 1;
	

 if (FLAG == 0)
	return true;
 else
	return false; 
}


function isValidEmail(emailStr) {
	/* The following pattern is used to check if the entered e-mail address
	   fits the user@domain format.  It also is used to separate the username
	   from the domain. */
	var emailPat=/^(.+)@(.+)$/
	/* The following string represents the pattern for matching all special
	   characters.  We don't want to allow special characters in the address. 
	   These characters include ( ) < > @ , ; : \ " . [ ]    */
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
	/* The following string represents the range of characters allowed in a 
	   username or domainname.  It really states which chars aren't allowed. */
	var validChars="\[^\\s" + specialChars + "\]"
	/* The following pattern applies if the "user" is a quoted string (in
	   which case, there are no rules about which characters are allowed
	   and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
	   is a legal e-mail address. */
	var quotedUser="(\"[^\"]*\")"
	/* The following pattern applies for domains that are IP addresses,
	   rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
	   e-mail address. NOTE: The square brackets are required. */
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
	/* The following string represents an atom (basically a series of
	   non-special characters.) */
	var atom=validChars + '+'
	/* The following string represents one word in the typical username.
	   For example, in john.doe@somewhere.com, john and doe are words.
	   Basically, a word is either an atom or quoted string. */
	var word="(" + atom + "|" + quotedUser + ")"
	// The following pattern describes the structure of the user
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
	/* The following pattern describes the structure of a normal symbolic
	   domain, as opposed to ipDomainPat, shown above. */
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")


	/* Finally, let's start trying to figure out if the supplied address is
	   valid. */

	/* Begin with the coarse pattern to simply break up user@domain into
	   different pieces that are easy to analyze. */
	var matchArray=emailStr.match(emailPat)
	if (matchArray==null) {
	  /* Too many/few @'s or something; basically, this address doesn't
		 even fit the general mould of a valid e-mail address. */
		alert("Please enter a valid email.")
		return false
	}
	var user=matchArray[1]
	var domain=matchArray[2]

	// See if "user" is valid 
	if (user.match(userPat)==null) {
		// user is not valid
		alert("The username doesn't seem to be valid.")
		return false
	}

	/* if the e-mail address is at an IP address (as opposed to a symbolic
	   host name) make sure the IP address is valid. */
	var IPArray=domain.match(ipDomainPat)
	if (IPArray!=null) {
		// this is an IP address
		  for (var i=1;i<=4;i++) {
			if (IPArray[i]>255) {
				alert("Destination IP address is invalid!")
			return false
			}
		}
		return true
	}

	// Domain is symbolic name
	var domainArray=domain.match(domainPat)
	if (domainArray==null) {
		alert("The domain name doesn't seem to be valid.")
		return false
	}

	/* domain name seems valid, but now make sure that it ends in a
	   three-letter word (like com, edu, gov) or a two-letter word,
	   representing country (uk, nl), and that there's a hostname preceding 
	   the domain or country. */

	/* Now we need to break up the domain to get a count of how many atoms
	   it consists of. */
	var atomPat=new RegExp(atom,"g")
	var domArr=domain.match(atomPat)
	var len=domArr.length
	if (domArr[domArr.length-1].length<2 || 
		domArr[domArr.length-1].length>3) {
	   // the address must end in a two letter or three letter word.
	   alert("Please enter a valid email.")
	   return false
	}

	// Make sure there's a host name preceding the domain.
	if (len<2) {
	   var errStr="This address is missing a hostname!"
	   alert(errStr)
	   return false
	}

	// If we've gotten this far, everything's valid!
	return true;
}

function FormatNumber(num,decimalNum,bolLeadingZero,bolParens,bolCommas)
/**********************************************************************
	IN:
		NUM - the number to format
		decimalNum - the number of decimal places to format the number to
		bolLeadingZero - true / false - display a leading zero for
										numbers between -1 and 1
		bolParens - true / false - use parenthesis around negative numbers
		bolCommas - put commas as number separators.
 
	RETVAL:
		The formatted number!
 **********************************************************************/
{ 
        if (isNaN(parseInt(num))) return "NaN";

	var tmpNum = num;
	var iSign = num < 0 ? -1 : 1;		// Get sign of number
	
	// Adjust number so only the specified number of numbers after
	// the decimal point are shown.
	tmpNum *= Math.pow(10,decimalNum);
	tmpNum = Math.round(Math.abs(tmpNum))
	tmpNum /= Math.pow(10,decimalNum);
	tmpNum *= iSign;					// Readjust for sign
	
	
	// Create a string object to do our formatting on
	var tmpNumStr = new String(tmpNum);

	// See if we need to strip out the leading zero or not.
	if (!bolLeadingZero && num < 1 && num > -1 && num != 0)
		if (num > 0)
			tmpNumStr = tmpNumStr.substring(1,tmpNumStr.length);
		else
			tmpNumStr = "-" + tmpNumStr.substring(2,tmpNumStr.length);
		
	// See if we need to put in the commas
	if (bolCommas && (num >= 1000 || num <= -1000)) {
		var iStart = tmpNumStr.indexOf(".");
		if (iStart < 0)
			iStart = tmpNumStr.length;

		iStart -= 3;
		while (iStart >= 1) {
			tmpNumStr = tmpNumStr.substring(0,iStart) + "," + tmpNumStr.substring(iStart,tmpNumStr.length)
			iStart -= 3;
		}		
	}

	// See if we need to use parenthesis
	if (bolParens && num < 0)
		tmpNumStr = "(" + tmpNumStr.substring(1,tmpNumStr.length) + ")";

	return tmpNumStr;		// Return our formatted string!
}

function validSC(thisField)
{
	var iChars = "!@#$%^&*()+=[]\\\;/{}|\":<>?";

  	for (var i = 0; i < thisField.value.length; i++) 
	{
  		if (iChars.indexOf(thisField.value.charAt(i)) != -1) 
		{
  			alert ("Special characters are not allowed. \n Please remove them and try again.");
			filterNum(thisField.value, thisField);
  			return false;
  		}
  	}
}

function filterNum(str, thisField) 
{
	re = /\$|@|#|~|`|\%|\*|\^|\&|\(|\)|\+|\=|\[|\-|\_|\]|\[|\}|\{|\;|\:|\"|\<|\>|\?|\||\\|\!|\$|/g;
	//re = /\$|@|#|~|`|\%|\*|\^|\&|\(|\)|\+|\=|\[|\-|\_|\]|\[|\}|\{|\;|\:||\"|\<|\>|\?|\||\\|\!|\$|/g;
    // remove special characters like "$" and "," etc...
    thisField.value = str.replace(re, "");
}