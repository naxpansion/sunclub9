﻿function openNav() {
    document.getElementById("mySidenav").style.width = "65%";
    $("#menu-open").hide();
    $("#menu-close").show();
    $("#cover-layer").show();
    $("body").css("overflow-y", "hidden");
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    $("#menu-open").show();
    $("#menu-close").hide();
    $("#cover-layer").hide();
    $("body").css("overflow-y", "visible");
}