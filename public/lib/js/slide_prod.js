﻿/***********************************************
* Conveyor belt slideshow script- © Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

//Specify the slider's width (in pixels)
var sliderwidth2 = "1012px"
//Specify the slider's height
var sliderheight2 = "70px"
//Specify the slider's slide speed (larger is faster 1-10)
var slidespeed2 = 2
//configure background color:
slidebgcolor2 = "black"

//Specify the slider's images
var leftrightslide2 = new Array()
var finalslide2 = ''

leftrightslide2[0] = '<img src="/images/games/1.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[1] = '<img src="/images/games/2.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[2] = '<img src="/images/games/3.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[3] = '<img src="/images/games/4.png" style="height:70px; border:0px solid white;" />'
leftrightslide2[4] = '<img src="/images/games/5.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[5] = '<img src="/images/games/6.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[6] = '<img src="/images/games/7.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[7] = '<img src="/images/games/8.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[8] = '<img src="/images/games/9.png" style="height:60px; border:0px solid white;" />'
leftrightslide2[9] = '<img src="/images/games/10.png" style="height:60px; border:0px solid white;" />'

//Specify gap between each image (use HTML):
var imagegap2 = "<img src='/images/spacer.gif' width='80' height='1' />"

//Specify pixels gap between each slideshow rotation (use integer):
var slideshowgap2 = 80

////NO NEED TO EDIT BELOW THIS LINE////////////

var copyspeed2 = slidespeed2
leftrightslide2 = '<nobr>' + leftrightslide2.join(imagegap2) + '</nobr>'
var iedom2 = document.all || document.getElementById
if (iedom2)
    document.write('<span id="temp2" style="visibility:hidden;position:absolute;top:-100px;left:-9000px">' + leftrightslide2 + '</span>')
var actualwidth2 = ''
var cross2_slide, ns2_slide

function fillup2() {
    if (iedom2) {
        cross2_slide = document.getElementById ? document.getElementById("test2a") : document.all.test2a
        cross2_slide2 = document.getElementById ? document.getElementById("test3a") : document.all.test3a
        cross2_slide.innerHTML = cross2_slide2.innerHTML = leftrightslide2
        actualwidth2 = document.all ? cross2_slide.offsetWidth : document.getElementById("temp2").offsetWidth
        cross2_slide2.style.left = actualwidth2 + slideshowgap2 + "px"
    }
    else if (document.layers) {
        ns2_slide = document.ns2_slidemenu.document.ns2_slidemenu2
        ns2_slide2 = document.ns2_slidemenu.document.ns2_slidemenu3
        ns2_slide.document.write(leftrightslide2)
        ns2_slide.document.close()
        actualwidth2 = ns2_slide.document.width
        ns2_slide2.left = actualwidth2 + slideshowgap2
        ns2_slide2.document.write(leftrightslide2)
        ns2_slide2.document.close()
    }
    lefttime2 = setInterval("slideleft2()", 30)
}

function slideleft2() {
    if (iedom2) {
        if (parseInt(cross2_slide.style.left) > (actualwidth2 * (-1) + 8))
            cross2_slide.style.left = parseInt(cross2_slide.style.left) - copyspeed2 + "px"
        else
            cross2_slide.style.left = parseInt(cross2_slide2.style.left) + actualwidth2 + slideshowgap2 + "px"

        if (parseInt(cross2_slide2.style.left) > (actualwidth2 * (-1) + 8))
            cross2_slide2.style.left = parseInt(cross2_slide2.style.left) - copyspeed2 + "px"
        else
            cross2_slide2.style.left = parseInt(cross2_slide.style.left) + actualwidth2 + slideshowgap2 + "px"

    }
    else if (document.layers) {
        if (ns2_slide.left > (actualwidth2 * (-1) + 8))
            ns2_slide.left -= copyspeed2
        else
            ns2_slide.left = ns2_slide2.left + actualwidth2 + slideshowgap2

        if (ns2_slide2.left > (actualwidth2 * (-1) + 8))
            ns2_slide2.left -= copyspeed2
        else
            ns2_slide2.left = ns2_slide.left + actualwidth2 + slideshowgap2
    }
}


if (iedom2 || document.layers) {
    with (document) {
        document.write('<table border="0" cellspacing="0" cellpadding="0"><td>')
        if (iedom2) {
            write('<div style="position:relative;width:' + sliderwidth2 + ';height:' + sliderheight2 + ';overflow:hidden">')
            write('<div style="position:absolute;width:' + sliderwidth2 + ';height:' + sliderheight2 + ';background-color:' + slidebgcolor2 + '" onMouseover="copyspeed2=0" onMouseout="copyspeed2=slidespeed2">')
            write('<div id="test2a" style="position:absolute;left:0px;top:0px"></div>')
            write('<div id="test3a" style="position:absolute;left:-1000px;top:0px"></div>')
            write('</div></div>')
        }
        else if (document.layers) {
            write('<ilayer width=' + sliderwidth2 + ' height=' + sliderheight2 + ' name="ns2_slidemenu" bgColor=' + slidebgcolor2 + '>')
            write('<layer name="ns2_slidemenu2" left=0 top=0 onMouseover="copyspeed2=0" onMouseout="copyspeed2=slidespeed2"></layer>')
            write('<layer name="ns2_slidemenu3" left=0 top=0 onMouseover="copyspeed2=0" onMouseout="copyspeed2=slidespeed2"></layer>')
            write('</ilayer>')
        }
        document.write('</td></table>')
    }
}