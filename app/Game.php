<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function spins()
    {
        return $this->hasMany('App\Spin');
    }

    public function GameAccounts()
    {
        return $this->hasMany('App\GameAccount');
    }

    public function GameAccountsNotAssigned()
    {
    	return $this->hasMany('App\GameAccount')->where('status',0);
    }
}
