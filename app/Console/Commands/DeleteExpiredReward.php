<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Spin;
use Carbon\Carbon;

class DeleteExpiredReward extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reward:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete expired reward more than 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $spins = Spin::where('status',1)->get();

        foreach($spins as $spin)
        {
            $reward_age = $spin->created_at->diffInMinutes(Carbon::now());
            if(1440 < $reward_age)
            {
                $spin->status = 4;
                $spin->remarks = 'no claim within 24 hours';
                $spin->save(); 
            }
        }
    }
}
