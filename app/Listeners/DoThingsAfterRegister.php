<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;
use App\Spin;
use Pusher\Pusher;
use App\Wheel;

class DoThingsAfterRegister
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterWasSuccessful  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $random_tac = mt_rand(100000, 999999);

        $event->user->tac_no = $random_tac;
        $event->user->save();

        if(Session::get('rewards') !== null)
        {
            foreach (Session::get('rewards') as $reward_id) 
            {
                $reward = new Spin;

                if($reward_id == '45')
                {
                    $text_reward = Wheel::find(1)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '90')
                {
                    $text_reward = Wheel::find(2)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '135')
                {
                    return 'you won nothing';
                }
                elseif($reward_id == '180')
                {
                    $text_reward = Wheel::find(4)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '225')
                {
                    $text_reward = Wheel::find(5)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '270')
                {
                    $text_reward = Wheel::find(6)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '315')
                {
                    $text_reward = Wheel::find(7)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                else
                {
                    $text_reward = Wheel::find(8)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }

                $reward->user_id = $event->user->id;
                $reward->reward = $text_reward;
                $reward->reward_is_percentage = $is_perc;
                $reward->save();

                Session::forget('rewards');

            }
        }

        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
        );
        $pusher = new Pusher('32a087e0e1378c7b7210', 'bda79f5550252850598e', '494870', $options);
        // $pusher->trigger('sbdots', 'register', []);
    }
}
