<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;
use App\Spin;
use App\Wheel;
use App\SystemLog;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $event->user->last_login = date('Y-m-d H:i:s');
        $event->user->last_login_ip = \Request::ip();
        $event->user->save();

        if(Session::get('rewards') !== null)
        {
            foreach (Session::get('rewards') as $reward_id) 
            {

                $reward = new Spin;

                if($reward_id == '45')
                {
                    $text_reward = Wheel::find(1)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '90')
                {
                    $text_reward = Wheel::find(2)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '135')
                {
                    return 'you won nothing';
                }
                elseif($reward_id == '180')
                {
                    $text_reward = Wheel::find(4)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '225')
                {
                    $text_reward = Wheel::find(5)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '270')
                {
                    $text_reward = Wheel::find(6)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                elseif($reward_id == '315')
                {
                    $text_reward = Wheel::find(7)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }
                else
                {
                    $text_reward = Wheel::find(8)->name;
                    $is_perc = 0;
                    $reward->status = 1;
                }

                $reward->user_id = $event->user->id;
                $reward->reward = $text_reward;
                $reward->reward_is_percentage = $is_perc;
                $reward->save();

                Session::forget('rewards');

            }
        }

        $user = $event->user;
        $system = new SystemLog;
        $system->log = $user->email.' login to the system';
        $system->save();
    }
}
