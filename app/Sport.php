<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    protected $dates = ['match_time', 'created_at', 'updated_at', 'deleted_at'];
}
