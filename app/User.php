<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password' , 'phone', 'bank_name', 'bank_account_no', 'affiliate_id', 'referred_by',
    ];

    protected $dates = [
        'last_login',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function depositSum()
    {
        return $this->hasMany('App\Transaction')->where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->sum('amount');
    }
    public function withdrawSum()
    {
        return $this->hasMany('App\Transaction')->where('transaction_type','withdraw')->where('status',2)->sum('amount');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function BankRecords()
    {
        return $this->belongsTo('App\BankRecord');
    }

    public function spins()
    {
        return $this->hasMany('App\Spin');
    }

    public function password_requests()
    {
        return $this->hasMany('App\PasswordRequest');
    }

    public function permissions()
    {
        return $this->hasMany('App\Permission');
    }
}
