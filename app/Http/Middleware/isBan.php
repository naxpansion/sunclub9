<?php

namespace App\Http\Middleware;

use Closure;

class isBan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        
        if ($user && $user->is_ban == '0')
        {
            return $next($request); 
        }
        else
        {
            return redirect('player/ban');
        }
    }
}
