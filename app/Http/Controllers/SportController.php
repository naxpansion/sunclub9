<?php

namespace App\Http\Controllers;

use App\Sport;
use Session;
use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;

class SportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sports.index');
    }

    public function data(Datatables $datatables)
    {
        $sports = Sport::All();
        return Datatables::of($sports)
            ->addColumn('actions', function($sport) {
                return ' <a onclick="return confirm();" href="'.url('admin/sports/'.$sport->id.'/delete').'"><span class="label label-danger">delete</span></a> <a href="'.route('sports.edit',$sport->id).'"><span class="label label-info">edit</span></a>';
            })
            ->rawColumns(['status','actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $sport = new Sport;
        $sport->team_1_name = $request->team_1_name;
        $sport->team_2_name = $request->team_2_name;
        $sport->team_1_rating = $request->team_1_rating;
        $sport->team_2_rating = $request->team_2_rating;
        

        if ($request->hasFile('team_1_logo'))
        {
            $save_image = $request->team_1_logo->store('public/matches');
            $filename_team_1 = $request->team_1_logo->hashName();
            $sport->team_1_logo = $filename_team_1;
        }
        if ($request->hasFile('team_2_logo'))
        {
            $save_image = $request->team_2_logo->store('public/matches');
            $filename_team_2 = $request->team_2_logo->hashName();
            $sport->team_2_logo = $filename_team_2;
        }

        $sport->match_time = Carbon::createFromFormat('d-m-Y h:i A',$request->match_time)->format('Y-m-d H:i:s');
        $sport->save();

        Session::flash('message', 'match succesfully added!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('sports.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function show(Sport $sport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function edit(Sport $sport)
    {
        return view('admin.sports.edit',compact('sport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sport $sport)
    {
        $sport->team_1_name = $request->team_1_name;
        $sport->team_2_name = $request->team_2_name;
        $sport->team_1_rating = $request->team_1_rating;
        $sport->team_2_rating = $request->team_2_rating;
        

        if ($request->hasFile('team_1_logo'))
        {
            $save_image = $request->team_1_logo->store('public/matches');
            $filename_team_1 = $request->team_1_logo->hashName();
            $sport->team_1_logo = $filename_team_1;
        }
        if ($request->hasFile('team_2_logo'))
        {
            $save_image = $request->team_2_logo->store('public/matches');
            $filename_team_2 = $request->team_2_logo->hashName();
            $sport->team_2_logo = $filename_team_2;
        }

        $sport->match_time = Carbon::createFromFormat('d-m-Y h:i A',$request->match_time)->format('Y-m-d H:i:s');
        $sport->save();

        Session::flash('message', 'Match succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('sports.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sport  $sport
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sport $sport)
    {
        $sport->delete();

        Session::flash('message', 'Match succesfully deleted!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('sports.index');
    }
}
