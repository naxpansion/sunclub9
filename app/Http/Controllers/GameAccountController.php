<?php

namespace App\Http\Controllers;

use App\GameAccount;
use App\Game;
use Illuminate\Http\Request;
use Session;

class GameAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GameAccount  $gameAccount
     * @return \Illuminate\Http\Response
     */
    public function show(GameAccount $gameAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GameAccount  $gameAccount
     * @return \Illuminate\Http\Response
     */
    public function edit($account)
    {
        $game_account = GameAccount::find($account);
        return view('admin.gameaccounts.edit',compact('game_account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GameAccount  $gameAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $account)
    {
        
        $game_account = GameAccount::find($account);
        $game_account->username = $request->username;
        $game_account->password = $request->password;
        $game_account->save();

        Session::flash('message', 'Account updated succesfully!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/users/'.$game_account->user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GameAccount  $gameAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy($game_id,$user_id)
    {
        $game_account = GameAccount::find($game_id);

        $game_account->delete();

        Session::flash('message', 'Account deleted succesfully!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/users/'.$user_id);
    }

    public function request(Game $game)
    {
        $account = GameAccount::where('status',0)->where('game_id',$game->id)->first();
        $account->user_id = \Auth::user()->id;
        $account->status = 1;
        $account->save();

        return redirect('player');
    }
}
