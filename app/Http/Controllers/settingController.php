<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Session;

class settingController extends Controller
{
    public function index()
    {
    	return view('admin.settings');
    }

    public function update(Request $request)
    {
    	$input = $request->all();

    	$setting_1 = Setting::find(1);
    	$setting_1->value = $input['seo_title'];
    	$setting_1->save();

    	$setting_2 = Setting::find(2);
    	$setting_2->value = $input['seo_desc'];
    	$setting_2->save();

    	$setting_3 = Setting::find(3);
    	$setting_3->value = $input['seo_keyword'];
    	$setting_3->save();

    	$setting_4 = Setting::find(4);
    	$setting_4->value = $input['google_analytic_id'];
    	$setting_4->save();

    	$setting_5 = Setting::find(5);
    	$setting_5->value = $input['min_deposit'];
    	$setting_5->save();

    	$setting_6 = Setting::find(6);
    	$setting_6->value = $input['max_deposit'];
    	$setting_6->save();

        $setting_7 = Setting::find(7);
        $setting_7->value = $input['annoucement'];
        $setting_7->save();

        $setting_8 = Setting::find(8);
        $setting_8->value = $input['whatapp'];
        $setting_8->save();

        $setting_9 = Setting::find(10);
        $setting_9->value = $input['email'];
        $setting_9->save();

        $setting_10 = Setting::find(9);
        $setting_10->value = $input['wechat'];
        $setting_10->save();

        $setting_11 = Setting::find(12);
        $setting_11->value = $input['min_withdraw'];
        $setting_11->save();

        $setting_12 = Setting::find(13);
        $setting_12->value = $input['max_withdraw'];
        $setting_12->save();

        $setting_13 = Setting::find(14);
        $setting_13->value = $input['min_transfer'];
        $setting_13->save();

        $setting_14 = Setting::find(15);
        $setting_14->value = $input['max_transfer'];
        $setting_14->save();

    	Session::flash('message', 'Settings Succesfully Updated'); 
        Session::flash('alert-class', 'alert-success');

    	return redirect('admin/settings');
    }
}
