<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\BankRecord;
use Auth;
use App\Transaction;
use App\Spin;
use Illuminate\Support\Facades\Input;

class SpinController extends Controller
{
    public function index()
    {
    	return view('admin.rewards.spin.index');
    }

    public function data(Datatables $datatables,Request $request)
    {
        $arrStart = explode("-", Input::get('from_date'));
        $arrEnd = explode("-", Input::get('to_date'));
        $from = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        if($request->status == '')
        {
            $rewards = Spin::with('user')->where('status','>',1)->with('game')->where('created_at','<=',$to)->where('created_at','>=',$from)->latest();
        }
        else
        {
            $rewards = Spin::with('user')->where('status',$request->status)->with('game')->where('created_at','<=',$to)->where('created_at','>=',$from)->latest();
        }
    	
        return Datatables::of($rewards)
            ->editColumn('id', function ($transaction) {
                return '#S'.sprintf('%05d', $transaction->id);
            })
            ->editColumn('created_at', function ($reward) {
                return $reward->created_at ? with(new Carbon($reward->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->editColumn('actions', function ($reward) {
                
                return '<a href="'.url('admin/rewards/spin/'.$reward->id.'').'" class="label label-info">view</a>';
            })
            ->editColumn('game', function ($transaction) {

                $game = \App\Game::find($transaction->game_id);


                if($game)
                {
                    $game_id = \App\GameAccount::where('game_id',$game->id)->where('user_id',$transaction->user->id)->first();
                    if($game_id){
                        $username =  $game_id->username;
                    }
                    else
                    {
                        $username = "-";
                    }

                    return $game->name.' / '.$username;
                }
                else
                {
                    return '-';
                }
            })
            ->editColumn('status', function ($reward) {
                if($reward->status == 2)
                {
                    return '<span class="label label-warning">In Progress</span>';
                }
                else if($reward->status == 3)
                {
                    return '<span class="label label-success">Approve</span>';
                }
                else if($reward->status == 4)
                {
                    return '<span class="label label-danger">Reject</span>';
                }
                else if($reward->status == 5)
                {
                    return '<span class="label label-danger">Try Again</span>';
                }
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    public function edit($id)
    {
        $reward = Spin::find($id);
        return view('admin.rewards.spin.edit',compact('reward'));
    }

    public function show($id)
    {
        $user = \App\User::find(\Auth::user()->id);
        
        foreach ($user->unreadNotifications as $notification) {

            if($notification->type == 'App\Notifications\NewSpinReward')
            {
                $notification->markAsRead();
            }
            
        }
        
        $reward = Spin::find($id);
        return view('admin.rewards.spin.show',compact('reward'));
    }

    public function update(Request $request, $id)
    {
        // return $request;
        $reward = Spin::find($id);
        $reward->status = $request->status;
        $reward->remarks = $request->remarks;
        $reward->save();

        Session::flash('message', 'Reward succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/rewards/spin');
    }

    public function accept($id)
    {
        $reward = Spin::find($id);
        $reward->status = 3;
        $reward->save();

        Session::flash('message', 'Reward succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/rewards/spin');
    }

    public function reject($id)
    {
        $reward = Spin::find($id);
        $reward->status = 4;
        $reward->save();

        Session::flash('message', 'Reward succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/rewards/spin');
    }

    public function progress($id)
    {
        $reward = Spin::find($id);
        $reward->status = 2;
        $reward->save();

        Session::flash('message', 'Reward succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/rewards/spin');
    }

    public function destroy($id)
    {
        $reward = Spin::find($id);
        $reward->delete();

        Session::flash('message', 'Reward succesfully deleted!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/rewards/spin');
    }
}
