<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\SpinIp;
use App\Wheel;
use App\PasswordRequest;
use Carbon\Carbon;
use App\Transaction;
use Session;
use App\Spin;
use App\Notifications\NewPasswordRequest;

class ApiController extends Controller
{
    public function adminNotofication()
    {
    	$user = User::find(\Auth::user()->id);

    	$count = $user->unreadNotifications()->count();

    	if($count > 0)
    	{
    		return 1;
    	}
    	else
    	{
    		return 0;
    	}
    }

    public function depositTotal(Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['fromdate']);
        $arrEnd = explode("-", $input['todate']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        return $transactions = Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('created_at','<=',$to_date)->where('status','2')->where('created_at','>=',$from_date)->sum('amount');
    }

    public function withdrawTotal(Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['fromdate']);
        $arrEnd = explode("-", $input['todate']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        return $transactions = Transaction::where('transaction_type','withdraw')->where('created_at','<=',$to_date)->where('status','2')->where('created_at','>=',$from_date)->sum('amount');
    }

    public function depositUserTotal(Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['fromdate']);
        $arrEnd = explode("-", $input['todate']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        return $transactions = Transaction::where('user_id',$request->user_id)->where('transaction_type','deposit')->where('deposit_type','normal')->where('created_at','<=',$to_date)->where('status','2')->where('created_at','>=',$from_date)->sum('amount');
    }

    public function withdrawUserTotal(Request $request)
    {
        $input = $request->all();

        $arrStart = explode("-", $input['fromdate']);
        $arrEnd = explode("-", $input['todate']);

        $from_date = Carbon::create($arrStart[2], $arrStart[1], $arrStart[0], 0, 0, 0);
        $to_date = Carbon::create($arrEnd[2], $arrEnd[1], $arrEnd[0], 23, 59, 59);

        return $transactions = Transaction::where('user_id',$request->user_id)->where('transaction_type','withdraw')->where('created_at','<=',$to_date)->where('status','2')->where('created_at','>=',$from_date)->sum('amount');
    }

    public function desktop()
    {
        session(['view' => 'desktop']);

        return back();
    }

    public function mobile(Request $request)
    {
        $request->session()->forget('view');

        return back();
    }

    public function spin(Request $request)
    {
        $spinip = new SpinIp;
        $spinip->ip = $request->ip;
        $spinip->save();

        return $spinip;
    }

    public function guestReward(Request $request)
    {
        if(Session::get('rewards') !== null)
        {
            $rewards = Session::get('rewards');
        }
        else
        {
            $rewards= array();
        }

        $rewards[] = $request->reward;

        Session::put('rewards',$rewards);

        return Session::get('rewards');
        
    }

    public function authReward(Request $request)
    {
        $user = User::find(\Auth::user()->id);
        $new_token = $user->token - 1;
        $user->token = $new_token;
        $user->save();
        
        $reward_id = $request->reward;
        $reward = new Spin;

        if($reward_id == '45')
        {
            $text_reward = Wheel::find(1)->name;
            $is_perc = 0;
            $reward->status = 1;
        }
        elseif($reward_id == '90')
        {
            $text_reward = Wheel::find(2)->name;
            $is_perc = 0;
            $reward->status = 1;
        }
        elseif($reward_id == '135')
        {
            return 'you won nothing';
        }
        elseif($reward_id == '180')
        {
            $text_reward = Wheel::find(4)->name;
            $is_perc = 0;
            $reward->status = 1;
        }
        elseif($reward_id == '225')
        {
            $text_reward = Wheel::find(5)->name;
            $is_perc = 0;
            $reward->status = 1;
        }
        elseif($reward_id == '270')
        {
            $text_reward = Wheel::find(6)->name;
            $is_perc = 0;
            $reward->status = 1;
        }
        elseif($reward_id == '315')
        {
            $text_reward = Wheel::find(7)->name;
            $is_perc = 0;
            $reward->status = 1;
        }
        else
        {
            $text_reward = Wheel::find(8)->name;
            $is_perc = 0;
            $reward->status = 1;
        }

        
        $reward->user_id = \Auth::user()->id;
        $reward->reward = $text_reward;
        $reward->reward_is_percentage = $is_perc;
        $reward->save();

        
        
        
    }

    public function password_reset(Request $request)
    {
        $user = User::where('phone',$request->phone)->first();

        if($user)
        {
            $PasswordRequest = new PasswordRequest;
            $PasswordRequest->user_id = $user->id;
            $PasswordRequest->name = $user->username;
            $PasswordRequest->phone = $user->phone;
            $PasswordRequest->save();

            $admins = User::where('role',1)->get();
            foreach($admins as $admin)
            {
                $admin->notify(new NewPasswordRequest($PasswordRequest));
            }

            Session::flash('message', 'You request has been sent to admin!'); 
            Session::flash('alert-class', 'alert-success');

            return redirect('password/reset');
        }
        else
        {
            Session::flash('message', 'Mobile number not registered with any account!'); 
            Session::flash('alert-class', 'alert-warning');

            return redirect('password/reset');
        }
    }
}
