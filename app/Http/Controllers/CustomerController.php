<?php

namespace App\Http\Controllers;

use App\Customer;
use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Excel;
use Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customers.index');
    }

    public function data(Datatables $datatables)
    {
        $customers = Customer::All();
        return Datatables::of($customers)
            ->addColumn('status', function($customer) {
                if($customer->status == 1)
                {
                    return '<span class="label label-warning">No Deposit</span>';
                }
                else
                {
                    return '<span class="label label-success">Deposit</span>';
                }
            })
            ->editColumn('created_at', function ($customer) {
                return $customer->created_at ? with(new Carbon($customer->created_at))->format('d M Y, h:i A') : '';
            })
            ->addColumn('actions', function($customer) {
                return '<a href="'.route('customers.show',$customer->id).'"><span class="label label-info">view</span></a>';
            })
            ->rawColumns(['status','actions'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:11|max:12',
        ]);

        if ($validator->fails())
        {
            Session::flash('message', $validator->messages()->first()); 
            Session::flash('alert-class', 'alert-warning');

            return redirect()->back()->withInput();
        }

        $customer = new Customer;
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->source = $request->source;
        $customer->status = $request->status;
        $customer->game = $request->game;
        $customer->remarks = $request->remarks;
        $customer->save();

        Session::flash('message', 'customer succesfully added!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('customers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return view('admin.customers.show',compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('admin.customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:11|max:12',
        ]);

        if ($validator->fails())
        {
            Session::flash('message', $validator->messages()->first()); 
            Session::flash('alert-class', 'alert-warning');

            return redirect()->back()->withInput();
        }

        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->source = $request->source;
        $customer->status = $request->status;
        $customer->game = $request->game;
        $customer->remarks = $request->remarks;
        $customer->save();

        Session::flash('message', 'customer succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('customers.show',$customer->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        Session::flash('message', 'customer succesfully deleted!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('customers.index');
    }

    public function export()
    {
            $customers = Customer::All();

            // Initialize the array which will be passed into the Excel
            // generator.
            $orderArray = []; 

            // Define the Excel spreadsheet headers
            $orderArray[] = ['Added At','Name','Phone','source','Games Interested','remarks'];

            // Convert each member of the returned collection into an array,
            // and append it to the payments array.
            foreach ($customers as $customer) {

                $date = $customer->created_at->format('d M Y');
                $name = $customer->name;
                $phone = $customer->phone;
                $source = $customer->source;
                $game = $customer->game;
                $remarks = $customer->remarks;
                

                $tempArray = array($date,$name,$phone,$source,$game,$remarks);          

                $orderArray[] = $tempArray;

            }

            

            Excel::create('mbet11')
            ->sheet('sheet1', function($sheet) use ($orderArray) {
                    $sheet->fromArray($orderArray, null, 'A1', false, false);
                    $sheet->row(1, function($row) {
                        $row->setBackground('#eeeeee');
                    });
                })           
            ->export('xls');
    }
}
