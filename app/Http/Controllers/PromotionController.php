<?php

namespace App\Http\Controllers;

use App\Promotion;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use DataTables;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.promotions.index');
    }

    public function data(Datatables $datatables)
    {
        $promotions = Promotion::get();
        return Datatables::of($promotions)
            ->addColumn('actions', function($promotion) {
                return '<a class="label label-info" href="'.route('promotions.edit',$promotion->id).'">Edit</a> <form method="post" action="'.route('promotions.destroy',$promotion->id).'">'.csrf_field().''.method_field('DELETE').'<button onclick="return confirm();" class="label label-danger">Delete</button></form>';
            })
            ->editColumn('created_at', function ($promotion) {
                return $promotion->created_at ? with(new Carbon($promotion->created_at))->format('d M Y, h:i A') : '';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;

        $promotion = new Promotion;
        $promotion->title = $request->name;

        $i = 0;
        $promotion->desktop = $request->content_desktop;
        $promotion->mobile = $request->content_mobile;
        
        if ($request->hasFile('img_desktop'))
        {
            $save_image = $request->img_desktop->store('public/promotions');
            $filename = $request->img_desktop->hashName();
            $promotion->img_desktop = $filename;
        }

        if ($request->hasFile('img_mobile'))
        {
            $save_image = $request->img_mobile->store('public/promotions');
            $filename = $request->img_mobile->hashName();
            $promotion->img_mobile = $filename;
        }

        $promotion->save();

        Session::flash('message', 'New promotion succesfully added!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/promotions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        return view('admin.promotions.edit',compact('promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        $promotion->title = $request->name;
        $promotion->desktop = $request->content_desktop;
        $promotion->mobile = $request->content_mobile;
        
        if ($request->hasFile('img_desktop'))
        {
            $save_image = $request->img_desktop->store('public/promotions');
            $filename = $request->img_desktop->hashName();
            $promotion->img_desktop = $filename;
        }

        if ($request->hasFile('img_mobile'))
        {
            $save_image = $request->img_mobile->store('public/promotions');
            $filename = $request->img_mobile->hashName();
            $promotion->img_mobile = $filename;
        }

        $promotion->save();

        Session::flash('message', 'Promotion succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('promotions.edit',$promotion->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        $promotion->delete();

        Session::flash('message', 'Promotion succesfully deleted!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('promotions.index');
    }
}
