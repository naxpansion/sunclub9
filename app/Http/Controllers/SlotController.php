<?php

namespace App\Http\Controllers;

use App\Slot;
use Session;
use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;

class SlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slots.index');
    }

    public function data(Datatables $datatables)
    {
        $slots = Slot::All();
        return Datatables::of($slots)
            ->editColumn('others', function($slot) {

                $others = '';

                if($slot->is_hot == 1)
                {
                     $others .= '<label class="label label-default">HOT</label> ';
                }
                if($slot->is_new == 1)
                {
                     $others .= '<label class="label label-default">NEW</label> ';
                }
                if($slot->is_progressive == 1)
                {
                     $others .= '<label class="label label-default">PROGRESSIVE</label> ';
                }
                if($slot->is_featured == 1)
                {
                     $others .= '<label class="label label-default">FEATURED</label> ';
                }
                return $others;
            })
            ->addColumn('actions', function($slot) {
                return '<a href="'.route('slots.edit',$slot->id).'"><span class="label label-info">edit</span></a>';
            })
            ->rawColumns(['others','actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slot = new Slot;
        $slot->name = $request->name;
        if ($request->hasFile('image'))
        {
            $save_image = $request->image->store('public/slots');
            $filename = $request->image->hashName();
            $slot->image = $filename;
        }
        $slot->rating = $request->rating;
        $slot->link = $request->link;
        $slot->is_hot = $request->is_hot;
        $slot->is_new = $request->is_new;
        $slot->is_progressive = $request->is_progressive;
        $slot->is_featured = $request->is_featured;

        $slot->save();

        Session::flash('message', 'Slots succesfully added!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('slots.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slot  $slot
     * @return \Illuminate\Http\Response
     */
    public function show(Slot $slot)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slot  $slot
     * @return \Illuminate\Http\Response
     */
    public function edit(Slot $slot)
    {
        return view('admin.slots.edit',compact('slot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slot  $slot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slot $slot)
    {
        $slot->name = $request->name;
        if ($request->hasFile('image'))
        {
            $save_image = $request->image->store('public/slots');
            $filename = $request->image->hashName();
            $slot->image = $filename;
        }
        $slot->rating = $request->rating;
        $slot->link = $request->link;
        $slot->is_hot = $request->is_hot;
        $slot->is_new = $request->is_new;
        $slot->is_progressive = $request->is_progressive;
        $slot->is_featured = $request->is_featured;

        $slot->save();

        Session::flash('message', 'Slots succesfully updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('slots.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slot  $slot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slot $slot)
    {
        //
    }
}
