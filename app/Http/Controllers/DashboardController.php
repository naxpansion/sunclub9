<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use App\Game;
use App\Spin;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
    	

    	$user_today = User::where('role',3)->where('created_at', '>=', Carbon::today())->count();

        $today_deposit_sum = Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->where('created_at', '>=', Carbon::today())->sum('amount');

        $today_withdraw_sum = Transaction::where('transaction_type','withdraw')->where('status',2)->where('created_at', '>=', Carbon::today())->sum('amount');

        $today_bonus_amount = Transaction::where('transaction_type','deposit')->whereIn('deposit_type',['bonus','rebate','birthday'])->where('status',2)->where('created_at', '>=', Carbon::today())->sum('amount');

    	$total_transaction = Transaction::all()->count();

    	$pending_transaction_count = Transaction::where('status',1)->count();

    	$pending_transactions = Transaction::where('status',1)->paginate(10);

        $total_count_deposits = Transaction::where('transaction_type','deposit')->where('deposit_type','normal')->where('status',2)->where('created_at', '>=', Carbon::today())->get();


        $spins = Spin::where('reward_is_percentage',0)->where('reward','!=','Jackpot')->where('status',3)->where('reward','!=','Try Again')->where('created_at', '>=', Carbon::today())->get();
        $total_reward = 0;
        foreach($spins as $spin)
        {
            $new_Reward = preg_replace("/[^0-9.]/", '', $spin->reward);
            $total_reward = $total_reward + (int)$new_Reward;
        }
        $total_reward;

        $today_count_member_deposit = 0;
        $today_count_member_deposit = array();

        foreach($total_count_deposits as $total_count_deposit)
        {
            array_push($today_count_member_deposit,$total_count_deposit->user_id);
        }
        $remove_dup = array_unique($today_count_member_deposit);
        $today_count_member_deposit = count($remove_dup);

        if(\Auth::user()->role == 1)
        {
            return view('admin.dashboard',[
                'user_today' => $user_today,
                'today_deposit_sum' => $today_deposit_sum,
                'today_withdraw_sum' => $today_withdraw_sum,
                'today_bonus_amount' => $today_bonus_amount,
                'total_transaction' => $total_transaction,
                'pending_transaction_count' => $pending_transaction_count,
                'pending_transactions' => $pending_transactions,
                'today_count_member_deposit' => $today_count_member_deposit,
                'total_reward' => $total_reward
            ]);$role = 'admin';
        }
        else
        {
            return view('staff.dashboard',[
                'user_today' => $user_today,
                'today_deposit_sum' => $today_deposit_sum,
                'today_withdraw_sum' => $today_withdraw_sum,
                'today_bonus_amount' => $today_bonus_amount,
                'total_transaction' => $total_transaction,
                'pending_transaction_count' => $pending_transaction_count,
                'pending_transactions' => $pending_transactions
            ]);
        }

    	
    }

    public function login()
    {
        return view('admin.login');
    }
}
