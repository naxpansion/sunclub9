<?php

namespace App\Http\Controllers;

use App\SystemLog;
use Illuminate\Http\Request;
use Session;
use DataTables;
use Carbon\Carbon;

class SystemLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.systemlogs.index');
    }

    public function data()
    {
        $systemlogs = SystemLog::latest()->get();
        return Datatables::of($systemlogs)
            ->editColumn('created_at', function ($systemlog) {
                return $systemlog->created_at ? with(new Carbon($systemlog->created_at))->format('d/m/Y, h:i A') : '';
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function show(SystemLog $systemLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function edit(SystemLog $systemLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SystemLog $systemLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(SystemLog $systemLog)
    {
        //
    }
}
