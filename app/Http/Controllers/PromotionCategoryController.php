<?php

namespace App\Http\Controllers;

use App\PromotionCategory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class PromotionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promoCategories = PromotionCategory::all();

        return view('admin.promotions.category',compact('promoCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PromotionCategory = new PromotionCategory;
        $PromotionCategory->name = $request->name;
        $PromotionCategory->save();

        Session::flash('message', 'Category succesfully added!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/promotionscategory/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PromotionCategory  $promotionCategory
     * @return \Illuminate\Http\Response
     */
    public function show(PromotionCategory $promotionCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PromotionCategory  $promotionCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PromotionCategory $promotionCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PromotionCategory  $promotionCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PromotionCategory $promotionCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PromotionCategory  $promotionCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromotionCategory $promotionCategory)
    {
        //
    }
}
