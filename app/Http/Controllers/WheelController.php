<?php

namespace App\Http\Controllers;

use App\Wheel;
use Illuminate\Http\Request;
use Session;

class WheelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.wheels.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wheel  $wheel
     * @return \Illuminate\Http\Response
     */
    public function show(Wheel $wheel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wheel  $wheel
     * @return \Illuminate\Http\Response
     */
    public function edit(Wheel $wheel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wheel  $wheel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wheel $wheel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wheel  $wheel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wheel $wheel)
    {
        //
    }

    public function sync(Request $request)
    {
        // return $request;
        $section_1 = Wheel::find(1);
        $section_1->name = $request->name_1;
        $section_1->percentage = $request->percentage_1;
        $section_1->code = $request->code_1; 

        $section_2 = Wheel::find(2);
        $section_2->name = $request->name_2;
        $section_2->percentage = $request->percentage_2;
        $section_2->code = $request->code_2; 

        $section_3 = Wheel::find(3);
        $section_3->name = $request->name_3;
        $section_3->percentage = $request->percentage_3;
        $section_3->code = $request->code_3; 

        $section_4 = Wheel::find(4);
        $section_4->name = $request->name_4;
        $section_4->percentage = $request->percentage_4;
        $section_4->code = $request->code_4; 

        $section_5 = Wheel::find(5);
        $section_5->name = $request->name_5;
        $section_5->percentage = $request->percentage_5;
        $section_5->code = $request->code_5; 

        $section_6 = Wheel::find(6);
        $section_6->name = $request->name_6;
        $section_6->percentage = $request->percentage_6;
        $section_6->code = $request->code_6; 

        $section_7 = Wheel::find(7);
        $section_7->name = $request->name_7;
        $section_7->percentage = $request->percentage_7;
        $section_7->code = $request->code_7; 

        $section_8 = Wheel::find(8);
        $section_8->name = $request->name_8;
        $section_8->percentage = $request->percentage_8;
        $section_8->code = $request->code_8; 

        $section_1->save();
        $section_2->save();
        $section_3->save();
        $section_4->save();
        $section_5->save();
        $section_6->save();
        $section_7->save();
        $section_8->save();

        Session::flash('message', 'Wheel Updated!'); 
        Session::flash('alert-class', 'alert-success');

        return redirect('admin/wheel');
    }
}
